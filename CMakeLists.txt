cmake_minimum_required(VERSION 3.7)
project(Testli)

# enable C++11
if (CMAKE_VERSION VERSION_LESS "3.1")
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        set(CMAKE_CXX_FLAGS "--std=gnu++11 ${CMAKE_CXX_FLAGS}")
    endif ()
else ()
    set(CMAKE_CXX_STANDARD 11)
endif ()

find_package(Qt5Core)
find_package(Qt5Gui)
find_package(Qt5Svg)
find_package(Qt5Widgets)
find_package(Qt5Xml)
find_package(Qt5XmlPatterns)

# handle version inforamtions
include(version.cmake)
set(VERSION_FULL "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
set(BUILD_NUMBER CACHE STRING "")
set(BUILD_NOTE CACHE STRING "")
string(TIMESTAMP BUILD_TIME "%Y-%m-%d %H:%M" UTC)

if (BUILD_NUMBER STREQUAL "")
    set(HAS_BUILD_NUMBER OFF)
    set(VERSION_FULL_BUILD "${VERSION_FULL}")
else()
    set(HAS_BUILD_NUMBER ON)
    set(VERSION_FULL_BUILD "${VERSION_FULL}.${BUILD_NUMBER}")
endif()

configure_file(
    src/version.h.in
    version.h
    @ONLY
)

# enable moc and rcc
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long -pedantic -Wextra")

include_directories(${CMAKE_BINARY_DIR})

# define executable
file(GLOB_RECURSE SRC_CPP "src/*.cpp")
file(GLOB_RECURSE SRC_UI "src/*.ui")
file(GLOB_RECURSE SRC_QRC "res/*.qrc")

if(WIN32)
    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        add_executable(${PROJECT_NAME} ${SRC_CPP} ${SRC_UI} ${SRC_QRC})
    else()
        add_executable(${PROJECT_NAME} WIN32 ${SRC_CPP} ${SRC_UI} ${SRC_QRC})
    endif()
else()
    add_executable(${PROJECT_NAME} ${SRC_CPP} ${SRC_UI} ${SRC_QRC})
endif()

# link libaries
target_link_libraries(${PROJECT_NAME} Qt5::Core Qt5::Gui Qt5::Widgets
                      Qt5::Xml Qt5::XmlPatterns Qt5::Svg)

set_target_properties(${PROJECT_NAME}
                      PROPERTIES
                      RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
