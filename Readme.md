# Testli
Testli is a simple application to manage test specification and create
test reports.

## Dependencies
* [Qt 5](https://www.qt.io/)
* [Tango icon set](https://commons.wikimedia.org/wiki/Tango_icons)
* [CMake](https://cmake.org/)
* GCC compiler

## How to build
```
mkdir -p build/
cd build/
cmake ../
make
```

