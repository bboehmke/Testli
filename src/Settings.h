/**
 * @file Settings.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_SETTINGS_H
#define TESTLI_SETTINGS_H

#include <QString>
#include <QDomElement>
#include <QVariant>
#include <QSettings>

/**
 * Setting singleton
 */
class Settings {
    public:
        /**
         * Load application configuration & set default values
         */
        static void init();
        /**
         * Load project configuration
         * @param element Root config element
         * @param specPath Path of test spec file
         */
        static void loadProject(const QDomElement& element,
                                const QString& specPath);
        /**
         * Save project settings
         * @param element Root config element
         */
        static void saveProject(QDomElement* element);
        /**
         * Clear settings instance
         */
        static void clear();

        /**
         * Get value (project -> application -> default)
         * @param key Value key
         * @return Value
         */
        static QVariant getValue(const QString& key);
        /**
         * Get project value (application -> default)
         * @param key Value key
         * @return Value
         */
        static QVariant getProjectValue(const QString& key);
        /**
         * Set project value
         * @param key Value key
         * @param value Value
         */
        static void setProjectValue(const QString& key, const QVariant& value);
        /**
         * Get application value (application -> default)
         * @param key Value key
         * @return Value
         */
        static QVariant getApplicationValue(const QString& key);
        /**
         * Set application value
         * @param key Value key
         * @param value Value
         */
        static void setApplicationValue(const QString& key, const QVariant& value);

        /**
         * Get the base path of the test spec
         * @return Path to test spec directory
         */
        static QString getSpecBasePath();

    private:
        /**
         * Unused constructor
         */
        Settings();

        /**
         * List of default values
         */
        static QMap<QString, QVariant> defaultValues;

        /**
         * Application settings
         */
        static QSettings* applicationSettings;

        /**
         * Project settings
         */
        static QMap<QString, QVariant> projectSettings;

        /**
         * Path of test spec directory
         */
        static QString specBasePath;

        /**
         * Set default value
         * @param key Value key
         * @param value Value
         */
        static void setDefaultValue(const QString& key, const QVariant& value);
        /**
         * Get default value
         * @param key Value key
         * @return Value
         */
        static QVariant getDefaultValue(const QString& key);
};


#endif //TESTLI_SETTINGS_H
