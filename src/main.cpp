/**
 * @file main.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDebug>

#include "MainWindow.h"

/**
 * Custom message handler for better debug messages
 * @param type Type of message
 * @param context Message context
 * @param msg Message content
 */
void messageHandler(QtMsgType type, const QMessageLogContext &context,
                    const QString &msg) {
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
        case QtDebugMsg:
            fprintf(stderr, "[DEB] - %s:%u - %s\n", context.file, context.line, localMsg.constData());
            break;
        case QtInfoMsg:
            fprintf(stderr, "[INF] - %s:%u - %s\n", context.file, context.line, localMsg.constData());
            break;
        case QtWarningMsg:
            fprintf(stderr, "[WRN] - %s:%u - %s\n", context.file, context.line, localMsg.constData());
            break;
        case QtCriticalMsg:
            fprintf(stderr, "[CRT] - %s:%u - %s\n", context.file, context.line, localMsg.constData());
            break;
        case QtFatalMsg:
            fprintf(stderr, "[ERR] - %s:%u - %s\n", context.file, context.line, localMsg.constData());
    }
}

/**
 * Main function
 * @param argc Amount of parameters
 * @param argv List of command line parameters
 * @return
 */
int main(int argc, char** argv) {
    qInstallMessageHandler(messageHandler);
    QApplication::setApplicationName("Testli");
    QApplication app(argc, argv);

    // try to get project file from parameter
    QString project;
    if (argc > 1) {
        project = argv[1];
    }

    MainWindow window(project);
    window.show();
    return app.exec();
}