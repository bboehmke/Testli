/**
 * @file XmlHelper.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "XmlHelper.h"

#include <QTextStream>
#include <QFile>
#include <QRegularExpression>

#include "../Exception.h"

QDomDocument XmlHelper::loadXml(const QString& path) {
    // open test spec
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        throw Exception(Exception::FileOpenFailed,
                        "Failed to open file " + path + ": " + file.errorString());
    }

    // load XML document
    QString errorMessage;
    QDomDocument doc;
    if (!doc.setContent(&file, &errorMessage)) {
        file.close();
        throw Exception(Exception::XmlError, errorMessage);
    }
    file.close();

    return doc;
}

QString XmlHelper::getRootAttribute(QDomElement element,
                                    const QString& attr) {
    while (!element.parentNode().toElement().isNull()) {
        element = element.parentNode().toElement();
    }
    return element.attribute(attr);
}

QDomElement XmlHelper::findElement(const QDomElement& element,
                                   const QString& name) {

    for (QDomNode n = element.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == name) {
                return e;

            } else if (e.hasChildNodes()) {
                QDomElement ele = findElement(e, name);
                if (!ele.isNull()) {
                    return ele;
                }
            }
        }
    }
    return QDomElement();
}

QString XmlHelper::getXmlContent(const QDomElement& element) {
    if (element.isNull() || element.text().isEmpty()) {
        return "";
    }

    QString str;
    QTextStream stream(&str, QIODevice::WriteOnly);
    element.save(stream, 2);

    str.remove(QRegularExpression(QString("<%1[^>]*>").arg(element.tagName())));
    str.remove(QString("</%1>").arg(element.tagName()));

    return str.trimmed();
}

void XmlHelper::addXmlContent(QDomElement* element, const QString& text) {
    QDomDocument doc;
    if (!doc.setContent(QString("<XmlContent>%1</XmlContent>").arg(text))) {
        return;
    }
    QDomElement root = doc.documentElement();

    for (QDomNode n = root.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        element->appendChild(n.cloneNode(true));
    }
}