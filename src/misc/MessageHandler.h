/**
 * @file MessageHandler.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_MESSAGEHANDLER_H
#define TESTLI_MESSAGEHANDLER_H

#include <QAbstractMessageHandler>

/**
 * Handler for XML messages
 */
class MessageHandler : public QAbstractMessageHandler {
    Q_OBJECT
    public:
        /**
         * Create message handler
         */
        MessageHandler();

        /**
         * Get description of message
         * @return Description
         */
        QString getDescription() const;
        /**
         * Get location of message source
         * @return Location of message source
         */
        QSourceLocation getSourceLocation() const;

    protected:
        /**
         * Handle message
         * @param type Type of message
         * @param description Message content
         * @param identifier Message identifier
         * @param sourceLocation Source location of message
         */
        void handleMessage(QtMsgType type,
                           const QString& description,
                           const QUrl& identifier,
                           const QSourceLocation& sourceLocation);

    private:
        /**
         * Message content
         */
        QString description;
        /**
         * Location of message source
         */
        QSourceLocation sourceLocation;

};


#endif //TESTLI_MESSAGEHANDLER_H
