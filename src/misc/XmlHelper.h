/**
 * @file XmlHelper.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_XMLHELPER_H
#define TESTLI_XMLHELPER_H

#include <QString>
#include <QDomElement>

/**
 * Helper functions for XML handling
 */
class XmlHelper {
    public:
        /**
         * Load XML file
         * @param path Path to XML file
         * @return XML document
         */
        static QDomDocument loadXml(const QString& path);

        /**
         * Get attribute of root element
         * @param element XML element
         * @param attr Attribute name
         * @return Value of attribute
         */
        static QString getRootAttribute(QDomElement element,
                                        const QString& attr);

        /**
         * Find child element with given name
         * @param element Base XML element
         * @param name Tag name to search for
         * @return XML element or null element if not found
         */
        static QDomElement findElement(const QDomElement& element,
                                       const QString& name);

        /**
         * Get content of element as string
         * @param element XML element
         * @return Content XML
         */
        static QString getXmlContent(const QDomElement& element);

        /**
         * Parse XML text to XML element
         * @param element Element where the content should be added
         * @param text XML content
         */
        static void addXmlContent(QDomElement* element, const QString& text);
};


#endif //TESTLI_XMLHELPER_H
