/**
 * @file XmlReceiver.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "XmlReceiver.h"


XmlReceiver::XmlReceiver(QXmlQuery* query) : query(query) {

}

QString XmlReceiver::getResultValue() const {
    return resultValue;
}

void XmlReceiver::attribute(const QXmlName& /*name*/, const QStringRef& value) {
    resultValue = value.toString();
}


void XmlReceiver::characters(const QStringRef &value) {
    resultValue = value.toString();
}


//@cond nodoc
void XmlReceiver::processingInstruction(const QXmlName& /*target*/,
                                        const QString& /*value*/) {

}
void XmlReceiver::comment(const QString& /*value*/) {

}
void XmlReceiver::atomicValue(const QVariant& /*value*/) {

}
void XmlReceiver::namespaceBinding(const QXmlName& /*name*/) {

}
void XmlReceiver::startElement(const QXmlName& /*name*/) {

}
void XmlReceiver::endElement() {

}
void XmlReceiver::startDocument() {

}
void XmlReceiver::endDocument() {

}
void XmlReceiver::startOfSequence() {

}
void XmlReceiver::endOfSequence() {

}
//@endcond
