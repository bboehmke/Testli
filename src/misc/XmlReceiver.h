/**
 * @file XmlReceiver.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_XMLRECEIVER_H
#define TESTLI_XMLRECEIVER_H

#include <QAbstractXmlReceiver>
#include <QXmlQuery>

/**
 * Receiver for XML queries
 */
class XmlReceiver : public QAbstractXmlReceiver {
    public:
        /**
         * Create receiver
         * @param query Executed query
         */
        XmlReceiver(QXmlQuery* query);

        /**
         * Get the result value of the query
         * @return Result value
         */
        QString getResultValue() const;

        /**
         * Handle attribute
         * @param name Name of attribute
         * @param value Value of attribute
         */
        void attribute(const QXmlName& name,
                       const QStringRef& value);

        /**
         * Handle text data
         * @param value Text data
         */
        void characters(const QStringRef& value);

        // unused callbacks
        //@cond nodoc
        void processingInstruction(const QXmlName& target,
                                   const QString& value);
        void comment(const QString& value);
        void atomicValue(const QVariant& value);
        void namespaceBinding(const QXmlName& name);
        void startElement(const QXmlName& name);
        void endElement();
        void startDocument();
        void endDocument();
        void startOfSequence();
        void endOfSequence();
        //@endcond
    private:
        /**
         * Query instance
         */
        QXmlQuery* query;

        /**
         * Value of query result
         */
        QString resultValue;
};


#endif //TESTLI_XMLRECEIVER_H
