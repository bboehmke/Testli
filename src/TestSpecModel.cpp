/**
 * @file TestSpecModel.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestSpecModel.h"

#include <QBrush>
#include <QColor>
#include <QFont>

TestSpecModel::TestSpecModel(TestSpec* testSpec) : testSpec(testSpec) {

}


QModelIndex TestSpecModel::insertFeatureGroup(const QModelIndex &current,
                                              const QString &name) {
    int pos = testSpec->getFeatureGroupList().size();


    if (current.isValid()) {
        TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
        if (testcase) {
            item = testcase->getParent();
        } else if (linkedTestcase) {
            item = linkedTestcase->getParent();
        }
        Feature* feature = dynamic_cast<Feature*>(item);
        if (feature) {
            item = feature->getParent();
        }
        FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
        if (featureGroup) {
            pos = featureGroup->getIndexNumber() + 1;
        }
    }
    
    beginInsertRows(QModelIndex(), pos, pos);
    FeatureGroup* newFeatures = testSpec->addFeatureGroup(pos, name);
    endInsertRows();
    return createIndex(newFeatures->getIndexNumber(), 0, newFeatures);
}
QModelIndex TestSpecModel::insertFeature(const QModelIndex& current,
                                         const QString& name,
                                         const QString& id) {
    if (!canInsertFeature(current)) {
        return QModelIndex();
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    if (testcase) {
        item = testcase->getParent();
    } else if (linkedTestcase) {
        item = linkedTestcase->getParent();
    }

    int pos;
    Feature* feature = dynamic_cast<Feature*>(item);
    FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
    if (feature) {
        pos = feature->getIndexNumber() + 1;
        featureGroup = feature->getParent();

    } else if (featureGroup) {
        pos = featureGroup->getFeatureList().size();
    } else {
        return QModelIndex();
    }

    beginInsertRows(createIndex(featureGroup->getIndexNumber(), 0, featureGroup), pos, pos);
    Feature* newFeature = featureGroup->addFeature(pos, name, id);
    endInsertRows();
    return createIndex(newFeature->getIndexNumber(), 0, newFeature);
}
QModelIndex TestSpecModel::insertTestcase(const QModelIndex& current,
                                          const QString& id) {
    if (!canInsertTestcase(current)) {
        return QModelIndex();
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Feature* feature = dynamic_cast<Feature*>(item);
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    int pos;
    if (testcase) {
        pos = current.row() + 1;
        feature = testcase->getParent();
    } else if (linkedTestcase) {
        pos = current.row() + 1;
        feature = linkedTestcase->getParent();
    } else if (feature) {
        pos = feature->getTestcaseList().size();
    } else {
        return QModelIndex();
    }

    beginInsertRows(createIndex(feature->getIndexNumber(), 0, feature), pos, pos);
    Testcase* newTestcase = feature->addTestcase(pos, id);
    endInsertRows();
    return createIndex(newTestcase->getIndexNumber(), 0, newTestcase);
}
QModelIndex TestSpecModel::insertLinkedTestcase(const QModelIndex& current,
                                                const QString& featureId,
                                                const QString& testcaseId) {
    if (!canInsertTestcase(current)) {
        return QModelIndex();
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Feature* feature = dynamic_cast<Feature*>(item);
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    int pos;
    if (testcase) {
        pos = current.row() + 1;
        feature = testcase->getParent();
    } else if (linkedTestcase) {
        pos = current.row() + 1;
        feature = linkedTestcase->getParent();
    } else if (feature) {
        pos = feature->getTestcaseList().size();
    } else {
        return QModelIndex();
    }

    beginInsertRows(createIndex(feature->getIndexNumber(), 0, feature), pos, pos);
    LinkedTestcase* newTestcase =
            feature->addLinkedTestcase(pos, featureId, testcaseId);
    endInsertRows();
    return createIndex(newTestcase->getIndexNumber(), 0, newTestcase);
}
bool TestSpecModel::canInsertFeature(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
    Feature* feature = dynamic_cast<Feature*>(item);
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    return linkedTestcase || testcase || feature || featureGroup;
}
bool TestSpecModel::canInsertTestcase(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Feature* feature = dynamic_cast<Feature*>(item);
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    return linkedTestcase || testcase || feature;
}


bool TestSpecModel::remove(const QModelIndex& current) {
    if (!canRemove(current)) {
        return false;
    }
    
    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    Feature* feature = dynamic_cast<Feature*>(item);
    FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);


    if (testcase) {
        Feature* parent = testcase->getParent();
        beginRemoveRows(
                createIndex(parent->getIndexNumber(), 0, parent),
                testcase->getIndexNumber(),
                testcase->getIndexNumber());
        parent->removeTestcase(testcase);

    } else if (linkedTestcase) {
        Feature* parent = linkedTestcase->getParent();
        beginRemoveRows(
                createIndex(parent->getIndexNumber(), 0, parent),
                linkedTestcase->getIndexNumber(),
                linkedTestcase->getIndexNumber());
        parent->removeTestcase(linkedTestcase);

    } else if (feature) {
        FeatureGroup* parent = feature->getParent();
        beginRemoveRows(
                createIndex(parent->getIndexNumber(), 0, parent),
                feature->getIndexNumber(),
                feature->getIndexNumber());
        parent->removeFeature(feature);

    } else if (featureGroup) {
        TestSpec* parent = featureGroup->getParent();
        beginRemoveRows(
                QModelIndex(),
                featureGroup->getIndexNumber(),
                featureGroup->getIndexNumber());
        parent->removeFeatureGroup(featureGroup);

    } else {
        return false;
    }
    endRemoveRows();
    return true;
}
bool TestSpecModel::canRemove(const QModelIndex& current) const {
    return current.isValid();
}

bool TestSpecModel::moveUp(const QModelIndex& current) {
    if (!canMoveUp(current)) {
        return false;
    }
    
    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    Feature* feature = dynamic_cast<Feature*>(item);
    FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);

    if (testcase) {
        Feature* parent = testcase->getParent();
        QModelIndex parentIndex = createIndex(parent->getIndexNumber(), 0, parent);
        beginMoveRows(parentIndex, testcase->getIndexNumber(), testcase->getIndexNumber(),
                      parentIndex, testcase->getIndexNumber()-1);

        parent->moveTestcase(testcase, testcase->getIndexNumber()-1);

    } else if (linkedTestcase) {
        Feature* parent = linkedTestcase->getParent();
        QModelIndex parentIndex = createIndex(parent->getIndexNumber(), 0, parent);
        beginMoveRows(parentIndex, linkedTestcase->getIndexNumber(), linkedTestcase->getIndexNumber(),
                      parentIndex, linkedTestcase->getIndexNumber()-1);

        parent->moveTestcase(linkedTestcase, linkedTestcase->getIndexNumber()-1);

    } else if (feature) {
        FeatureGroup* parent = feature->getParent();
        QModelIndex parentIndex = createIndex(parent->getIndexNumber(), 0, parent);
        beginMoveRows(parentIndex, feature->getIndexNumber(), feature->getIndexNumber(),
                      parentIndex, feature->getIndexNumber()-1);

        parent->moveFeature(feature, feature->getIndexNumber()-1);

    } else if (featureGroup) {
        TestSpec* parent = featureGroup->getParent();
        beginMoveRows(QModelIndex(), featureGroup->getIndexNumber(), featureGroup->getIndexNumber(),
                      QModelIndex(), featureGroup->getIndexNumber()-1);

        parent->moveFeatureGroup(featureGroup, featureGroup->getIndexNumber() - 1);

    } else {
        return false;
    }

    endMoveRows();
    return true;
}
bool TestSpecModel::moveDown(const QModelIndex& current) {
    if (!canMoveDown(current)) {
        return false;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    Feature* feature = dynamic_cast<Feature*>(item);
    FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);

    if (testcase) {
        Feature* parent = testcase->getParent();
        QModelIndex parentIndex = createIndex(parent->getIndexNumber(), 0, parent);
        beginMoveRows(parentIndex, testcase->getIndexNumber(), testcase->getIndexNumber(),
                      parentIndex, testcase->getIndexNumber()+2);

        parent->moveTestcase(testcase, testcase->getIndexNumber()+1);

    } else if (linkedTestcase) {
        Feature* parent = linkedTestcase->getParent();
        QModelIndex parentIndex = createIndex(parent->getIndexNumber(), 0, parent);
        beginMoveRows(parentIndex, linkedTestcase->getIndexNumber(), linkedTestcase->getIndexNumber(),
                      parentIndex, linkedTestcase->getIndexNumber()+2);

        parent->moveTestcase(linkedTestcase, linkedTestcase->getIndexNumber()+1);

    } else if (feature) {
        FeatureGroup* parent = feature->getParent();
        QModelIndex parentIndex = createIndex(parent->getIndexNumber(), 0, parent);
        beginMoveRows(parentIndex, feature->getIndexNumber(), feature->getIndexNumber(),
                      parentIndex, feature->getIndexNumber()+2);

        parent->moveFeature(feature, feature->getIndexNumber()+1);

    } else if (featureGroup) {
        TestSpec* parent = featureGroup->getParent();
        beginMoveRows(QModelIndex(), featureGroup->getIndexNumber(), featureGroup->getIndexNumber(),
                      QModelIndex(), featureGroup->getIndexNumber()+2);

        parent->moveFeatureGroup(featureGroup, featureGroup->getIndexNumber() + 1);

    } else {
        return false;
    }

    endMoveRows();
    return true;
}
bool TestSpecModel::canMoveUp(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    return item->getIndexNumber() > 0;
}
bool TestSpecModel::canMoveDown(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }
    
    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    Feature* feature = dynamic_cast<Feature*>(item);
    FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);

    if (testcase) {
        return item->getIndexNumber() < testcase->getParent()->getTestcaseList().size()-1;

    } else if (linkedTestcase) {
        return item->getIndexNumber() < linkedTestcase->getParent()->getTestcaseList().size()-1;

    } else if (feature) {
        return item->getIndexNumber() < feature->getParent()->getFeatureList().size()-1;

    } else if (featureGroup) {
        return item->getIndexNumber() <
                featureGroup->getParent()->getFeatureGroupList().size()-1;

    } else {
        return false;
    }
}

FeatureGroup* TestSpecModel::getFeatureGroup(const QModelIndex& current) const {
    if (!current.isValid()) {
        return NULL;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    return dynamic_cast<FeatureGroup*>(item);
}
Feature* TestSpecModel::getFeature(const QModelIndex& current) const {
    if (!current.isValid()) {
        return NULL;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    return dynamic_cast<Feature*>(item);
}
Testcase* TestSpecModel::getTestcase(const QModelIndex& current) const {
    if (!current.isValid()) {
        return NULL;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    return dynamic_cast<Testcase*>(item);
}
LinkedTestcase* TestSpecModel::getLinkedTestcase(const QModelIndex& current) const {
    if (!current.isValid()) {
        return NULL;
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(current.internalPointer());
    return dynamic_cast<LinkedTestcase*>(item);
}

QModelIndex TestSpecModel::findTestcase(Testcase* testcase) const {
    if (!testcase) {
        return QModelIndex();
    }
    Feature* feature = testcase->getParent();
    FeatureGroup* featureGroup = feature->getParent();

    for (int i = 0; i < rowCount(); ++i) {
        QModelIndex fgIndex = index(i, 0);
        if (fgIndex.internalPointer() != featureGroup) {
            continue;
        }
        for (int j = 0; j < rowCount(fgIndex); ++j) {
            QModelIndex fIndex = index(j, 0, fgIndex);
            if (fIndex.internalPointer() != feature) {
                continue;
            }
            for (int k = 0; k < rowCount(fIndex); ++k) {
                QModelIndex tIndex = index(k, 0, fIndex);
                if (tIndex.internalPointer() == testcase) {
                    return tIndex;
                }
            }
            break;
        }
        break;
    }
    return QModelIndex();
}

int	TestSpecModel::columnCount(const QModelIndex& /*parent*/) const {
    return 3;
}
QVariant TestSpecModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid() || !index.internalPointer()) {
        return QVariant();
    }

    TestSpecItem* item = static_cast<TestSpecItem*>(index.internalPointer());
    if (role == Qt::DisplayRole) {
        FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
        Feature* feature = dynamic_cast<Feature*>(item);
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);

        if (featureGroup) {
            if (index.column() == 0) {
                return featureGroup->getName();
            }
        } else if (feature) {
            if (index.column() == 0) {
                if (!feature->getId().isEmpty()) {
                    return feature->getId()+"."+feature->getName();
                } else {
                    return feature->getName();
                }
            }
        } else if (testcase) {
            if (index.column() == 0) {
                return testcase->getId();
            } else if (index.column() == 1) {
                return QString("%1/%2").arg(testcase->getExecutedStep())
                        .arg(testcase->getStepList().size());
            } else if (index.column() == 2) {
                return testcase->getStepState();
            }
        } else if (linkedTestcase) {
            testcase = linkedTestcase->getTestcase();
            if (index.column() == 0) {
                return linkedTestcase->getTestcaseId();
            } else if (testcase && index.column() == 1) {
                return QString("%1/%2").arg(testcase->getExecutedStep())
                        .arg(testcase->getStepList().size());
            } else if (testcase && index.column() == 2) {
                return testcase->getStepState();
            }
        }

        return "";

    } else if (role == Qt::BackgroundRole) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
        if (linkedTestcase) {
            testcase = linkedTestcase->getTestcase();
        }
        if (testcase) {
            QString state = testcase->getStepState();
            if (state == "failed") {
                return QBrush(QColor(Qt::red));
            } else if (state == "passed") {
                return QBrush(QColor(Qt::green));
            }
        }
    } else if (role == Qt::ForegroundRole) {
        FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
        Feature* feature = dynamic_cast<Feature*>(item);
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);

        if (featureGroup && !featureGroup->isEnabled()) {
            return QBrush(QColor(Qt::darkGray));

        } else if (feature && !feature->isEnabled()) {
            return QBrush(QColor(Qt::darkGray));

        } else if (testcase && !testcase->isEnabled()) {
            return QBrush(QColor(Qt::darkGray));

        } else if (linkedTestcase && !linkedTestcase->isEnabled()) {
            return QBrush(QColor(Qt::darkGray));
        }

    } else if (role == Qt::FontRole) {
        if (index.column() == 0) {
            FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
            Feature* feature = dynamic_cast<Feature*>(item);
            Testcase* testcase = dynamic_cast<Testcase*>(item);

            QFont font;

            if (featureGroup) {
                font.setItalic(featureGroup->isSpecChanged() ||
                               (featureGroup->isReportChanged() && testSpec->hasReport()));
            } else if (feature) {
                font.setItalic(feature->isSpecChanged() ||
                               (feature->isReportChanged() && testSpec->hasReport()));
            } else if (testcase) {
                font.setItalic(testcase->isSpecChanged() ||
                               (testcase->isReportChanged() && testSpec->hasReport()));
            }
            return font;
        }
    } else if (role == Qt::ToolTipRole) {
        FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
        Feature* feature = dynamic_cast<Feature*>(item);
        Testcase* testcase = dynamic_cast<Testcase*>(item);

        bool specChanged = false;
        bool reportChanged = false;

        if (featureGroup) {
            specChanged = featureGroup->isSpecChanged();
            reportChanged = featureGroup->isReportChanged() && testSpec->hasReport();
        } else if (feature) {
            specChanged = feature->isSpecChanged();
            reportChanged = feature->isReportChanged() && testSpec->hasReport();
        } else if (testcase) {
            specChanged = testcase->isSpecChanged();
            reportChanged = testcase->isReportChanged() && testSpec->hasReport();
        }

        if (specChanged && reportChanged) {
            return "Spec & Report changed";
        } else if (specChanged) {
            return "Spec changed";
        } else if (reportChanged) {
            return "Report changed";
        } else {
            return "";
        }

    }
    return QVariant();
}
QVariant TestSpecModel::headerData(int section, Qt::Orientation /*orientation*/,
                                   int role) const {
    if (role == Qt::DisplayRole) {
        switch (section) {
            case 0:
                return tr("Testcase");
            case 1:
                return tr("Steps");
            case 2:
                return tr("State");
            default:
                return QVariant();
        }
    }
    return QVariant();
}
QModelIndex TestSpecModel::index(int row, int column,
                                 const QModelIndex &parent) const {
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    if (parent.isValid()) {
        TestSpecItem* item = static_cast<TestSpecItem*>(parent.internalPointer());
        FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
        if (featureGroup) {
            return createIndex(row, column, featureGroup->getFeature(row));
        }
        Feature* feature = dynamic_cast<Feature*>(item);
        if (feature) {
            return createIndex(row, column, feature->getTestcase(row));
        }
        return QModelIndex();
    } else {
        return createIndex(row, column, testSpec->getFeatureGroup(row));
    }
}
QModelIndex TestSpecModel::parent(const QModelIndex &index) const {
    if (!index.isValid())
        return QModelIndex();

    TestSpecItem* item = static_cast<TestSpecItem*>(index.internalPointer());
    Feature* feature = dynamic_cast<Feature*>(item);
    if (feature) {
        FeatureGroup* parent = feature->getParent();
        return createIndex(parent->getIndexNumber(), 0, parent);
    }
    Testcase* testcase = dynamic_cast<Testcase*>(item);
    if (testcase) {
        Feature* parent = testcase->getParent();
        return createIndex(parent->getIndexNumber(), 0, parent);
    }
    LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
    if (linkedTestcase) {
        Feature* parent = linkedTestcase->getParent();
        return createIndex(parent->getIndexNumber(), 0, parent);
    }
    return QModelIndex();
}
int TestSpecModel::rowCount(const QModelIndex &parent) const {
    if (parent.isValid()) {
        TestSpecItem* item = static_cast<TestSpecItem*>(parent.internalPointer());
        FeatureGroup* featureGroup = dynamic_cast<FeatureGroup*>(item);
        if (featureGroup) {
            return featureGroup->getFeatureList().size();
        }
        Feature* feature = dynamic_cast<Feature*>(item);
        if (feature) {
            return feature->getTestcaseList().size();
        }

        return 0;
    } else {
        return testSpec->getFeatureGroupList().size();
    }
}
