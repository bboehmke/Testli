/**
 * @file MainWindow.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "MainWindow.h"

#include <QMessageBox>
#include <QFileInfo>
#include <QDir>
#include <QFileDialog>

#include "Exception.h"
#include "misc/XmlHelper.h"
#include "Settings.h"

MainWindow::MainWindow(const QString& projectFile) :
        testSpec(NULL), 
        testSpecModel(NULL), requirementsModel(NULL), testStepsModel(NULL),
        currentFeatures(NULL), currentFeature(NULL), currentTestcase(NULL),
        projectChanged(false), reportChanged(false), projectReportMode(true) {
    ui.setupUi(this);

    ui.splitter_spec->setSizes({350, 850});
    ui.splitter_test->setSizes({150, 550});
    ui.splitter_test_desc->setSizes({500, 250});
    ui.splitter_step->setSizes({100, 700});
    ui.splitter_step_desc->setSizes({200, 200});

    Settings::init();

    // create dialogs
    createProjectDialog = new CreateProjectDialog(this);
    createFeatureGroupDialog = new CreateFeatureGroupDialog(this);
    createFeatureDialog = new CreateFeatureDialog(this);
    createTestcaseDialog = new CreateTestcaseDialog(this);
    textEditDialog = new TextEditDialog(this);
    editFeatureGroupDialog = new EditFeatureGroupDialog(this);
    editFeatureDialog = new EditFeatureDialog(this);
    editTestcaseDialog = new EditTestcaseDialog(this);
    editStepDialog = new EditStepDialog(this);
    projectSettingsDialog = new ProjectSettingsDialog(this);
    applicationSettingsDialog = new ApplicationSettingsDialog(this);
    testExecutionDialog = new TestExecutionDialog(this);
    stepLoggerDialog = new StepLoggerDialog(this);
    createRequirementDialog = new CreateRequirementDialog(this);
    createLinkedTestcaseDialog = new CreateLinkedTestcaseDialog(this);
    editLinkedTestcaseDialog = new EditLinkedTestcaseDialog(this);
    aboutDialog = new AboutDialog(this);

    connect(createProjectDialog, SIGNAL(createProject(QString, QString)),
            this, SLOT(createProject(QString, QString)));
    connect(createFeatureGroupDialog, SIGNAL(createFeatureGroup(QString)),
            this, SLOT(createFeatures(QString)));
    connect(createFeatureDialog, SIGNAL(createFeature(QString, QString)),
            this, SLOT(createFeature(QString, QString)));
    connect(createTestcaseDialog, SIGNAL(createTestcase(QString)),
            this, SLOT(createTestcase(QString)));
    connect(textEditDialog, SIGNAL(textEdited(QString, QObject*)),
            this, SLOT(textEdited(QString, QObject*)));
    connect(editFeatureGroupDialog, SIGNAL(editFeatureGroup(QString)),
            this, SLOT(editFeatureGroup(QString)));
    connect(editFeatureDialog, SIGNAL(editFeature(QString, QString)),
            this, SLOT(editFeature(QString, QString)));
    connect(editTestcaseDialog, SIGNAL(editTestcase(QString, QString)),
            this, SLOT(editTestcase(QString, QString)));
    connect(editStepDialog, SIGNAL(editStep(QString)),
            this, SLOT(editStep(QString)));
    connect(projectSettingsDialog, SIGNAL(projectSettingsChanged()),
            this, SLOT(projectSettingsChanged()));
    connect(createRequirementDialog, SIGNAL(createRequirement(QString)),
            this, SLOT(createRequirement(QString)));
    connect(createLinkedTestcaseDialog, SIGNAL(createLinkedTestcase(QString, QString)),
            this, SLOT(createLinkedTestcase(QString, QString)));
    connect(editLinkedTestcaseDialog, SIGNAL(editLinkedTestcase(QString, QString)),
            this, SLOT(editLinkedTestcase(QString, QString)));

    connect(testExecutionDialog, SIGNAL(accepted()),
            this, SLOT(updateGuiState()));

    // create menus
    QMenu* testSpecAddMenu = new QMenu(ui.specAddButton);
    testSpecAddMenu->addAction("Add Features", this, SLOT(on_specAddFeatures()));
    addFeatureAction = testSpecAddMenu->addAction(
            "Add Feature", this, SLOT(on_specAddFeature()));
    addTestcaseAction = testSpecAddMenu->addAction(
            "Add Testcase", this, SLOT(on_specAddTestcase()));
    addLinkedTestcaseAction = testSpecAddMenu->addAction(
            "Add Linked Testcase", this, SLOT(on_specAddLinkedTestcase()));
    ui.specAddButton->setMenu(testSpecAddMenu);

    QMenu* testStepAddMenu = new QMenu(ui.stepAddButton);
    testStepAddMenu->addAction("Add Precondition", this, SLOT(on_stepAddPrecondition()));
    testStepAddMenu->addAction("Add Step", this, SLOT(on_stepAddStep()));
    testStepAddMenu->addAction("Add Postcondition", this, SLOT(on_stepAddPostcondition()));
    ui.stepAddButton->setMenu(testStepAddMenu);

    testListMenu = new QMenu(ui.testList);
    testListMenu->addAction("Collapse all", ui.testList, SLOT(collapseAll()));
    testListMenu->addAction("Expand all", ui.testList, SLOT(expandAll()));
    testListMenu->addSeparator();
    testListMenu->addAction("Enable", this, SLOT(on_testEnable()));
    testListMenu->addAction("Disable", this, SLOT(on_testDisable()));
    testListMenu->addAction("Enable All", this, SLOT(on_testEnableAll()));
    testListMenu->addAction("Disable All", this, SLOT(on_testDisableAll()));

    testStepListMenu = new QMenu(ui.stepList);
    testStepListMenu->addAction("Set Passed", this, SLOT(on_stepSetPassed()));
    testStepListMenu->addAction("Remove result", this, SLOT(on_stepRemoveResult()));

    // create event filter
    ui.testComment->installEventFilter(this);
    ui.testDescription->installEventFilter(this);
    ui.stepDescription->installEventFilter(this);
    ui.stepExpectedResult->installEventFilter(this);
    ui.testResultNote->installEventFilter(this);

    // disable GUI
    updateGuiState();

    if (!projectFile.isEmpty()) {
        loadProject(projectFile);
    }
}
MainWindow::~MainWindow() {
    delete(createProjectDialog);
    delete(createFeatureGroupDialog);
    delete(createFeatureDialog);
    delete(createTestcaseDialog);
    delete(textEditDialog);
    delete(editFeatureGroupDialog);
    delete(editFeatureDialog);
    delete(editTestcaseDialog);
    delete(editStepDialog);
    delete(projectSettingsDialog);
    delete(applicationSettingsDialog);
    delete(testExecutionDialog);
    delete(stepLoggerDialog);
    delete(createRequirementDialog);
    delete(createLinkedTestcaseDialog);
    delete(editLinkedTestcaseDialog);
    delete(aboutDialog);

    Settings::clear();
}

// main menu
void MainWindow::on_actionNew_triggered() {
    if (closeProject()) {
        createProjectDialog->show();
    }
}
void MainWindow::on_actionOpen_triggered() {
    QString lastProject = projectFile;
    if (closeProject()) {
        if (lastProject.isEmpty()) {
            lastProject = Settings::getApplicationValue("last_file/project").toString();
        }
        QString fileName = QFileDialog::getOpenFileName(
                this, tr("Open Project"), lastProject, tr("XML file (*.xml)"),
                NULL, QFileDialog::DontUseNativeDialog);

        if (!fileName.isEmpty()) {
            Settings::setApplicationValue("last_file/project", fileName);
            loadProject(fileName);
        }
    }
}
void MainWindow::on_actionSave_triggered() {
    try {
        saveProject();
    } catch (Exception& e) {
        QMessageBox::critical(this, "Error",
                              e.getTypeStr() + " - " + e.getMessage());
    }
}
void MainWindow::on_actionSettings_triggered() {
    applicationSettingsDialog->showDialog();
}
void MainWindow::on_actionProjectSettings_triggered() {
    projectSettingsDialog->showDialog();
}
void MainWindow::on_actionSelectReport_triggered() {
    if (testSpec) {
        QString lastReport = testSpec->getReportPath();
        if (lastReport.isEmpty()) {
            lastReport = Settings::getApplicationValue("last_file/report").toString();
        }
        QString file = QFileDialog::getSaveFileName(
                this, tr("Select test report"),
                lastReport, tr("XML file (*.xml)"),
                NULL, QFileDialog::DontConfirmOverwrite | QFileDialog::DontUseNativeDialog);
        if (!file.isEmpty()) {
            if (!file.endsWith(".xml")) {
                file = file + ".xml";
            }
            Settings::setApplicationValue("last_file/report", file);
            try {
                testSpec->loadReport(file);
            } catch (Exception& e) {
                QMessageBox::critical(this, "Error",
                                      e.getTypeStr() + " - " + e.getMessage());
            }

        }
    }
}
void MainWindow::on_actionRemoveReport_triggered() {
    if (testSpec) {
        if (testSpec->isReportChanged() || reportChanged) {
            QMessageBox saveAsk(QMessageBox::Warning, "Save",
                                "Save changes to disk?", QMessageBox::NoButton,
                                this);
            saveAsk.addButton(QMessageBox::Save);
            saveAsk.addButton(QMessageBox::Cancel);
            saveAsk.addButton(QMessageBox::Discard);

            int button = saveAsk.exec();
            if (button == QMessageBox::Cancel) {
                return;
            } else if (button == QMessageBox::Save) {
                try {
                    saveProject();
                } catch (Exception& e) {
                    QMessageBox::critical(this, "Error",
                                          e.getTypeStr() + " - " + e.getMessage());
                    return;
                }
            }
        }
        testSpec->removeReport();
        updateGuiState();
    }
}
void MainWindow::on_actionClearReport_triggered() {
    if (testSpec) {
        QMessageBox::StandardButton button =
                QMessageBox::question(this, "Clear Report", "Remove report results?");

        if (button == QMessageBox::Yes) {
            testSpec->clearReport();
        }
    }
}
void MainWindow::on_actionOpenRequirements_triggered() {
    if (testSpec) {
        QString lastRequirements = requirementsFile;
        if (lastRequirements.isEmpty()) {
            lastRequirements = Settings::getApplicationValue("last_file/requirements").toString();
        }
        QString file = QFileDialog::getOpenFileName(
                this, tr("Open requirements list"),
                lastRequirements, tr("XML file (*.xml)"),
                NULL, QFileDialog::DontUseNativeDialog);
        if (file.isEmpty()) {
            return;
        }
        Settings::setApplicationValue("last_file/requirements", file);

        try {
            loadRequirements(file);
        } catch (Exception& e) {
            QMessageBox::critical(this, "Error",
                                  e.getTypeStr() + " - " + e.getMessage());
        }
    }
}
void MainWindow::on_actionModeSpec_triggered() {
    ui.actionModeReport->setChecked(!ui.actionModeSpec->isChecked());
    projectReportMode = ui.actionModeReport->isChecked();
    updateGuiState();
    projectChanged = true;
}
void MainWindow::on_actionModeReport_triggered() {
    ui.actionModeSpec->setChecked(!ui.actionModeReport->isChecked());
    projectReportMode = ui.actionModeReport->isChecked();
    updateGuiState();
    projectChanged = true;
}
void MainWindow::on_actionAbout_triggered() {
    aboutDialog->show();
}
void MainWindow::on_actionAboutQt_triggered() {
    QMessageBox::aboutQt(this);
}

// test spec
void MainWindow::on_specAddFeatures() {
    createFeatureGroupDialog->show();
}
void MainWindow::on_specAddFeature() {
    createFeatureDialog->show();
}
void MainWindow::on_specAddTestcase() {
    createTestcaseDialog->show();
}
void MainWindow::on_specAddLinkedTestcase() {
    createLinkedTestcaseDialog->show();
}
void MainWindow::on_specRemoveButton_clicked() {
    int button = QMessageBox::warning(this, tr("Remove element"),
                                      tr("The the selected element will be remove and can not be restored!"),
                                      QMessageBox::Ok, QMessageBox::Cancel);
    if (button == QMessageBox::Ok) {
        QModelIndex index = ui.testList->selectionModel()->currentIndex();
        testSpecModel->remove(index);
    }
}
void MainWindow::on_specUpButton_clicked() {
    QModelIndex index = ui.testList->selectionModel()->currentIndex();
    testSpecModel->moveUp(index);
}
void MainWindow::on_specDownButton_clicked() {
    QModelIndex index = ui.testList->selectionModel()->currentIndex();
    testSpecModel->moveDown(index);
}
void MainWindow::on_specEditButton_clicked() {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    FeatureGroup* featureGroup = testSpecModel->getFeatureGroup(currentTest);
    if (featureGroup) {
        editFeatureGroupDialog->showDialog(featureGroup->getName());
    }
    Feature* feature = testSpecModel->getFeature(currentTest);
    if (feature) {
        editFeatureDialog->showDialog(feature->getName(), feature->getId());
    }
    Testcase* testcase = testSpecModel->getTestcase(currentTest);
    if (testcase) {
        editTestcaseDialog->showDialog(testcase->getId(), testcase->getState());
    }
    LinkedTestcase* linkedTestcase = testSpecModel->getLinkedTestcase(currentTest);
    if (linkedTestcase) {
        editLinkedTestcaseDialog->showDialog(linkedTestcase->getFeatureId(),
                                             linkedTestcase->getTestcaseId());
    }
}
void MainWindow::on_testList_doubleClicked(const QModelIndex& current) {
    LinkedTestcase* linkedTestcase = testSpecModel->getLinkedTestcase(current);
    if (linkedTestcase) {
        Testcase* tc = linkedTestcase->getTestcase();
        if (tc) {
            QModelIndex index = testSpecModel->findTestcase(tc);
            if (index.isValid()) {
                ui.testList->setCurrentIndex(index);
            }
        }
    }
}
void MainWindow::on_testList_customContextMenuRequested(const QPoint &pos) {
    if (!testSpec || testSpec->getFeatureGroupList().isEmpty()) {
        return;
    }
    QPoint point = ui.testList->mapToGlobal(pos);
    testListMenu->exec(point);
}
void MainWindow::on_testEnable() {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    FeatureGroup* featureGroup = testSpecModel->getFeatureGroup(currentTest);
    if (featureGroup) {
        projectChanged = true;
        reportChanged = true;
        featureGroup->setEnabled(true);
    }
    Feature* feature = testSpecModel->getFeature(currentTest);
    if (feature) {
        projectChanged = true;
        reportChanged = true;
        feature->setEnabled(true);
    }
    Testcase* testcase = testSpecModel->getTestcase(currentTest);
    if (testcase) {
        projectChanged = true;
        reportChanged = true;
        testcase->setEnabled(true);
    }
    LinkedTestcase* linkedTestcase = testSpecModel->getLinkedTestcase(currentTest);
    if (linkedTestcase) {
        projectChanged = true;
        reportChanged = true;
        linkedTestcase->setEnabled(true);
    }
    updateGuiState();
}
void MainWindow::on_testDisable() {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    FeatureGroup* featureGroup = testSpecModel->getFeatureGroup(currentTest);
    if (featureGroup) {
        projectChanged = true;
        reportChanged = true;
        featureGroup->setEnabled(false);
    }
    Feature* feature = testSpecModel->getFeature(currentTest);
    if (feature) {
        projectChanged = true;
        reportChanged = true;
        feature->setEnabled(false);
    }
    Testcase* testcase = testSpecModel->getTestcase(currentTest);
    if (testcase) {
        projectChanged = true;
        reportChanged = true;
        testcase->setEnabled(false);
    }
    LinkedTestcase* linkedTestcase = testSpecModel->getLinkedTestcase(currentTest);
    if (linkedTestcase) {
        projectChanged = true;
        reportChanged = true;
        linkedTestcase->setEnabled(false);
    }
    updateGuiState();
}
void MainWindow::on_testEnableAll() {
    for(FeatureGroup* group : testSpec->getFeatureGroupList()) {
        group->setEnabled(true);
    }
    updateGuiState();
}
void MainWindow::on_testDisableAll() {
    for(FeatureGroup* group : testSpec->getFeatureGroupList()) {
        group->setEnabled(false);
    }
    updateGuiState();
}
void MainWindow::testList_currentChanged(const QModelIndex& current) {

    Testcase* testcase = testSpecModel->getTestcase(current);
    if (testcase) {
        // set testcase content
        ui.testComment->clear();
        ui.testComment->setHtml(
                TextEditDialog::docBookToHtml(testcase->getComment()));
        ui.testDescription->clear();
        ui.testDescription->setHtml(
                TextEditDialog::docBookToHtml(testcase->getDescription()));


        if (testcase->getDescription().isEmpty() &&
            !testcase->getComment().isEmpty()) {
            ui.testTextTabs->setCurrentWidget(ui.comTab);
        } else {
            ui.testTextTabs->setCurrentWidget(ui.descTab);
        }

        QStringListModel* rModel = new QStringListModel(testcase->getRequirements());
        if (requirementsModel) {
            QItemSelectionModel* selectionModel = ui.requirementList->selectionModel();
            ui.requirementList->setModel(rModel);
            delete(selectionModel);
            delete(requirementsModel);
        } else {
            ui.requirementList->setModel(rModel);
        }
        requirementsModel = rModel;

        connect(ui.requirementList->selectionModel(),
                &QItemSelectionModel::currentChanged,
                this,
                &MainWindow::requirementList_currentChanged);

        TestStepsModel* model = new TestStepsModel(testcase);
        if (testStepsModel) {
            QItemSelectionModel* selectionModel = ui.stepList->selectionModel();
            ui.stepList->setModel(model);
            delete(selectionModel);
            delete(testStepsModel);
        } else {
            ui.stepList->setModel(model);
        }
        testStepsModel = model;

        connect(ui.stepList->selectionModel(),
                SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
                this,
                SLOT(stepList_currentChanged(const QModelIndex&)));
    }
    updateGuiState();
}

// test steps
void MainWindow::on_stepAddPrecondition() {
    QModelIndex index = ui.stepList->selectionModel()->currentIndex();
    QModelIndex newIndex = testStepsModel->insertStep(index, Step::Pre);

    if (newIndex.isValid()) {
        ui.stepList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    }
}
void MainWindow::on_stepAddStep() {
    QModelIndex index = ui.stepList->selectionModel()->currentIndex();
    QModelIndex newIndex = testStepsModel->insertStep(index, Step::Main);

    if (newIndex.isValid()) {
        ui.stepList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    }
}
void MainWindow::on_stepAddPostcondition() {
    QModelIndex index = ui.stepList->selectionModel()->currentIndex();
    QModelIndex newIndex = testStepsModel->insertStep(index, Step::Post);

    if (newIndex.isValid()) {
        ui.stepList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    }
}
void MainWindow::on_stepRemoveButton_clicked() {
    int button = QMessageBox::warning(this, tr("Remove Step"),
                                      tr("The the selected step will be remove and can not be restored!"),
                                      QMessageBox::Ok, QMessageBox::Cancel);
    if (button == QMessageBox::Ok) {
        QModelIndex index = ui.stepList->selectionModel()->currentIndex();
        testStepsModel->remove(index);
    }
}
void MainWindow::on_stepUpButton_clicked() {
    QModelIndex index = ui.stepList->selectionModel()->currentIndex();
    testStepsModel->moveUp(index);
}
void MainWindow::on_stepDownButton_clicked() {
    QModelIndex index = ui.stepList->selectionModel()->currentIndex();
    testStepsModel->moveDown(index);
}
void MainWindow::on_stepEditButton_clicked() {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (step) {
        editStepDialog->showDialog(step->getTitle());
    }
}
void MainWindow::on_stepExecutionButton_clicked() {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (step) {
        testExecutionDialog->showDialog(step, projectReportMode);
    }
}
void MainWindow::on_loggerButton_clicked() {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (step) {
        stepLoggerDialog->showDialog(step);
    }
}
void MainWindow::on_testResultState_currentTextChanged(const QString& text) {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (step) {
        if (step->getResultState() != text) {
            step->setResultState(text);
        }
    }
}
void MainWindow::on_stepList_customContextMenuRequested(const QPoint &pos) {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (!step) {
        return;
    }
    QPoint point = ui.stepList->mapToGlobal(pos);
    testStepListMenu->exec(point);
}
void MainWindow::on_stepSetPassed() {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (step) {
        step->setResultState("passed");
    }
}
void MainWindow::on_stepRemoveResult() {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (!step) {
        return;
    }
    QMessageBox ask(QMessageBox::Warning, "Remove step result",
                    "Remove test step results permanently?", QMessageBox::NoButton,
                    this);
    ask.addButton(QMessageBox::Yes);
    ask.addButton(QMessageBox::No);

    if (ask.exec() == QMessageBox::No) {
        return;
    }

    step->removeReport();
    stepList_currentChanged(current);
}
void MainWindow::stepList_currentChanged(const QModelIndex& current) {

    Step* step = testStepsModel->getStep(current);
    if (step) {
        ui.stepDescription->setHtml(
                TextEditDialog::docBookToHtml(step->getDescription()));
        ui.stepExpectedResult->setHtml(
                TextEditDialog::docBookToHtml(step->getExpectedResult()));

        if (!testSpec->getReportPath().isEmpty()) {
            ui.testResultNote->setHtml(
                    TextEditDialog::docBookToHtml(step->getResultNote()));
            if (step->getResultState().isEmpty()) {
                ui.testResultState->setCurrentIndex(0);
            } else {
                ui.testResultState->setCurrentText(step->getResultState());
            }
        } else {
            ui.testResultState->setCurrentIndex(0);
        }
    }
    updateGuiState();
}

void MainWindow::on_requirementAddButton_clicked() {
    createRequirementDialog->show();
}
void MainWindow::on_requirementRemoveButton_clicked() {
    if (!requirementsModel) {
        return;
    }
    QModelIndex currentTestcase =
            ui.testList->selectionModel()->currentIndex();
    Testcase* testcase = testSpecModel->getTestcase(currentTestcase);
    QModelIndex current = ui.requirementList->selectionModel()->currentIndex();
    if (!testcase || !current.isValid()) {
        return;
    }
    QString req = requirementsModel->data(current, Qt::DisplayRole).toString();
    testcase->removeRequirement(req);
    requirementsModel->removeRow(current.row());
}
void MainWindow::requirementList_currentChanged() {
    updateGuiState();
}

void MainWindow::createProject(QString project, QString spec) {
    projectFile = project;
    projectChanged = true;
    loadTestSpec(spec, QStringList());
    Settings::loadProject(QDomElement(), spec);
    updateGuiState();
}
void MainWindow::createFeatures(QString name) {
    QModelIndex index = ui.testList->selectionModel()->currentIndex();
    QModelIndex newIndex = testSpecModel->insertFeatureGroup(index, name);

    if (newIndex.isValid()) {
        ui.testList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        ui.testList->resizeColumnToContents(0);
    }
}
void MainWindow::createFeature(QString name, QString id) {
    QModelIndex index = ui.testList->selectionModel()->currentIndex();
    QModelIndex newIndex = testSpecModel->insertFeature(index, name, id);

    if (newIndex.isValid()) {
        ui.testList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        ui.testList->resizeColumnToContents(0);
    }
}
void MainWindow::createTestcase(QString id) {
    QModelIndex index = ui.testList->selectionModel()->currentIndex();
    QModelIndex newIndex = testSpecModel->insertTestcase(index, id);

    if (newIndex.isValid()) {
        ui.testList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        ui.testList->resizeColumnToContents(0);
    }
}
void MainWindow::createLinkedTestcase(QString featureId, QString testcaseId) {
    QModelIndex index = ui.testList->selectionModel()->currentIndex();
    QModelIndex newIndex = testSpecModel->insertLinkedTestcase(index,
                                                               featureId,
                                                               testcaseId);

    if (newIndex.isValid()) {
        ui.testList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
        ui.testList->resizeColumnToContents(0);
    }
}
void MainWindow::textEdited(QString text, QObject* ref) {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    QModelIndex currentStep = ui.stepList->selectionModel()->currentIndex();
    if (ref == ui.testComment) {
        Testcase* testcase = testSpecModel->getTestcase(currentTest);
        if (testcase) {
            testcase->setComment(text);
            ui.testComment->setHtml(
                    TextEditDialog::docBookToHtml(testcase->getComment()));
            updateGuiState();
        }

    } else if (ref == ui.testDescription) {
        Testcase* testcase = testSpecModel->getTestcase(currentTest);
        if (testcase) {
            testcase->setDescription(text);
            ui.testDescription->setHtml(
                    TextEditDialog::docBookToHtml(testcase->getDescription()));
            updateGuiState();
        }

    } else if (ref == ui.stepDescription) {
        Step* step = testStepsModel->getStep(currentStep);
        if (step) {
            step->setDescription(text);
            ui.stepDescription->setHtml(
                    TextEditDialog::docBookToHtml(step->getDescription()));
            updateGuiState();
        }

    } else if (ref == ui.stepExpectedResult) {
        Step* step = testStepsModel->getStep(currentStep);
        if (step) {
            step->setExpectedResult(text);
            ui.stepExpectedResult->setHtml(
                    TextEditDialog::docBookToHtml(step->getExpectedResult()));
            updateGuiState();
        }

    } else if (ref == ui.testResultNote) {
        Step* step = testStepsModel->getStep(currentStep);
        if (step) {
            step->setResultNote(text);
            ui.testResultNote->setHtml(
                    TextEditDialog::docBookToHtml(step->getResultNote()));
            updateGuiState();
        }
    }
}
void MainWindow::editFeatureGroup(QString name) {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    FeatureGroup* featureGroup = testSpecModel->getFeatureGroup(currentTest);
    if (featureGroup) {
        featureGroup->setName(name);
    }
}
void MainWindow::editFeature(QString name, QString id) {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    Feature* feature = testSpecModel->getFeature(currentTest);
    if (feature) {
        feature->setName(name);
        feature->setId(id);
    }
}
void MainWindow::editTestcase(QString id, QString state) {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    Testcase* testcase = testSpecModel->getTestcase(currentTest);
    if (testcase) {
        testcase->setId(id);
        testcase->setState(state);
        updateGuiState();
    }
}
void MainWindow::editLinkedTestcase(QString featureId, QString testcaseId) {
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    LinkedTestcase* linkedTestcase = testSpecModel->getLinkedTestcase(currentTest);
    if (linkedTestcase) {
        linkedTestcase->setFeatureId(featureId);
        linkedTestcase->setTestcaseId(testcaseId);
    }
}
void MainWindow::editStep(QString title) {
    QModelIndex current = ui.stepList->selectionModel()->currentIndex();
    Step* step = testStepsModel->getStep(current);
    if (step) {
        step->setTitle(title);
    }
}
void MainWindow::projectSettingsChanged() {
    projectChanged = true;
}
void MainWindow::createRequirement(QString id) {
    if (!requirementsModel) {
        return;
    }
    QModelIndex currentTestcase =
            ui.testList->selectionModel()->currentIndex();
    Testcase* testcase = testSpecModel->getTestcase(currentTestcase);
    testcase->addRequirement(id);
    requirementsModel->setStringList(testcase->getRequirements());
}

void MainWindow::updateGuiState() {
    if (testSpec) {
        ui.menuProject->setEnabled(true);
        ui.testList->setEnabled(true);

        // spec
        QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();

        ui.specAddButton->setEnabled(!projectReportMode);
        addFeatureAction->setEnabled(testSpecModel->canInsertFeature(currentTest));
        addTestcaseAction->setEnabled(testSpecModel->canInsertTestcase(currentTest));
        addLinkedTestcaseAction->setEnabled(testSpecModel->canInsertTestcase(currentTest));
        ui.specRemoveButton->setEnabled(!projectReportMode && testSpecModel->canRemove(currentTest));
        ui.specUpButton->setEnabled(!projectReportMode && testSpecModel->canMoveUp(currentTest));
        ui.specDownButton->setEnabled(!projectReportMode && testSpecModel->canMoveDown(currentTest));
        // condition of edit & add feature are the same
        ui.specEditButton->setEnabled(!projectReportMode && testSpecModel->canInsertFeature(currentTest));

        // report actions
        ui.actionRemoveReport->setEnabled(testSpec->hasReport());
        ui.actionClearReport->setEnabled(testSpec->hasReport());

        Testcase* testcase = testSpecModel->getTestcase(currentTest);
        if (testcase) {
            ui.testComment->setEnabled(true);
            ui.testDescription->setEnabled(true);
            ui.stepList->setEnabled(true);
            ui.requirementList->setEnabled(true);

            // requirements
            QModelIndex currentRequirement = ui.requirementList->selectionModel()->currentIndex();
            ui.requirementAddButton->setEnabled(!projectReportMode);
            ui.requirementRemoveButton->setEnabled(!projectReportMode && currentRequirement.isValid());

            // steps
            QModelIndex currentStep = ui.stepList->selectionModel()->currentIndex();

            ui.stepAddButton->setEnabled(!projectReportMode);
            ui.stepRemoveButton->setEnabled(!projectReportMode && testStepsModel->canRemove(currentStep));
            ui.stepEditButton->setEnabled(!projectReportMode && testStepsModel->canRemove(currentStep));
            ui.stepUpButton->setEnabled(!projectReportMode && testStepsModel->canMoveUp(currentStep));
            ui.stepDownButton->setEnabled(!projectReportMode && testStepsModel->canMoveDown(currentStep));

            // update title
            Feature* feature = testcase->getParent();
            FeatureGroup* featureGroup = feature->getParent();

            // set testcase title
            QString featureName = feature->getName();
            if (!feature->getId().isEmpty()) {
                featureName = feature->getId();
            }
            QString title = QString("%1.%2.%3").arg(featureGroup->getName())
                    .arg(featureName).arg(testcase->getId());

            ui.testcaseGroup->setTitle(
                    title + " - " +
                    testcase->getState() + " - " +
                    testcase->getStateDate().toString("yyyy-MM-dd"));


            Step* step = testStepsModel->getStep(currentStep);
            if (step) {
                bool hasReport = testSpec->getReportPath().isEmpty();
                ui.stepDescription->setEnabled(true);
                ui.stepExpectedResult->setEnabled(true);
                ui.stepExecutionButton->setEnabled(!projectReportMode || testcase->isEnabled());
                ui.testResultNote->setEnabled(!hasReport && testcase->isEnabled());
                ui.testResultState->setEnabled(!hasReport && testcase->isEnabled());
                ui.loggerButton->setEnabled(!hasReport && testcase->isEnabled());

                QString logger = QString(tr("Logger (%1)").arg(step->getLogs().size()));
                ui.loggerButton->setText(logger);

            } else {
                gui_disableStep();
            }
        } else {
            ui.testcaseGroup->setTitle(tr("Testcase"));
            gui_disableTestcase();
        }

    } else {
        ui.menuProject->setEnabled(false);
        gui_disableSpec();
    }

    // update mode check
    ui.actionModeSpec->setChecked(!projectReportMode);
    ui.actionModeReport->setChecked(projectReportMode);
    if (projectReportMode) {
        this->setWindowTitle("Testli - Report Mode");
    } else {
        this->setWindowTitle("Testli - Spec Mode");
    }
}

void MainWindow::closeEvent(QCloseEvent* event) {
    if (!closeProject()) {
        event->ignore();
    }
}
bool MainWindow::eventFilter(QObject* watched, QEvent* event) {
    if (event->type() != QEvent::MouseButtonDblClick) {
        return QMainWindow::eventFilter(watched, event);
    }

    // get selected test case
    QModelIndex currentTest = ui.testList->selectionModel()->currentIndex();
    Testcase* testcase = testSpecModel->getTestcase(currentTest);
    if (!testcase) {
        return QMainWindow::eventFilter(watched, event);
    }
    QMouseEvent* mouseEvent = dynamic_cast<QMouseEvent*>(event);

    if (watched == ui.testComment && ui.testComment->isEnabled() && !projectReportMode) {
        if (mouseEvent->button() & Qt::LeftButton) {
            textEditDialog->showDialog(testcase->getComment(),
                                       ui.testComment);
            return true;
        }

    } else if (watched == ui.testDescription && ui.testDescription->isEnabled() && !projectReportMode) {
        if (mouseEvent->button() & Qt::LeftButton) {
            textEditDialog->showDialog(testcase->getDescription(),
                                       ui.testDescription);
            return true;
        }

    } else if (watched == ui.stepDescription && ui.stepDescription->isEnabled() && !projectReportMode) {
        if (mouseEvent->button() & Qt::LeftButton) {
            QModelIndex currentStep = ui.stepList->selectionModel()->currentIndex();
            Step* step = testStepsModel->getStep(currentStep);
            if (step) {
                textEditDialog->showDialog(step->getDescription(),
                                           ui.stepDescription);
                return true;
            }
        }

    } else if (watched == ui.stepExpectedResult && ui.stepExpectedResult->isEnabled() && !projectReportMode) {
        if (mouseEvent->button() & Qt::LeftButton) {
            QModelIndex currentStep = ui.stepList->selectionModel()->currentIndex();
            Step* step = testStepsModel->getStep(currentStep);
            if (step) {
                textEditDialog->showDialog(step->getExpectedResult(),
                                           ui.stepExpectedResult);
                return true;
            }
        }

    } else if (watched == ui.testResultNote && ui.testResultNote->isEnabled()) {
        if (mouseEvent->button() & Qt::LeftButton) {
            QModelIndex currentStep = ui.stepList->selectionModel()->currentIndex();
            Step* step = testStepsModel->getStep(currentStep);
            if (step) {
                textEditDialog->showDialog(step->getResultNote(),
                                           ui.testResultNote);
                return true;
            }
        }
    }

    return QMainWindow::eventFilter(watched, event);
}

bool MainWindow::closeProject() {
    if (testSpec) {
        if (testSpec->isSpecChanged() || testSpec->isReportChanged() ||
                projectChanged || reportChanged) {
            QMessageBox saveAsk(QMessageBox::Warning, "Save",
                                "Save changes to disk?", QMessageBox::NoButton,
                                this);
            saveAsk.addButton(QMessageBox::Save);
            saveAsk.addButton(QMessageBox::Cancel);
            saveAsk.addButton(QMessageBox::Discard);

            int button = saveAsk.exec();
            if (button == QMessageBox::Cancel) {
                return false;
            } else if (button == QMessageBox::Save) {
                try {
                    saveProject();
                } catch (Exception& e) {
                    QMessageBox::critical(this, "Error",
                                          e.getTypeStr() + " - " + e.getMessage());
                    return false;
                }
            }
        }

        if (testSpecModel) {
            ui.testList->setModel(NULL);
            delete(testSpecModel);
            testSpecModel = NULL;
        }

        delete(testSpec);
        testSpec = NULL;

        updateGuiState();
    }
    return true;
}
void MainWindow::loadTestSpec(const QString& specPath,
                              const QStringList& disabledTests) {
    testSpec = new TestSpec(specPath, disabledTests);

    testSpecModel = new TestSpecModel(testSpec);
    ui.testList->setModel(testSpecModel);

    connect(ui.testList->selectionModel(),
            SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
            this,
            SLOT(testList_currentChanged(const QModelIndex&)));


    ui.testList->setColumnWidth(0, 200);
    ui.testList->setColumnWidth(1, 70);
    updateGuiState();
}

void MainWindow::loadRequirements(const QString& path) {
    QDomElement root = XmlHelper::loadXml(path).documentElement();

    // update requirements
    QStringList requirementList = loadRequirementGroup(root);
    requirementList.sort();
    createRequirementDialog->setRequirementList(requirementList);

    requirementsFile = path;
}
QStringList MainWindow::loadRequirementGroup(const QDomElement& group) const {
    QStringList list;

    for (QDomNode n = group.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == "Requirement" ||
                e.tagName().endsWith(":Requirement")) {
                QString id = e.attribute("Id");
                if (!id.isEmpty()) {
                    list.append(id);
                }
                
            } else if (e.tagName() == "Group" ||
                       e.tagName().endsWith(":Group")) {
                list.append(loadRequirementGroup(e));
            }
        }
    }
    return list;
}

bool MainWindow::loadProject(const QString &projectFile) {
    if (!closeProject()) {
        return false;
    }

    try {
        // get root element
        QDomElement root = XmlHelper::loadXml(projectFile).documentElement();
        if (root.tagName() != "Testli") {
            throw Exception(Exception::XmlError,
                            "Invalid project file");
        }

        QDir baseDir = QFileInfo(projectFile).absoluteDir();

        // disabled test cases
        QDomElement disabledElement = root.firstChildElement("DisabledElements");
        QStringList disabledTests;
        for (QDomElement e = disabledElement.firstChildElement("Element");
             !e.isNull();
             e = e.nextSiblingElement("Element")) {
            disabledTests.append(e.text());
        }

        QDomElement specElement = root.firstChildElement("TestSpec");
        QString specPath = baseDir.filePath(specElement.attribute("Path"));
        loadTestSpec(specPath, disabledTests);
        this->projectFile = projectFile;

        // load project settings
        Settings::loadProject(root.firstChildElement("ProjectSettings"), specPath);

        // try to load project
        QDomElement reportElement = root.firstChildElement("TestReport");
        if (reportElement.hasAttribute("Path")) {

            testSpec->loadReport(baseDir.filePath(reportElement.attribute("Path")));
        }

        projectReportMode = root.attribute("Mode").toLower() == "report";

        QDomElement requirementsElement = root.firstChildElement("Requirements");
        if (!requirementsElement.attribute("Path").isEmpty()) {
            loadRequirements(
                    baseDir.filePath(
                            requirementsElement.attribute("Path")));
        }

    } catch (Exception& e) {
        QMessageBox::critical(this, "Error",
                              e.getTypeStr() + " - " + e.getMessage());
        return false;
    }
    updateGuiState();

    return true;
}
void MainWindow::saveProject() {
    if (!testSpec) {
        return; // nothing to save
    }
    if (testSpec->isReportChanged() || reportChanged) {
        // save test report
        testSpec->saveReport();
        reportChanged = false;
    }
    if (testSpec->isSpecChanged()) {
        // save test spec
        testSpec->saveSpec();
    }

    // save project file
    QDir baseDir = QFileInfo(projectFile).absoluteDir();

    QDomDocument doc;
    doc.appendChild(
            doc.createProcessingInstruction(
                    "xml", "version=\"1.0\" encoding=\"utf-8\""));
    QDomElement root = doc.createElement("Testli");
    doc.appendChild(root);
    if (projectReportMode) {
        root.setAttribute("Mode", "Report");
    } else {
        root.setAttribute("Mode", "Spec");
    }

    QDomElement specElement = doc.createElement("TestSpec");
    specElement.setAttribute("Path",
                             baseDir.relativeFilePath(
                                     testSpec->getSpecPath()));
    root.appendChild(specElement);

    if (!testSpec->getReportPath().isEmpty()) {
        QDomElement reportElement = doc.createElement("TestReport");
        reportElement.setAttribute("Path",
                                 baseDir.relativeFilePath(
                                         testSpec->getReportPath()));
        root.appendChild(reportElement);
    }
    if (!requirementsFile.isEmpty()) {
        QDomElement reportElement = doc.createElement("Requirements");
        reportElement.setAttribute("Path",
                                   baseDir.relativeFilePath(requirementsFile));
        root.appendChild(reportElement);
    }

    QDomElement disabledElement = doc.createElement("DisabledElements");
    for (FeatureGroup* featureGroup : testSpec->getFeatureGroupList()) {
        for (Feature* feature : featureGroup->getFeatureList()) {
            for (TestSpecItem* item : feature->getTestcaseList()) {
                Testcase* testcase = dynamic_cast<Testcase*>(item);
                LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);

                QString name = featureGroup->getName() + "." +
                               feature->getId() + "." +
                               feature->getName();
                if (testcase && !testcase->isEnabled()) {
                    name += "." + testcase->getId();

                    QDomElement tcElement = doc.createElement("Element");
                    tcElement.appendChild(doc.createTextNode(name));
                    disabledElement.appendChild(tcElement);

                } else if (linkedTestcase && !linkedTestcase->isEnabled()) {
                    name += "." + linkedTestcase->getFeatureId() +
                            "." + linkedTestcase->getTestcaseId();

                    QDomElement tcElement = doc.createElement("Element");
                    tcElement.appendChild(doc.createTextNode(name));
                    disabledElement.appendChild(tcElement);
                }
            }
        }
    }
    if (disabledElement.hasChildNodes()) {
        root.appendChild(disabledElement);
    }


    QDomElement settingsElement = doc.createElement("ProjectSettings");
    Settings::saveProject(&settingsElement);
    if (settingsElement.hasChildNodes()) {
        root.appendChild(settingsElement);
    }

    // write to file
    QFile file(projectFile);
    if (Settings::getApplicationValue("Gen/OverwriteReadOnly").toBool()) {
        file.setPermissions(
                file.permissions() | QFile::WriteUser |
                        QFile::WriteOwner | QFile::WriteOther);
    }
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        throw Exception(Exception::FileOpenFailed,
                        "Failed to save file " + projectFile + ": " +
                                file.errorString());
    }
    file.write(doc.toByteArray(4));
    file.close();
    projectChanged = false;
}

void MainWindow::gui_disableSpec() {
    ui.testList->setEnabled(false);

    ui.specAddButton->setEnabled(false);
    ui.specRemoveButton->setEnabled(false);
    ui.specUpButton->setEnabled(false);
    ui.specDownButton->setEnabled(false);
    ui.specEditButton->setEnabled(false);

    addFeatureAction->setEnabled(false);
    addTestcaseAction->setEnabled(false);
    addLinkedTestcaseAction->setEnabled(false);

    gui_disableTestcase();
}
void MainWindow::gui_disableTestcase() {
    ui.testComment->clear();
    ui.testComment->setEnabled(false);
    ui.testDescription->clear();
    ui.testDescription->setEnabled(false);

    ui.requirementAddButton->setEnabled(false);
    ui.requirementRemoveButton->setEnabled(false);
    
    if (requirementsModel) {
        QItemSelectionModel* selectionModel = ui.stepList->selectionModel();
        ui.requirementList->setModel(NULL);
        delete(selectionModel);
        delete(requirementsModel);
        requirementsModel = NULL;
    }
    ui.requirementList->setEnabled(false);

    if (testStepsModel) {
        QItemSelectionModel* selectionModel = ui.stepList->selectionModel();
        ui.stepList->setModel(NULL);
        delete(selectionModel);
        delete(testStepsModel);
        testStepsModel = NULL;
    }

    ui.stepList->setEnabled(false);

    ui.stepAddButton->setEnabled(false);
    ui.stepRemoveButton->setEnabled(false);
    ui.stepUpButton->setEnabled(false);
    ui.stepDownButton->setEnabled(false);
    ui.stepEditButton->setEnabled(false);

    gui_disableStep();
}
void MainWindow::gui_disableStep() {
    ui.stepDescription->clear();
    ui.stepDescription->setEnabled(false);
    ui.stepExpectedResult->clear();
    ui.stepExpectedResult->setEnabled(false);
    ui.stepExecutionButton->setEnabled(false);
    ui.testResultNote->clear();
    ui.testResultNote->setEnabled(false);
    ui.testResultState->setEnabled(false);
    ui.testResultState->setCurrentIndex(0);
    ui.loggerButton->setEnabled(false);
}