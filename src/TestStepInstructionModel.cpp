/**
 * @file TestStepInstructionModel.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestStepInstructionModel.h"

TestStepInstructionModel::TestStepInstructionModel(Step* step) : step(step) {

}

QModelIndex TestStepInstructionModel::insertInstruction(const QModelIndex &current, 
                                                 const QString& name) {
    int pos = step->getInstructionList().size();
    if (current.isValid()) {
        pos = current.row() + 1;
    }

    beginInsertRows(QModelIndex(), pos, pos);
    Instruction* instruction = step->addInstruction(pos, name);
    endInsertRows();
    if (instruction) {
        return createIndex(pos, 0, instruction);
    } else {
        return QModelIndex();
    }
}

bool TestStepInstructionModel::remove(const QModelIndex& current) {
    if (!canRemove(current)) {
        return false;
    }

    beginRemoveRows(QModelIndex(), current.row(), current.row());
    step->removeInstruction(step->getInstruction(current.row()));
    endRemoveRows();
    return true;
}
bool TestStepInstructionModel::canRemove(const QModelIndex& current) const {
    return current.isValid();
}

bool TestStepInstructionModel::moveUp(const QModelIndex& current) {
    if (!canMoveUp(current)) {
        return false;
    }

    beginMoveRows(QModelIndex(), current.row(), current.row(),
                  QModelIndex(), current.row()-1);
    step->moveInstruction(step->getInstruction(current.row()), current.row()-1);
    endMoveRows();
    return true;
}
bool TestStepInstructionModel::moveDown(const QModelIndex& current) {
    if (!canMoveDown(current)) {
        return false;
    }

    beginMoveRows(QModelIndex(), current.row(), current.row(),
                  QModelIndex(), current.row()+2);
    step->moveInstruction(step->getInstruction(current.row()), current.row()+1);
    endMoveRows();
    return true;
}
bool TestStepInstructionModel::canMoveUp(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }

    return current.row() > 0 && 
            current.row() < step->getInstructionList().size();
}
bool TestStepInstructionModel::canMoveDown(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }
    
    return current.row() >= 0 &&
           current.row() < step->getInstructionList().size()-1;
}

Instruction* TestStepInstructionModel::getInstruction(const QModelIndex& current) const {
    if (!current.isValid()) {
        return NULL;
    }

    return step->getInstruction(current.row());
}


QVariant TestStepInstructionModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    Instruction* instruction = step->getInstruction(index.row());
    if (role == Qt::DisplayRole && instruction) {

        if (instruction->isChanged()) {
            return instruction->getName() + " *";
        } else {
            return instruction->getName();
        }
    }
    return QVariant();
}
int TestStepInstructionModel::rowCount(const QModelIndex& /*parent*/) const {
    return step->getInstructionList().size();
}