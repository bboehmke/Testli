/**
 * @file TestStepInstructionModel.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TESTSTEPINSTRUCTIONMODEL_H
#define TESTLI_TESTSTEPINSTRUCTIONMODEL_H

#include <QAbstractListModel>

#include "test_spec/Step.h"

/**
 * Model to interface instructions of a test step
 */
class TestStepInstructionModel : public QAbstractListModel {
    Q_OBJECT
    public:

        /**
         * Create model
         * @param step Test step instance
         */
        TestStepInstructionModel(Step* step);

        /**
         * Insert new instruction in current position
         * @param current Current selected item
         * @param name Name of instruction
         * @return Index of created instruction
         */
        QModelIndex insertInstruction(const QModelIndex &current,
                                      const QString& name);

        /**
         * Remove selected item
         * @param current Current selected item
         * @return True if item was removed
         */
        bool remove(const QModelIndex& current);
        /**
         * Check if item can be removed
         * @param current Current selected item
         * @return True if item can be removed
         */
        bool canRemove(const QModelIndex& current) const;

        /**
         * Move item up
         * @param current Current selected item
         * @return True if item was moved
         */
        bool moveUp(const QModelIndex& current);
        /**
         * Move item down
         * @param current Current selected item
         * @return True if item was moved
         */
        bool moveDown(const QModelIndex& current);
        /**
         * Check if item can be moved up
         * @param current Current selected item
         * @return True if item can be moved
         */
        bool canMoveUp(const QModelIndex& current) const;
        /**
         * Check if item can be moved down
         * @param current Current selected item
         * @return True if item can be moved
         */
        bool canMoveDown(const QModelIndex& current) const;

        /**
         * Get actual selected instruction
         * @param current Current selected item
         * @return Instruction instance or NULL if no instruction selected
         */
        Instruction* getInstruction(const QModelIndex& current) const;


        /**
         * Get data of item
         * @param index Index of item
         * @param role Requested data type
         * @return Data of item
         */
        QVariant data(const QModelIndex& index,
                      int role = Qt::DisplayRole) const;
        /**
         * Get amount of child rows of item
         * @param parent Parent index
         * @return Amount of child items
         */
        int rowCount(const QModelIndex& parent = QModelIndex()) const;

    private:
        /**
         * Instance of test step
         */
        Step* step;
};


#endif //TESTLI_TESTSTEPINSTRUCTIONMODEL_H
