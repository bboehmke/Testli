/**
 * @file StepLoggerDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_STEPLOGGERDIALOG_H
#define TESTLI_STEPLOGGERDIALOG_H

#include <QObject>
#include <QDialog>
#include <QStringListModel>

#include "ui_StepLoggerDialog.h"

#include "../test_spec/Step.h"

/**
 * Step logger dialog
 */
class StepLoggerDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        StepLoggerDialog(QWidget* parent);

        /**
         * Show dialog
         * @param s Test step instance
         */
        void showDialog(Step* s);

    private slots:
        /**
         * Handle add button click
         */
        void on_addButton_clicked();
        /**
         * Handle remove button click
         */
        void on_removeButton_clicked();

        /**
         * Handle instruction selection change
         * @param current Current selected instruction
         */
        void currentChanged(const QModelIndex& current);

    private:
        /**
         * GUI elements
         */
        Ui::StepLoggerDialog ui;

        /**
         * Model for file list
         */
        QStringListModel* model;

        /**
         * Step instance
         */
        Step* step;
};


#endif //TESTLI_STEPLOGGERDIALOG_H
