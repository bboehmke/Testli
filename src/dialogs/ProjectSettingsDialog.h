/**
 * @file ProjectSettingsDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_PROJECTSETTINGSDIALOG_H
#define TESTLI_PROJECTSETTINGSDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_ProjectSettingsDialog.h"

/**
 * Dialog for edit project settings
 */
class ProjectSettingsDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        ProjectSettingsDialog(QWidget* parent);

        /**
         * Show dialog
         */
        void showDialog();

    signals:
        /**
         * Emitted settings changed
         */
        void projectSettingsChanged();

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle save button
         */
        void on_saveButton_clicked();

        /**
         * Handle toggle of feature separation
         */
        void on_testEnableSeparateFeatures_toggled();
        /**
         * Handle toggle of testcase separation
         */
        void on_testEnableSeparateTestcases_toggled();

    private:
        /**
         * GUI elements
         */
        Ui::ProjectSettingsDialog ui;
};


#endif //TESTLI_PROJECTSETTINGSDIALOG_H
