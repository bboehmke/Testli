/**
 * @file CreateStepInstructionDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_CREATESTEPINSTRUCTIONDIALOG_H
#define TESTLI_CREATESTEPINSTRUCTIONDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_CreateStepInstructionDialog.h"

/**
 * Dialog for instruction creation
 */
class CreateStepInstructionDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        CreateStepInstructionDialog(QWidget* parent);

    signals:
        /**
         * Emitted if instruction should be created
         * @param type Type of instruction
         */
        void createInstruction(QString type);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle create button
         */
        void on_createButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::CreateStepInstructionDialog ui;
};


#endif //TESTLI_CREATESTEPINSTRUCTIONDIALOG_H
