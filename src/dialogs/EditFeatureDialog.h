/**
 * @file EditFeatureDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_EDITFEATUREDIALOG_H
#define TESTLI_EDITFEATUREDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_EditFeatureDialog.h"

/**
 * Dialog for edit of feature
 */
class EditFeatureDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        EditFeatureDialog(QWidget* parent);

        /**
         * Show dialog
         * @param name Name of feature
         * @param id Id of feature
         */
        void showDialog(const QString& name, const QString& id);

    signals:
        /**
         * Emitted if feature should be edited
         * @param name Name of feature group
         * @param id Id of feature
         */
        void editFeature(QString name, QString id);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle edit button
         */
        void on_editButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::EditFeatureDialog ui;

};


#endif //TESTLI_EDITFEATUREDIALOG_H
