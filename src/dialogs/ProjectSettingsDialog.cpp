/**
 * @file ProjectSettingsDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ProjectSettingsDialog.h"

#include "../Settings.h"

ProjectSettingsDialog::ProjectSettingsDialog(QWidget* parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QFont f("unexistent");
    f.setStyleHint(QFont::Monospace);
    ui.testDefaultText->setFont(f);
}

void ProjectSettingsDialog::showDialog() {
    ui.testEnableSeparateFeatures->setChecked(
            Settings::getProjectValue("Test/EnableFeatureSeparation").toBool());
    ui.testEnableSeparateTestcases->setChecked(
            Settings::getProjectValue("Test/EnableTestSeparation").toBool());
    ui.testSubdirectory->setText(
            Settings::getProjectValue("Test/Subdirectory").toString());

    ui.testEnableDefaultPrecond->setChecked(
            Settings::getProjectValue("Test/EnableDefaultPrecond").toBool());
    ui.testEnableDefaultStep->setChecked(
            Settings::getProjectValue("Test/EnableDefaultStep").toBool());
    ui.testEnableDefaultPostcond->setChecked(
            Settings::getProjectValue("Test/EnableDefaultPostcond").toBool());

    ui.testDefaultState->setText(
            Settings::getProjectValue("Test/DefaultState").toString());
    ui.testDefaultText->setPlainText(
            Settings::getProjectValue("Test/DefaultText").toString());
    ui.testEnableChangeToDefaultState->setChecked(
            Settings::getProjectValue("Test/EnableChangeToDefaultState").toBool());
    ui.testEnableSpecResultElement->setChecked(
            Settings::getProjectValue("Test/EnableSpecResultElement").toBool());

    ui.execWorkingDir->setText(
            Settings::getProjectValue("Execution/WorkingDir").toString());
    ui.execToolDir->setText(
            Settings::getProjectValue("Execution/ToolDir").toString());


    ui.testSubdirectory->setEnabled(
            ui.testEnableSeparateFeatures->isChecked() ||
            ui.testEnableSeparateTestcases->isChecked());
    show();
}

void ProjectSettingsDialog::on_cancelButton_clicked() {
    close();
}
void ProjectSettingsDialog::on_saveButton_clicked() {
    Settings::setProjectValue("Test/EnableFeatureSeparation",
                              ui.testEnableSeparateFeatures->isChecked());
    Settings::setProjectValue("Test/EnableTestSeparation",
                              ui.testEnableSeparateTestcases->isChecked());
    Settings::setProjectValue("Test/Subdirectory",
                              ui.testSubdirectory->text());

    Settings::setProjectValue("Test/EnableDefaultPrecond",
                              ui.testEnableDefaultPrecond->isChecked());
    Settings::setProjectValue("Test/EnableDefaultStep",
                              ui.testEnableDefaultStep->isChecked());
    Settings::setProjectValue("Test/EnableDefaultPostcond",
                              ui.testEnableDefaultPostcond->isChecked());

    Settings::setProjectValue("Test/DefaultState",
                              ui.testDefaultState->text());
    Settings::setProjectValue("Test/DefaultText",
                              ui.testDefaultText->toPlainText());
    Settings::setProjectValue("Test/EnableChangeToDefaultState",
                              ui.testEnableChangeToDefaultState->isChecked());
    Settings::setProjectValue("Test/EnableSpecResultElement",
                              ui.testEnableSpecResultElement->isChecked());

    Settings::setProjectValue("Execution/WorkingDir",
                              ui.execWorkingDir->text());
    Settings::setProjectValue("Execution/ToolDir",
                              ui.execToolDir->text());
    close();
    emit(projectSettingsChanged());
}

void ProjectSettingsDialog::on_testEnableSeparateFeatures_toggled() {
    ui.testSubdirectory->setEnabled(
            ui.testEnableSeparateFeatures->isChecked() ||
            ui.testEnableSeparateTestcases->isChecked());
}
void ProjectSettingsDialog::on_testEnableSeparateTestcases_toggled() {
    ui.testSubdirectory->setEnabled(
            ui.testEnableSeparateFeatures->isChecked() ||
            ui.testEnableSeparateTestcases->isChecked());
}