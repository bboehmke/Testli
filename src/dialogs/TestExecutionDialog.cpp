/**
 * @file TestExecutionDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestExecutionDialog.h"
#include "../Settings.h"

#include "../test_spec/TestSpec.h"

#include <QMessageBox>
#include <QDir>

TestExecutionDialog::TestExecutionDialog(QWidget* parent) :
        QDialog(parent), reportMode(false), step(NULL),
        running(false), runAll(false) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui.splitter->setSizes({300,350});

    QFont f("unexistent");
    f.setStyleHint(QFont::Monospace);
    ui.logConsole->setFont(f);

    createStepInstructionDialog = new CreateStepInstructionDialog(this);
    connect(createStepInstructionDialog, SIGNAL(createInstruction(QString)),
            this, SLOT(createInstruction(QString)));

    ui.stopButton->setEnabled(false);
}
TestExecutionDialog::~TestExecutionDialog() {
    delete(createStepInstructionDialog);
}

void TestExecutionDialog::showDialog(Step* s, bool report) {
    reportMode = report;
    if (step) {
        QItemSelectionModel* selectionModel =
                ui.stepInstructionList->selectionModel();
        TestStepInstructionModel* m = new TestStepInstructionModel(s);
        ui.stepInstructionList->setModel(m);
        delete(model);
        delete(selectionModel);
        model = m;
    } else {
        model = new TestStepInstructionModel(s);
        ui.stepInstructionList->setModel(model);
    }
    step = s;

    connect(ui.stepInstructionList->selectionModel(),
            SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
            this,
            SLOT(currentChanged(const QModelIndex&, const QModelIndex&)));

    for (QObject* obj : ui.instructionGroup->children()) {
        delete(obj);
    }
    on_clearButton_clicked();

    ui.addButton->setEnabled(!reportMode);
    ui.removeButton->setEnabled(false);
    ui.upButton->setEnabled(false);
    ui.downButton->setEnabled(false);
    ui.executeButton->setEnabled(false);
    ui.stopButton->setEnabled(false);
    ui.clearButton->setEnabled(true);

    ui.closeButton->setEnabled(true);
    ui.saveButton->setEnabled(
            !step->getParent()->getParent()->getParent()->getParent()->getReportPath().isEmpty() &&
                    reportMode);
    ui.savePassedButton->setEnabled(
            !step->getParent()->getParent()->getParent()->getParent()->getReportPath().isEmpty() &&
                    reportMode);

    show();
}

void TestExecutionDialog::on_saveButton_clicked() {
    if (consoleOutput.isEmpty()) {
        return;
    }

    step->addLog("console", consoleOutput);

    QDir dir(Settings::getSpecBasePath() + "/" +
             Settings::getValue("Execution/WorkingDir").toString());
    for (QString fileName : dir.entryList(QDir::Files)) {
        QFile file(dir.filePath(fileName));
        if (file.open(QIODevice::ReadOnly)) {

            step->addLog(fileName, QString::fromUtf8(file.readAll()));
            file.close();
        }
    }
    accept();
}
void TestExecutionDialog::on_savePassedButton_clicked() {
    if (consoleOutput.isEmpty()) {
        return;
    }

    step->setResultState("passed");

    on_saveButton_clicked();
}

void TestExecutionDialog::on_addButton_clicked() {
    createStepInstructionDialog->show();
}
void TestExecutionDialog::on_removeButton_clicked() {
    int button = QMessageBox::warning(this, tr("Remove Instruction"),
                                      tr("The the selected instruction will be remove and can not be restored!"),
                                      QMessageBox::Ok, QMessageBox::Cancel);
    if (button == QMessageBox::Ok) {
        QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
        model->remove(index);
    }
}
void TestExecutionDialog::on_upButton_clicked() {
    QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
    model->moveUp(index);
}
void TestExecutionDialog::on_downButton_clicked() {
    QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
    model->moveDown(index);
}
void TestExecutionDialog::on_executeButton_clicked() {
    QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
    Instruction* instruction = model->getInstruction(index);
    if ((!running || runAll) && instruction) {
        if (!instruction->canRun()) {
            logConsole(QString("##> Skip execution of Instruction %1:\n\n")
                               .arg(step->getInstructionList().indexOf(instruction)));
            if (!runAll) {
                running = false;
                return;
            }
            if (model->canMoveDown(index)) {
                ui.stepInstructionList->selectionModel()->setCurrentIndex(
                        model->index(index.row()+1, index.column()),
                        QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);

                on_executeButton_clicked();
            } else {
                runAll = false;
                running = false;

                currentChanged(index, index);
            }
            return;
        }
        running = true;
        ui.stopButton->setEnabled(true);

        ui.stepInstructionList->setEnabled(false);
        ui.addButton->setEnabled(false);
        ui.removeButton->setEnabled(false);
        ui.upButton->setEnabled(false);
        ui.downButton->setEnabled(false);
        ui.executeButton->setEnabled(false);
        ui.executeAllButton->setEnabled(false);
        ui.clearButton->setEnabled(false);
        ui.instructionGroup->setEnabled(false);

        ui.closeButton->setEnabled(false);
        ui.saveButton->setEnabled(false);
        ui.savePassedButton->setEnabled(false);

        logConsole(QString("##> Execute Instruction %1:\n")
                           .arg(step->getInstructionList().indexOf(instruction)));
        consoleOutput += QString("Instruction %1:\n")
                .arg(step->getInstructionList().indexOf(instruction));

        instruction->run();
    } else {
        runAll = false;
    }
}
void TestExecutionDialog::on_executeAllButton_clicked() {
    runAll = true;
    ui.stepInstructionList->selectionModel()->setCurrentIndex(
            model->index(0, 0),
            QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    on_executeButton_clicked();
}
void TestExecutionDialog::on_stopButton_clicked() {
    QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
    Instruction* instruction = model->getInstruction(index);

    if (instruction) {
        runAll = false;
        instruction->stop();
    }
}
void TestExecutionDialog::on_clearButton_clicked() {
    ui.logConsole->clear();
    consoleOutput = "";

    QDir dir(Settings::getSpecBasePath() + "/" +
             Settings::getValue("Execution/WorkingDir").toString());
    if (dir != QDir(Settings::getSpecBasePath()) && dir.exists()) {
        dir.removeRecursively();
    }
}

void TestExecutionDialog::createInstruction(QString type) {
    QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
    QModelIndex newIndex = model->insertInstruction(index, type);

    if (newIndex.isValid()) {
        ui.stepInstructionList->selectionModel()->setCurrentIndex(
                newIndex,
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
    }
}

void TestExecutionDialog::currentChanged(const QModelIndex& current,
                                         const QModelIndex& previous) {
    ui.stepInstructionList->setEnabled(!running);
    ui.addButton->setEnabled(!running && !reportMode);
    ui.removeButton->setEnabled(!running && !reportMode && model->canRemove(current));
    ui.upButton->setEnabled(!running && !reportMode && model->canMoveUp(current));
    ui.downButton->setEnabled(!running && !reportMode && model->canMoveDown(current));
    ui.executeButton->setEnabled(!running);
    ui.executeAllButton->setEnabled(!running);
    ui.stopButton->setEnabled(running);
    ui.clearButton->setEnabled(!running);

    ui.closeButton->setEnabled(true);
    ui.saveButton->setEnabled(
            !step->getParent()->getParent()->getParent()->getParent()->getReportPath().isEmpty() &&
            reportMode);
    ui.savePassedButton->setEnabled(
            !step->getParent()->getParent()->getParent()->getParent()->getReportPath().isEmpty() &&
            reportMode);

    ui.instructionGroup->setEnabled(!running && !reportMode);

    Instruction* prevInstruction = model->getInstruction(previous);
    if (prevInstruction) {
        disconnect(prevInstruction, SIGNAL(logConsole(QString)),
                NULL, NULL);
        disconnect(prevInstruction, SIGNAL(logConsoleError(QString)),
                NULL, NULL);
        disconnect(prevInstruction, SIGNAL(finished(int)),
                NULL, NULL);
    }

    Instruction* instruction = model->getInstruction(current);
    if (instruction) {
        for (QObject* obj : ui.instructionGroup->children()) {
            delete(obj);
        }

        instruction->getSettingLayout(ui.instructionGroup);

        connect(instruction, SIGNAL(logConsole(QString)),
                this, SLOT(logConsole(QString)));
        connect(instruction, SIGNAL(logConsoleError(QString)),
                this, SLOT(logConsoleError(QString)));
        connect(instruction, SIGNAL(finished(int)),
                this, SLOT(on_finished(int)));

    } else {
        ui.instructionGroup->setEnabled(false);
    }
}


void TestExecutionDialog::logConsole(QString text) {
    ui.logConsole->appendHtml(QString("<pre>%1</pre>").arg(text));
    consoleOutput += text;
}
void TestExecutionDialog::logConsoleError(QString text) {
    ui.logConsole->appendHtml(
            QString("<span style=\"color:#ff0000;\"><pre>%1</pre></span>").arg(text));
    consoleOutput += text;
}
void TestExecutionDialog::on_finished(int returnCode) {
    QModelIndex index = ui.stepInstructionList->selectionModel()->currentIndex();
    Instruction* instruction = model->getInstruction(index);

    logConsole(QString("##> Instruction %1 finished with: %2 \n\n")
                       .arg(step->getInstructionList().indexOf(instruction))
                       .arg(returnCode));
    consoleOutput += "\n";

    if (runAll && model->canMoveDown(index)) {
        ui.stepInstructionList->selectionModel()->setCurrentIndex(
                model->index(index.row()+1, index.column()),
                QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);

        on_executeButton_clicked();
    } else {
        runAll = false;
        running = false;

        currentChanged(index, index);
    }
}

void TestExecutionDialog::closeEvent(QCloseEvent* event) {
    if (running) {
        event->ignore();
    }
}
