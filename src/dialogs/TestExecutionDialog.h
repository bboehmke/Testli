/**
 * @file TestExecutionDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TESTEXECUTIONDIALOG_H
#define TESTLI_TESTEXECUTIONDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_TestExecutionDialog.h"

#include "CreateStepInstructionDialog.h"

#include "../test_spec/Step.h"
#include "../TestStepInstructionModel.h"

/**
 * Test step execution
 */
class TestExecutionDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        TestExecutionDialog(QWidget* parent);
        ~TestExecutionDialog();

        /**
         * Show dialog
         * @param s Test step instance
         * @param report True if in report mode
         */
        void showDialog(Step* s, bool report);

    private slots:
        /**
         * Handle save button click
         */
        void on_saveButton_clicked();
        /**
         * Handle save passed button click
         */
        void on_savePassedButton_clicked();

        /**
         * Handle add button click
         */
        void on_addButton_clicked();
        /**
         * Handle remove button click
         */
        void on_removeButton_clicked();
        /**
         * Handle up button click
         */
        void on_upButton_clicked();
        /**
         * Handle down button click
         */
        void on_downButton_clicked();
        /**
         * Handle execute button click
         */
        void on_executeButton_clicked();
        /**
         * Handle execute all button click
         */
        void on_executeAllButton_clicked();
        /**
         * Handle stop button click
         */
        void on_stopButton_clicked();
        /**
         * Handle clear button click
         */
        void on_clearButton_clicked();

        /**
         * Handle instruction creation
         * @param type Type of instruction
         */
        void createInstruction(QString type);

        /**
         * Handle instruction selection change
         * @param current Current selected instruction
         * @param previous Last selected instruction
         */
        void currentChanged(const QModelIndex& current,
                            const QModelIndex& previous);

        /**
         * Log message to console
         * @param text Text to log
         */
        void logConsole(QString text);
        /**
         * Log error message to console
         * @param text Text to log
         */
        void logConsoleError(QString text);
        /**
         * Handle finished instruction finished
         * @param returnCode Execution return code
         */
        void on_finished(int returnCode);

    protected:
        /**
         * Called if window closed
         * @param event Event object
         */
        void closeEvent(QCloseEvent* event);

    private:
        /**
         * GUI elements
         */
        Ui::TestExecutionDialog ui;

        /**
         * True if report mode is enabled
         */
        bool reportMode;

        /**
         * Dialog for instruction creation
         */
        CreateStepInstructionDialog* createStepInstructionDialog;

        /**
         * Step instance
         */
        Step* step;

        /**
         * Model for instruction list
         */
        TestStepInstructionModel* model;

        /**
         * True if instruction is running
         */
        bool running;
        /**
         * True if all instruction should run
         */
        bool runAll;

        /**
         * Console output content
         */
        QString consoleOutput;
};


#endif //TESTLI_TESTEXECUTIONDIALOG_H
