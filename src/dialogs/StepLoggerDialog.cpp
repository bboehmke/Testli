/**
 * @file StepLoggerDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "StepLoggerDialog.h"

#include <QFileDialog>
#include <QMessageBox>

StepLoggerDialog::StepLoggerDialog(QWidget* parent) :
        QDialog(parent), step(NULL) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui.splitter->setSizes({200,600});

    QFont f("unexistent");
    f.setStyleHint(QFont::Monospace);
    ui.logViewer->setFont(f);

    model = new QStringListModel(this);
    ui.logList->setModel(model);

    connect(ui.logList->selectionModel(),
            SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
            this,
            SLOT(currentChanged(const QModelIndex&)));
}

void StepLoggerDialog::showDialog(Step* s) {
    step = s;
    model->setStringList(s->getLogs().keys());
    ui.removeButton->setEnabled(false);
    show();
}

void StepLoggerDialog::on_addButton_clicked() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Add File"));
    if (fileName.isEmpty()) {
        return;
    }

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(this, "Error",
                              "Failed to open file - " + file.errorString());
    }
    QFileInfo info(fileName);

    step->addLog(info.fileName(), QString::fromUtf8(file.readAll()));
    file.close();

    model->setStringList(step->getLogs().keys());
}
void StepLoggerDialog::on_removeButton_clicked() {
    QModelIndex current = ui.logList->selectionModel()->currentIndex();
    QString name = model->data(current, Qt::DisplayRole).toString();

    step->removeLog(name);
    model->removeRow(current.row());
}

void StepLoggerDialog::currentChanged(const QModelIndex& current) {
    ui.removeButton->setEnabled(current.isValid());
    if (current.isValid()) {
        QString name = model->data(current, Qt::DisplayRole).toString();
        QMap<QString, QString> logs = step->getLogs();
        if (logs.contains(name)) {
            ui.logViewer->setText(logs[name]);
        }
    } else {
        ui.logViewer->clear();
    }
}