/**
 * @file AboutDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AboutDialog.h"

#include "version.h"

AboutDialog::AboutDialog(QWidget* parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QFont f("unexistent");
    f.setStyleHint(QFont::Monospace);
    ui.license->setFont(f);

    QString note;
    if (!QString(BUILD_NOTE).isEmpty()) {
        note = QString("- %1").arg(BUILD_NOTE);
    }

    QString version = QString("Version %1 %2")
            .arg(VERSION_FULL)
            .arg(note);

    ui.versionLabel->setText(version);

    QFile file(":/misc/LICENSE");
    if (file.open(QIODevice::ReadOnly)) {
        ui.license->setPlainText(QString::fromUtf8(file.readAll()));
        file.close();
    }

    // TODO change URL
    QString tpl = ""
            "<b>Testli</b><br/>"
            "<br/>"
            "Version: %1 %2<br/>"
            "Build: #%3<br/>"
            "Build date: %4<br/>"
            "<br/>"
            "Copyright (c) 2017, Benjamin Böhmke &lt;benjamin@boehmke.net&gt;<br/>"
            "<a href=\"https://gitlab.com/bboehmke/Testli\">https://gitlab.com/bboehmke/Testli</a>";

    ui.about->setText(tpl.arg(VERSION_FULL).arg(note).arg(BUILD_NUMBER).arg(BUILD_TIME));
}