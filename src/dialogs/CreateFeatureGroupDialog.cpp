/**
 * @file CreateFeatureGroupDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CreateFeatureGroupDialog.h"

#include <QMessageBox>

CreateFeatureGroupDialog::CreateFeatureGroupDialog(QWidget* parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void CreateFeatureGroupDialog::on_cancelButton_clicked() {
    close();
    ui.name->clear();
}
void CreateFeatureGroupDialog::on_createButton_clicked() {
    if (ui.name->text().isEmpty()) {
        QMessageBox::warning(this, "Note", "Name missing");
        return;
    }

    close();
    emit(createFeatureGroup(ui.name->text()));
    ui.name->clear();
}
