/**
 * @file CreateProjectDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CreateProjectDialog.h"

#include <QFileDialog>
#include <QMessageBox>

#include "../Settings.h"

CreateProjectDialog::CreateProjectDialog(QWidget* parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void CreateProjectDialog::on_projectSelectButton_clicked() {
    QString projectFile = ui.projectFile->text();
    if (projectFile.isEmpty()) {
        projectFile = Settings::getApplicationValue("last_file/project").toString();
    }
    QString file = QFileDialog::getSaveFileName(
            this, tr("Select project file"),
            projectFile, tr("Project file (*.xml)"),
            NULL, QFileDialog::DontConfirmOverwrite | QFileDialog::DontUseNativeDialog);

    if (!file.isEmpty()) {
        if (!file.endsWith(".xml")) {
            file = file + ".xml";
        }
        Settings::setApplicationValue("last_file/project", file);
        ui.projectFile->setText(file);
    }
}

void CreateProjectDialog::on_testSpecSelectButton_clicked() {
    QString specFile = ui.testSpecFile->text();
    if (specFile.isEmpty()) {
        specFile = Settings::getApplicationValue("last_file/spec").toString();
    }
    QString file = QFileDialog::getSaveFileName(
            this, tr("Select test specification"),
            specFile, tr("XML file (*.xml)"),
            NULL, QFileDialog::DontConfirmOverwrite | QFileDialog::DontUseNativeDialog);

    if (!file.isEmpty()) {
        if (!file.endsWith(".xml")) {
            file = file + ".xml";
        }
        Settings::setApplicationValue("last_file/spec", file);
        ui.testSpecFile->setText(file);
    }
}

void CreateProjectDialog::on_cancelButton_clicked() {
    close();
}
void CreateProjectDialog::on_createButton_clicked() {
    if (ui.projectFile->text().isEmpty()) {
        QMessageBox::warning(this, tr("Note"),
                             tr("Project file missing"));
        return;
    }
    if (ui.testSpecFile->text().isEmpty()) {
        QMessageBox::warning(this, tr("Note"),
                             tr("Test spec file missing"));
        return;
    }

    close();
    emit(createProject(ui.projectFile->text(),
                       ui.testSpecFile->text()));
}
