/**
 * @file EditLinkedTestcaseDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "EditLinkedTestcaseDialog.h"

#include <QMessageBox>

EditLinkedTestcaseDialog::EditLinkedTestcaseDialog(QWidget* parent) :
        QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void EditLinkedTestcaseDialog::showDialog(const QString& featureId,
                                          const QString& testcaseId) {
    ui.featureId->setText(featureId);
    ui.testcaseId->setText(testcaseId);
    show();
}
void EditLinkedTestcaseDialog::on_cancelButton_clicked() {
    close();
}
void EditLinkedTestcaseDialog::on_editButton_clicked() {
    if (ui.featureId->text().isEmpty() || ui.testcaseId->text().isEmpty()) {
        QMessageBox::warning(this, "Note",
                             "Feature ID and Testcase ID are required");
        return;
    }

    close();
    emit(editLinkedTestcase(ui.featureId->text(), ui.testcaseId->text()));
}
