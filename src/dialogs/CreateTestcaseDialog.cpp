/**
 * @file CreateTestcaseDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CreateTestcaseDialog.h"

#include <QMessageBox>

CreateTestcaseDialog::CreateTestcaseDialog(QWidget* parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void CreateTestcaseDialog::on_cancelButton_clicked() {
    close();
    ui.testId->clear();
}
void CreateTestcaseDialog::on_createButton_clicked() {
    if (ui.testId->text().isEmpty()) {
        QMessageBox::warning(this, "Note", "Id missing");
        return;
    }

    close();
    emit(createTestcase(ui.testId->text()));
    ui.testId->clear();
}
