/**
 * @file EditFeatureDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "EditFeatureDialog.h"

#include <QMessageBox>

EditFeatureDialog::EditFeatureDialog(QWidget* parent) : QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void EditFeatureDialog::showDialog(const QString& name, const QString& id) {
    ui.featureName->setText(name);
    ui.featureId->setText(id);
    show();
}

void EditFeatureDialog::on_cancelButton_clicked() {
    close();
}
void EditFeatureDialog::on_editButton_clicked() {
    if (ui.featureName->text().isEmpty()) {
        QMessageBox::warning(this, "Note", "Name missing");
        return;
    }

    close();
    emit(editFeature(ui.featureName->text(), ui.featureId->text()));
}
