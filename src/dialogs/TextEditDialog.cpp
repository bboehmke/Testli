/**
 * @file TextEditDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TextEditDialog.h"
#include "../Settings.h"

#include <QDomDocument>
#include <QMessageBox>

TextEditDialog::TextEditDialog(QWidget* parent) : QDialog(parent), ref(NULL) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QFont f("unexistent");
    f.setStyleHint(QFont::Monospace);
    ui.source->setFont(f);
}

void TextEditDialog::showDialog(QString text, QObject* ref) {
    this->ref = ref;
    
    // default text
    if (text.isEmpty()) {
        text = Settings::getValue("Test/DefaultText").toString();
    }
    ui.source->setPlainText(text);
    show();
}

QString TextEditDialog::docBookToHtml(QString text) {
    text.replace("<ilist>", "<ul>");
    text.replace("</ilist>", "</ul>");
    text.replace("<olist>", "<ol>");
    text.replace("</olist>", "</ol>");
    text.replace("<item>", "<li>");
    text.replace("</item>", "</li>");
    return text;
}

void TextEditDialog::on_source_textChanged() {
    ui.text->setHtml(docBookToHtml(ui.source->toPlainText()));
}
void TextEditDialog::on_closeButton_clicked() {
    close();
}
void TextEditDialog::on_saveButton_clicked() {
    QDomDocument doc;
    QString error;

    QString text = ui.source->toPlainText();
    if (!doc.setContent(QString("<XmlContent>%1</XmlContent>").arg(text), &error)) {
        QMessageBox::critical(this, "Error", error);
        return;
    }

    close();
    emit(textEdited(text, ref));
}
