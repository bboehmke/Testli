/**
 * @file TextEditDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TEXTEDITDIALOG_H
#define TESTLI_TEXTEDITDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_TextEditDialog.h"

/**
 * Dialog for text edit
 */
class TextEditDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        TextEditDialog(QWidget* parent);

        /**
         * Show dialog
         * @param text Test to edit
         * @param ref Reference object
         */
        void showDialog(QString text, QObject* ref);

        /**
         * Convert docbook to HTML
         * @param text Docbook string
         * @return HTML string
         */
        static QString docBookToHtml(QString text);

    signals:
        /**
         * Emitted if text was edited
         * @param text Changed text
         * @param ref Reference object
         */
        void textEdited(QString text, QObject* ref);

    private slots:
        /**
         * Handle text change
         */
        void on_source_textChanged();
        /**
         * Handle close button
         */
        void on_closeButton_clicked();
        /**
         * Handle save button
         */
        void on_saveButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::TextEditDialog ui;

        /**
         * Reference object
         */
        QObject* ref;
};


#endif //TESTLI_TEXTEDITDIALOG_H
