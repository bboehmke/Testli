/**
 * @file CreateRequirementDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_CREATEREQUIREMENTDIALOG_H
#define TESTLI_CREATEREQUIREMENTDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_CreateRequirementDialog.h"

/**
 * Dialog for requirement creation
 */
class CreateRequirementDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        CreateRequirementDialog(QWidget* parent);

        /**
         * Set list of existing requirements
         * @param requirements Requirement list
         */
        void setRequirementList(const QStringList& requirements);

    signals:
        /**
         * Emitted if requirement should be created
         * @param id Id of requirement
         */
        void createRequirement(QString id);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle create button
         */
        void on_createButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::CreateRequirementDialog ui;
};


#endif //TESTLI_CREATEREQUIREMENTDIALOG_H
