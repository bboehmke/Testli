/**
 * @file CreateProjectDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_CREATIONWINDOW_H
#define TESTLI_CREATIONWINDOW_H

#include <QObject>
#include <QDialog>

#include "ui_CreateProjectDialog.h"

/**
 * Dialog for project creation
 */
class CreateProjectDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        CreateProjectDialog(QWidget* parent);

    signals:
        /**
         * Emitted if project should be created
         * @param project Path to project path
         * @param spec Path to test spec path
         */
        void createProject(QString project, QString spec);

    private slots:
        /**
         * Handle project select button
         */
        void on_projectSelectButton_clicked();
        /**
         * Handle test spec select button
         */
        void on_testSpecSelectButton_clicked();

        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle create button
         */
        void on_createButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::CreateProjectDialog ui;
};


#endif //TESTLI_CREATIONWINDOW_H
