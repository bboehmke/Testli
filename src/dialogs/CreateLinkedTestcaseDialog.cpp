/**
 * @file CreateLinkedTestcaseDialog.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CreateLinkedTestcaseDialog.h"

#include <QMessageBox>

CreateLinkedTestcaseDialog::CreateLinkedTestcaseDialog(QWidget* parent) :
        QDialog(parent) {
    ui.setupUi(this);

    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

void CreateLinkedTestcaseDialog::on_cancelButton_clicked() {
    close();
    ui.featureId->clear();
    ui.testcaseId->clear();
}
void CreateLinkedTestcaseDialog::on_createButton_clicked() {
    if (ui.featureId->text().isEmpty() || ui.testcaseId->text().isEmpty()) {
        QMessageBox::warning(this, "Note",
                             "Feature ID and Testcase ID are required");
        return;
    }

    close();
    emit(createLinkedTestcase(ui.featureId->text(), ui.testcaseId->text()));
    ui.featureId->clear();
    ui.testcaseId->clear();
}
