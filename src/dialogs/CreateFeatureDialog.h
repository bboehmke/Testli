/**
 * @file CreateFeatureDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_CREATEFEATUREDIALOG_H
#define TESTLI_CREATEFEATUREDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_CreateFeatureDialog.h"

/**
 * Dialog for feature creation
 */
class CreateFeatureDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        CreateFeatureDialog(QWidget* parent);

    signals:
        /**
         * Emitted if feature should be created
         * @param name Name of feature
         * @param id Id of feature
         */
        void createFeature(QString name, QString id);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle create button
         */
        void on_createButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::CreateFeatureDialog ui;
};


#endif //TESTLI_CREATEFEATUREDIALOG_H
