/**
 * @file EditTestcaseDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_EDITTESTCASEDIALOG_H
#define TESTLI_EDITTESTCASEDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_EditTestcaseDialog.h"

/**
 * Dialog for edit of testcase
 */
class EditTestcaseDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        EditTestcaseDialog(QWidget* parent);

        /**
         * Show dialog
         * @param id Id of testcase
         * @param state State of testcase
         */
        void showDialog(const QString& id, const QString& state);

    signals:
        /**
         * Emitted if testcase should be edited
         * @param id Id of testcase
         * @param state State of testcase
         */
        void editTestcase(QString id, QString state);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle edit button
         */
        void on_editButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::EditTestcaseDialog ui;
};


#endif //TESTLI_EDITTESTCASEDIALOG_H
