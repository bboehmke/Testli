/**
 * @file EditLinkedTestcaseDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_EDITLINKEDTESTCASEDIALOG_H
#define TESTLI_EDITLINKEDTESTCASEDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_EditLinkedTestcaseDialog.h"

/**
 * Dialog for edit of linked testcase
 */
class EditLinkedTestcaseDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        EditLinkedTestcaseDialog(QWidget* parent);

        /**
         * Show dialog
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         */
        void showDialog(const QString& featureId, const QString& testcaseId);

    signals:
        /**
         * Emitted if linked testcase should be edited
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         */
        void editLinkedTestcase(QString featureId, QString testcaseId);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle edit button
         */
        void on_editButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::EditLinkedTestcaseDialog ui;
};


#endif //TESTLI_EDITLINKEDTESTCASEDIALOG_H
