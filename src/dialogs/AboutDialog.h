/**
 * @file AboutDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_ABOUTDIALOG_H
#define TESTLI_ABOUTDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_AboutDialog.h"

/**
 * About dialog
 */
class AboutDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        AboutDialog(QWidget* parent);

    private:
        /**
         * GUI elements
         */
        Ui::AboutDialog ui;
};


#endif //TESTLI_ABOUTDIALOG_H
