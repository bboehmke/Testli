/**
 * @file CreateLinkedTestcaseDialog.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_CREATELINKEDTESTCASEDIALOG_H
#define TESTLI_CREATELINKEDTESTCASEDIALOG_H

#include <QObject>
#include <QDialog>

#include "ui_CreateLinkedTestcaseDialog.h"

/**
 * Dialog for linked testcase creation
 */
class CreateLinkedTestcaseDialog : public QDialog {
    Q_OBJECT
    public:
        /**
         * Create dialog
         * @param parent Parent window
         */
        CreateLinkedTestcaseDialog(QWidget* parent);

    signals:
        /**
         * Emitted if linked testcase should be created
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         */
        void createLinkedTestcase(QString featureId, QString testcaseId);

    private slots:
        /**
         * Handle cancel button
         */
        void on_cancelButton_clicked();
        /**
         * Handle create button
         */
        void on_createButton_clicked();

    private:
        /**
         * GUI elements
         */
        Ui::CreateLinkedTestcaseDialog ui;
};


#endif //TESTLI_CREATELINKEDTESTCASEDIALOG_H
