/**
 * @file Testcase.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TESTCASE_H
#define TESTLI_TESTCASE_H

#include <QList>
#include <QDomElement>
#include <QDate>

#include "TestSpecItem.h"
#include "Step.h"

class Feature;

/**
 * Class to handle testcase element
 */
class Testcase : public TestSpecItem {
    public:
        /**
         * Create testcase element from XML element
         * @param parent Parent element
         * @param element XML element
         */
        Testcase(Feature* parent, const QDomElement& element);
        /**
         * Create empty testcase element
         * @param parent Parent element
         * @param id Id of testcase element
         */
        Testcase(Feature* parent, const QString& id);
        ~Testcase();

        /**
         * Get unique name
         * @return Unique name of item
         */
        QString getUniqueName() const;

        /**
         * Load report components
         * @param element XML element
         * @return True if element was load
         */
        bool loadReport(const QDomElement& element);
        /**
         * Remove report components
         */
        void removeReport();

        /**
         * Parent item
         * @return Parent instance
         */
        Feature* getParent() const;
        /**
         * Index of this element
         * @return Index
         */
        int getIndexNumber() const;

        /**
         * Get test id
         * @return Id of test
         */
        QString getId() const;
        /**
         * Set test id
         * @param id Id of test
         */
        void setId(const QString& id);

        /**
         * Get state date
         * @return Date of state
         */
        QDate getStateDate() const;
        /**
         * Get state of test case
         * @return Test case state
         */
        QString getState() const;
        /**
         * Set state of test case
         * @param state Test case state
         */
        void setState(const QString& state);

        /**
         * Reset the test state to default value
         */
        void resetTestState();

        /**
         * Get comment of test
         * @return Test comment
         */
        QString getComment() const;
        /**
         * Set comment of test
         * @param text Text to set
         */
        void setComment(const QString& text);
        /**
         * Get description of test
         * @return Test description
         */
        QString getDescription() const;
        /**
         * Set description of test
         * @param text Text to set
         */
        void setDescription(const QString& text);

        /**
         * Update execution state of testcase
         */
        void updateExecutionState();

        /**
         * Get the step index (type specific)
         * @param step Step instance
         * @return Index of step
         */
        int getStepIndex(Step* step);
        /**
         * Get step
         * @param index Index of element
         * @return Step
         */
        Step* getStep(int index) const;
        /**
         * Get list of existing steps
         * @return List of steps
         */
        QList<Step*> getStepList() const;
        /**
         * Add new step
         * @param pos Position to insert
         * @param type Step type
         * @return Created instance
         */
        Step* addStep(int pos, Step::Type type);
        /**
         * Remove step
         * @param step Step to remove
         * @return True on success
         */
        bool removeStep(Step* step);
        /**
         * Move step to new position
         * @param step Step to move
         * @param pos Destination position
         * @return True on success
         */
        bool moveStep(Step* step, int pos);

        /**
         * Get the combined state of steps
         * @return State of steps
         */
        QString getStepState();
        /**
         * Get amount of executed steps
         * @return Amount of executed steps
         */
        int getExecutedStep();

        /**
         * Add requirement
         * @param requirement Requirement to add
         */
        void addRequirement(const QString& requirement);
        /**
         * Remove requirement
         * @param requirement Requirement to remove
         */
        void removeRequirement(const QString& requirement);
        /**
         * Get list of requirements
         * @return Requirement list
         */
        QStringList getRequirements() const;

        /**
         * Check if spec components changed
         * @return True if changed
         */
        bool isSpecChanged() const;
        /**
         * Check if report components changed
         * @return True if changed
         */
        bool isReportChanged() const;
        /**
         * Check if element is enabled
         * @return True if enabled
         */
        bool isEnabled() const;
        /**
         * Enable element
         * @param enabled True if enabled
         */
        void setEnabled(bool enabled);

        /**
         * Save element
         * @param doc XML document
         * @param basePath Base path of test spec
         * @param report True if report save
         * @return XML element
         */
        QDomElement save(QDomDocument& doc, const QString& basePath,
                         bool report);

    private:
        /**
         * Parent element
         */
        Feature* parent;

        /**
         * True if spec components changed
         */
        bool specChanged;

        /**
         * True if report components changed
         */
        bool reportChanged;

        /**
         * True if testcase is enabled in report
         */
        bool enabled;

        /**
         * Id of testcase element
         */
        QString id;
        /**
         * Storage path of test case
         */
        QString href;

        /**
         * Date of state
         */
        QDate stateDate;
        /**
         * Testcase state
         */
        QString state;

        /**
         * Execution variant
         */
        QString executionVariant;
        /**
         * Execution date
         */
        QDate executionDate;
        /**
         * Execution time
         */
        QTime executionTime;
        /**
         * Execution person
         */
        QString executionPerson;
        /**
         * Execution location
         */
        QString executionLocation;

        /**
         * Comment of test case
         */
        QString comment;
        /**
         * Description of test case
         */
        QString description;

        /**
         * List of requirements
         */
        QStringList requirements;

        /**
         * Annotations element of loaded report
         */
        QDomElement annotationsElement;

        /**
         * List of existing preconditions
         */
        QList<Step*> preconditionList;
        /**
         * List of existing steps
         */
        QList<Step*> stepList;
        /**
         * List of existing postconditions
         */
        QList<Step*> postconditionList;

        /**
         * Dump test case spec
         * @param doc Document to dump
         * @param report True if report dump
         * @return Testcase element
         */
        QDomElement dump(QDomDocument& doc, bool report);

        /**
         * Check if href is valid
         */
        void checkHref();
};


#endif //TESTLI_TESTCASE_H
