/**
 * @file FeatureGroup.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_FEATURES_H
#define TESTLI_FEATURES_H

#include <QList>
#include <QDomElement>

#include "TestSpecItem.h"
#include "Feature.h"

class TestSpec;

/**
 * Class to handle feature group element
 */
class FeatureGroup : public TestSpecItem {
    public:
        /**
         * Create feature group element from XML element
         * @param parent Test spec of this element
         * @param element XML element
         * @param disabledTests List of disabled test cases
         */
        FeatureGroup(TestSpec* parent, const QDomElement& element,
                     const QStringList& disabledTests);
        /**
         * Create empty feature group element
         * @param parent Test spec of this element
         * @param name Name of feature group element
         */
        FeatureGroup(TestSpec* parent, const QString& name);
        ~FeatureGroup();

        /**
         * Get unique name
         * @return Unique name of item
         */
        QString getUniqueName() const;

        /**
         * Load report components
         * @param element XML element
         * @return True if element was load
         */
        bool loadReport(const QDomElement& element);
        /**
         * Remove report components
         */
        void removeReport();

        /**
         * Parent item
         * @return Parent instance
         */
        TestSpec* getParent() const;
        /**
         * Index of this element
         * @return Index
         */
        int getIndexNumber() const;

        /**
         * Get name of feature group element
         * @return Name of element
         */
        QString getName() const;
        /**
         * Set name of feature group element
         * @param name Name of element
         */
        void setName(const QString& name);

        /**
         * Get feature element
         * @param index Index of element
         * @return Feature element
         */
        Feature* getFeature(int index) const;
        /**
         * Get list of existing feature
         * @return List of feature
         */
        QList<Feature*> getFeatureList() const;
        /**
         * Add new feature element
         * @param pos Position to insert
         * @param name Name of element
         * @param id Id of element
         * @return Created instance
         */
        Feature* addFeature(int pos, const QString& name, const QString& id);
        /**
         * Remove element
         * @param feature Element to remove
         * @return True on success
         */
        bool removeFeature(Feature* feature);
        /**
         * Move element to new position
         * @param feature Element to move
         * @param pos Destination position
         * @return True on success
         */
        bool moveFeature(Feature* feature, int pos);

        /**
         * Check if spec components changed
         * @return True if changed
         */
        bool isSpecChanged() const;
        /**
         * Check if report components changed
         * @return True if changed
         */
        bool isReportChanged() const;
        /**
         * Check if element is enabled
         * @return True if enabled
         */
        bool isEnabled() const;
        /**
         * Enable element
         * @param enabled True if enabled
         */
        void setEnabled(bool enabled);

        /**
         * Save element
         * @param doc XML document
         * @param basePath Base path of test spec
         * @param report True if report save
         * @return XML element
         */
        QDomElement save(QDomDocument &doc, const QString &basePath,
                         bool report);

    private:
        /**
         * Parent element
         */
        TestSpec* parent;

        /**
         * True if spec components changed
         */
        bool specChanged;

        /**
         * True if report components changed
         */
        bool reportChanged;

        /**
         * Name of feature group element
         */
        QString name;

        /**
         * List of existing features
         */
        QList<Feature*> featureList;
};


#endif //TESTLI_FEATURES_H
