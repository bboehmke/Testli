/**
 * @file InstructionLoader.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_INSTRUCTIONLOADER_H
#define TESTLI_INSTRUCTIONLOADER_H

#include "Instruction.h"

/**
 * Base class for the GenericInstructionCreator
 */
class InstructionCreator {
    public:
        virtual ~InstructionCreator() {}
        /**
         * Creates a new instruction instance
         * @return Instruction instance
         */
        virtual Instruction* create() = 0;
        /**
         * Creates a new instruction instance
         * @param element XML element of instruction
         * @return Instruction instance
         */
        virtual Instruction* create(const QDomElement& element) = 0;
};
/**
 * Class for the creation of a new instance of <em>T</em>
 */
template<class T>
class GenericInstructionCreator : public InstructionCreator {
    public:
        ~GenericInstructionCreator() {}
        /**
         * Creates a new instruction instance
         * @return Instruction instance
         */
        Instruction* create() {
            return new T();
        }
        /**
         * Creates a new instruction instance
         * @param element XML element of instruction
         * @return Instruction instance
         */
        Instruction* create(const QDomElement& element) {
            return new T(element);
        }
};
/**
 * Loader for new Instruction instances
 */
class InstructionLoader {
    public:
        ~InstructionLoader();
        /**
         * Register a new instruction type
         * @tparam The Instruction-class to register
         */
        template<class T>
        void registerInstruction() {
            creators[T().getName()] = new GenericInstructionCreator<T>;
        }
        /**
         * Creates a new instruction instance
         * @param name Function name
         * @return Instruction instance
         */
        Instruction* create(const QString& name);
        /**
         * Creates a new instruction instance
         * @param element XML element of instruction
         * @return Instruction instance
         */
        Instruction* create(const QDomElement& element);

        /**
         * Get a list with all supported instruction types
         * @return List of parameter object types
         */
        QStringList getInstructionTypes() const;

        /**
         * Get instruction loader instance
         * @return Loader for instruction
         */
        static InstructionLoader* getInstance();
        /**
         * Cleanup instance
         */
        static void cleanup();

    private:
        /**
         * Instruction loader instance
         */
        static InstructionLoader* instance;

        /**
         * List with all registered parameter object creators
         */
        QMap<QString, InstructionCreator*> creators;
};



#endif //TESTLI_INSTRUCTIONLOADER_H
