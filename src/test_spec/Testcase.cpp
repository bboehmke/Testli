/**
 * @file Testcase.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Testcase.h"

#include <QDir>

#include "Feature.h"
#include "FeatureGroup.h"
#include "../misc/XmlHelper.h"
#include "../Settings.h"
#include "../Exception.h"

Testcase::Testcase(Feature* parent, const QDomElement& element) :
        parent(parent), specChanged(false), reportChanged(false), enabled(true) {
    id = element.attribute("Id");
    href = element.attribute("__href");

    checkHref();

    QDomElement stateElement = element.firstChildElement("State");
    state = stateElement.text();
    stateDate = QDate::fromString(stateElement.attribute("date"), "yyyy-MM-dd");

    comment = XmlHelper::getXmlContent(element.firstChildElement("Comment"));
    description = XmlHelper::getXmlContent(element.firstChildElement("Description"));

    QDomElement requirementsElement = element.firstChildElement("CoveredRequirements");
    for (QDomElement e = requirementsElement.firstChildElement("CoveredRequirement");
         !e.isNull();
         e = e.nextSiblingElement("CoveredRequirement")) {
        QString req = e.attribute("Id");
        if (!req.isEmpty()) {
            requirements << req;
        }
    }

    QDomElement stepsElement = element.firstChildElement("Steps");
    
    for (QDomNode n = stepsElement.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == "Precondition") {
                preconditionList.append(new Step(this, e, Step::Pre));

            } else if (e.tagName() == "Step") {
                stepList.append(new Step(this, e, Step::Main));

            } else if (e.tagName() == "Postcondition") {
                postconditionList.append(new Step(this, e, Step::Post));
            }
        }
    }

    annotationsElement = element.firstChildElement("Annotations");
}
Testcase::Testcase(Feature* parent, const QString& id) :
        parent(parent), specChanged(true), reportChanged(false),
        enabled(true), id(id) {
    stateDate = QDate::currentDate();
    state = Settings::getValue("Test/DefaultState").toString();
    checkHref();
}
Testcase::~Testcase() {
    for (Step* step : preconditionList) {
        delete(step);
    }
    for (Step* step : stepList) {
        delete(step);
    }
    for (Step* step : postconditionList) {
        delete(step);
    }
}

QString Testcase::getUniqueName() const {
    return parent->getUniqueName() + "." + id;
}

bool Testcase::loadReport(const QDomElement& element) {
    if (element.attribute("Id") != id) {
        return false;
    }

    QDomElement executionElement = element.firstChildElement("Execution");
    executionVariant = executionElement.attribute("Variant");
    executionDate = QDate::fromString(executionElement.attribute("Date"), "yyyy-MM-dd");
    executionTime = QTime::fromString(executionElement.attribute("Time"), "hh:mm:ss");
    executionPerson = executionElement.attribute("Person");
    executionLocation = executionElement.attribute("Location");

    QDomElement stepsElement = element.firstChildElement("Steps");

    int preCount = 0;
    int stepCount = 0;
    int postCount = 0;
    for (QDomNode n = stepsElement.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == "Precondition") {
                ++preCount;

            } else if (e.tagName() == "Step") {
                ++stepCount;

            } else if (e.tagName() == "Postcondition") {
                ++postCount;
            }
        }
    }
    // skip step loading if amount of steps changed
    if (preCount == preconditionList.size() &&
        stepCount == stepList.size() &&
        postCount == postconditionList.size()) {

        int index = 0;
        for (QDomNode n = stepsElement.firstChild();
             !n.isNull();
             n = n.nextSibling()) {
            QDomElement e = n.toElement();
            if (!e.isNull()) {
                Step* step = getStep(index);
                if (step) {
                    step->loadReport(e);
                }
                ++index;
            }
        }
    } else if (preCount+stepCount+postCount > 0) {
        reportChanged = true;
    }
    return true;
}
void Testcase::removeReport() {
    for (Step* step : getStepList()) {
        step->removeReport();
    }
    executionVariant = "";
    executionDate = QDate();
    executionTime = QTime();
    executionPerson = "";
    executionLocation = "";
    reportChanged = true;
}

Feature* Testcase::getParent() const {
    return parent;
}
int Testcase::getIndexNumber() const {
    return parent->getTestcaseList().indexOf(
            dynamic_cast<TestSpecItem*>(
                    const_cast<Testcase*>(this)));
}

QString Testcase::getId() const {
    return id;
}
void Testcase::setId(const QString& id) {
    specChanged = true;
    this->id = id;
    stateDate = QDate::currentDate();
}
QDate Testcase::getStateDate() const {
    return stateDate;
}
QString Testcase::getState() const {
    return state;
}
void Testcase::setState(const QString& state) {
    specChanged = true;
    this->state = state;
    stateDate = QDate::currentDate();
}
void Testcase::resetTestState() {
    if (Settings::getValue("Test/EnableChangeToDefaultState").toBool()) {
        setState(Settings::getValue("Test/DefaultState").toString());
    }
}

QString Testcase::getComment() const {
    return comment;
}
void Testcase::setComment(const QString& text) {
    specChanged = true;
    comment = text;
    stateDate = QDate::currentDate();
    resetTestState();
}
QString Testcase::getDescription() const {
    return description;
}
void Testcase::setDescription(const QString& text) {
    specChanged = true;
    description = text;
    stateDate = QDate::currentDate();
    resetTestState();
}
void Testcase::updateExecutionState() {
    executionDate = QDate::currentDate();
    executionTime = QTime::currentTime();
    QString name = qgetenv("USER");
    if (name.isEmpty()) {
        name = qgetenv("USERNAME");
    }
    executionPerson = name;
}

int Testcase::getStepIndex(Step* step) {
    int index = preconditionList.indexOf(step);
    if (index >= 0) {
        return index;
    }
    index = stepList.indexOf(step);
    if (index >= 0) {
        return index;
    }
    index = postconditionList.indexOf(step);
    return index;
}
Step* Testcase::getStep(int index) const {
    if (index < 0) {
        return NULL;
    }

    // pre conditions
    if (index < preconditionList.size()) {
        return preconditionList.at(index);
    }
    index -= preconditionList.size();

    // normal steps
    if (index < stepList.size()) {
        return stepList.at(index);
    }
    index -= stepList.size();

    // post conditions
    if (index < postconditionList.size()) {
        return postconditionList.at(index);
    } else {
        return NULL;
    }
}
QList<Step*> Testcase::getStepList() const {
    QList<Step*> steps;
    steps.append(preconditionList);
    steps.append(stepList);
    steps.append(postconditionList);
    return steps;
}
Step* Testcase::addStep(int pos, Step::Type type) {
    Step* step = new Step(this, type);
    if (type == Step::Pre) {
        if (pos < 0) {
            pos = 0;
        }
        preconditionList.insert(pos, step);

    } else if (type == Step::Main) {
        pos -= preconditionList.size();
        if (pos < 0) {
            pos = 0;
        }
        stepList.insert(pos, step);

    } else if (type == Step::Post) {
        pos -= preconditionList.size();
        pos -= stepList.size();
        if (pos < 0) {
            pos = 0;
        }
        postconditionList.insert(pos, step);

    } else {
        delete(step);
        step = NULL;
    }
    specChanged = true;
    return step;
}
bool Testcase::removeStep(Step* step) {
    if (preconditionList.removeOne(step)) {
        delete(step);
    } else if (stepList.removeOne(step)) {
        delete(step);
    } else if (postconditionList.removeOne(step)) {
        delete(step);
    } else {
        return false;
    }
    specChanged = true;
    return true;
}
bool Testcase::moveStep(Step* step, int pos) {
    Step::Type type = step->getType();
    if (type == Step::Pre) {
        if (pos < 0) {
            pos = 0;
        }
        preconditionList.removeOne(step);
        preconditionList.insert(pos, step);

    } else if (type == Step::Main) {
        pos -= preconditionList.size();
        if (pos < 0) {
            pos = 0;
        }
        stepList.removeOne(step);
        stepList.insert(pos, step);

    } else if (type == Step::Post) {
        pos -= preconditionList.size();
        pos -= stepList.size();
        if (pos < 0) {
            pos = 0;
        }
        postconditionList.removeOne(step);
        postconditionList.insert(pos, step);

    } else {
        return false;
    }
    specChanged = true;
    return true;
}
QString Testcase::getStepState() {
    bool passed = true;
    for (Step* step : getStepList()) {
        if (step->getResultState() == "failed") {
            return "failed";
        }
        passed = passed && step->getResultState() == "passed";
    }
    if (passed && !getStepList().isEmpty()) {
        return "passed";
    } else {
        return "";
    }
}
int Testcase::getExecutedStep() {
    int counter = 0;
    for (Step* step : getStepList()) {
        if (!step->getResultState().isEmpty() &&
            step->getResultState() != "notexecuted") {
            ++counter;
        }
    }
    return counter;
}

void Testcase::addRequirement(const QString& requirement) {
    if (!requirements.contains(requirement)) {
        requirements.append(requirement);
        specChanged = true;
    }
}
void Testcase::removeRequirement(const QString& requirement) {
    requirements.removeAll(requirement);
    specChanged = true;
}
QStringList Testcase::getRequirements() const {
    return requirements;
}

bool Testcase::isSpecChanged() const {
    if (specChanged) {
        return true;
    }
    for (Step* step : preconditionList) {
        if (step->isSpecChanged()) {
            return true;
        }
    }
    for (Step* step : stepList) {
        if (step->isSpecChanged()) {
            return true;
        }
    }
    for (Step* step : postconditionList) {
        if (step->isSpecChanged()) {
            return true;
        }
    }
    return false;
}
bool Testcase::isReportChanged() const {
    if (reportChanged || specChanged) {
        return true;
    }
    for (Step* step : preconditionList) {
        if (step->isReportChanged()) {
            return true;
        }
    }
    for (Step* step : stepList) {
        if (step->isReportChanged()) {
            return true;
        }
    }
    for (Step* step : postconditionList) {
        if (step->isReportChanged()) {
            return true;
        }
    }
    return false;
}
bool Testcase::isEnabled() const {
    return enabled;
}
void Testcase::setEnabled(bool enabled) {
    this->enabled = enabled;
}

QDomElement Testcase::save(QDomDocument& doc, const QString& basePath,
                           bool report) {
    
    if (Settings::getValue("Test/EnableTestSeparation").toBool()) {
        // create include element
        QDomElement include = doc.createElement("xi:include");
        include.setAttribute("href", href);
        include.setAttribute("parse", "xml");

        QDir dir(basePath);
        QString testFile = dir.filePath(href);

        QDir incDir = QFileInfo(testFile).dir();
        if (!incDir.exists()) {
            incDir.mkpath(".");
        }

        if ((!report && isSpecChanged()) ||  // changed spec
            (report && isReportChanged()) || // changed report
            !QFile::exists(testFile)) {      // file not exist

            // open output file
            QFile file(testFile);
            if (Settings::getApplicationValue("Gen/OverwriteReadOnly").toBool()) {
                file.setPermissions(file.permissions() | QFile::WriteUser | QFile::WriteOwner | QFile::WriteOther);
            }
            if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
                throw Exception(Exception::FileOpenFailed,
                                "Failed to save file " + testFile + ": " +
                                file.errorString());
            }

            // create test document
            QDomDocument testDoc;
            testDoc.appendChild(
                    testDoc.createProcessingInstruction(
                            "xml", "version=\"1.0\" encoding=\"utf-8\""));
            testDoc.appendChild(dump(testDoc, report));
            testDoc.documentElement().setAttribute("xmlns", "http://www.younix.biz/docgen/include.xsd");

            // create destination path
            QFileInfo info(testFile);
            info.dir().mkpath(".");

            // write file
            file.write(testDoc.toByteArray(4));
            file.close();
        }

        if (report) {
            reportChanged = false;
        } else {
            specChanged = false;
        }
        return include;
        
    } else {
        if (report) {
            reportChanged = false;
        } else {
            specChanged = false;
        }
        return dump(doc, report);
    }
}

QDomElement Testcase::dump(QDomDocument& doc, bool report) {
    QDomElement element = doc.createElement("Testcase");
    element.setAttribute("Id", id);

    QDomElement stateElement = doc.createElement("State");
    stateElement.setAttribute("date", stateDate.toString("yyyy-MM-dd"));
    stateElement.appendChild(doc.createTextNode(state));
    element.appendChild(stateElement);

    if (!requirements.isEmpty()) {
        QDomElement requirementsElement = doc.createElement("CoveredRequirements");
        for (QString req : requirements) {
            QDomElement e = doc.createElement("CoveredRequirement");
            e.setAttribute("Id", req);
            requirementsElement.appendChild(e);
        }
        element.appendChild(requirementsElement);
    }

    if (report) {
        QDomElement executionElement = doc.createElement("Execution");
        if (!executionVariant.isEmpty()) {
            executionElement.setAttribute("Variant", executionVariant);
        }
        if (executionDate.isValid()) {
            executionElement.setAttribute("Date",
                                          executionDate.toString("yyyy-MM-dd"));
        }
        if (executionTime.isValid()) {
            executionElement.setAttribute("Time",
                                          executionTime.toString("hh:mm:ss"));
        }
        if (!executionPerson.isEmpty()) {
            executionElement.setAttribute("Person", executionPerson);
        }
        if (!executionLocation.isEmpty()) {
            executionElement.setAttribute("Location", executionLocation);
        }
        if (element.hasChildNodes()) {
            element.appendChild(executionElement);
        }
    }

    if (!comment.isEmpty()) {
        QDomElement commentElement = doc.createElement("Comment");
        XmlHelper::addXmlContent(&commentElement, comment);
        element.appendChild(commentElement);
    }

    if (!description.isEmpty()) {
        QDomElement descriptionElement = doc.createElement("Description");
        XmlHelper::addXmlContent(&descriptionElement, description);
        element.appendChild(descriptionElement);
    }

    QDomElement stepsElement = doc.createElement("Steps");
    for (Step* step : preconditionList) {
        stepsElement.appendChild(step->save(doc, report));
    }
    for (Step* step : stepList) {
        stepsElement.appendChild(step->save(doc, report));
    }
    for (Step* step : postconditionList) {
        stepsElement.appendChild(step->save(doc, report));
    }
    element.appendChild(stepsElement);

    if (!annotationsElement.isNull()) {
        element.appendChild(annotationsElement.cloneNode(true));
    }

    return element;
}

void Testcase::checkHref() {
    // create href if not exist
    if (href.isEmpty() && Settings::getValue("Test/EnableTestSeparation").toBool()) {
        if (Settings::getValue("Test/EnableFeatureSeparation").toBool()) {
            href = "";
        } else {
            href = Settings::getValue("Test/Subdirectory").toString();
            if (!href.endsWith("/")) {
                href += "/";
            }
        }
        href += getParent()->getParent()->getName() + "_";
        if (getParent()->getName().isEmpty()) {
            href += getParent()->getId() + "_";
        } else {
            href += getParent()->getName() + "_";
        }
        href += id + ".xml";
        specChanged = true;
    }
}
