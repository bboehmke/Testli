/**
 * @file Instruction.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_INSTRUCTION_H
#define TESTLI_INSTRUCTION_H

#include <QObject>
#include <QDomElement>
#include <QString>
#include <QMap>
#include <QLayout>

/**
 * Base instruction
 */
class Instruction : public QObject {
    Q_OBJECT
    public:
        /**
         * Create instruction
         * @param name Function name
         */
        Instruction(QString name);

        /**
         * Get function name of instruction
         * @return Name of function
         */
        QString getName() const;

        /**
         * Create GUI for instruction settings
         * @param parent Parent widget
         * @return Layout instance
         */
        virtual QLayout* getSettingLayout(QWidget* parent) = 0;

        /**
         * Check if action can run
         * @return True if run is supported
         */
        virtual bool canRun() const;
        /**
         * Start execution of instruction
         */
        virtual void run() = 0;

        /**
         * Check if instruction changed since last dump
         * @return True if changed
         */
        bool isChanged() const;

        /**
         * Dump instruction
         * @param doc XML destination document
         * @param report True if report save
         * @return XML element
         */
        virtual QDomElement dump(QDomDocument& doc, bool report) = 0;

    public slots:
        /**
         * Stop execution
         */
        virtual void stop();

    signals:
        /**
         * Log message to console
         * @param text Text to log
         */
        void logConsole(QString text);
        /**
         * Log error message to console
         * @param text Text to log
         */
        void logConsoleError(QString text);
        /**
         * Emitted if instruction finished
         * @param returnCode Execution return code
         */
        void finished(int returnCode);

    protected:
        /**
         * True if changed
         */
        bool changed;

        /**
         * Create dump of instruction
         * @param doc XML destination document
         * @param attributes List of attributes
         * @param data Content of instructions
         * @return XML element
         */
        QDomElement createDump(QDomDocument& doc,
                               const QMap<QString, QString>& attributes,
                               const QString& data);

        /**
         * Get working directory
         * @return Path to working directory
         */
        QString getWorkingDir() const;

        /**
         * Get the full path of the given tool
         * @param tool Tool executable name
         * @return Path to tool
         */
        QString getToolPath(const QString& tool) const;

        /**
         * Handle variables in string
         * @param data Data to handle
         * @return String with resolved variables
         */
        QString handleVariables(QString data);

    private:
        /**
         * Function name of instruction
         */
        QString name;

        /**
         * Handle the given variable
         * @param key Key of variable
         * @param value Content of variable
         * @return Handled variable
         */
        QString handleVariable(const QString& key, const QString& value);
};


#endif //TESTLI_INSTRUCTION_H
