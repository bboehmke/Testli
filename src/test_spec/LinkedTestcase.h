/**
 * @file LinkedTestcase.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TESTLI_LINKEDTESTCASE_H
#define TESTLI_LINKEDTESTCASE_H

#include <QList>
#include <QDomElement>

#include "TestSpecItem.h"
#include "Testcase.h"

class Feature;

/**
 * Class to handle linked testcase element
 */
class LinkedTestcase : public TestSpecItem {
    public:
        /**
         * Create linked testcase element from XML element
         * @param parent Parent element
         * @param element XML element
         */
        LinkedTestcase(Feature* parent, const QDomElement& element);
        /**
         * Create linked testcase element
         * @param parent Parent element
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         */
        LinkedTestcase(Feature* parent, const QString& featureId,
                       const QString& testcaseId);

        /**
         * Get unique name
         * @return Unique name of item
         */
        QString getUniqueName() const;

        /**
         * Parent item
         * @return Parent instance
         */
        Feature* getParent() const;
        /**
         * Index of this element
         * @return Index
         */
        int getIndexNumber() const;

        /**
         * Get feature id
         * @return Id of feature
         */
        QString getFeatureId() const;
        /**
         * Set feature id
         * @param id Feature id
         */
        void setFeatureId(const QString& id);
        /**
         * Get testcase id
         * @return Id of testcase
         */
        QString getTestcaseId() const;
        /**
         * Set testcase id
         * @param id Testcase id
         */
        void setTestcaseId(const QString& id);

        /**
         * Check if element is enabled
         * @return True if enabled
         */
        bool isEnabled() const;
        /**
         * Enable element
         * @param enabled True if enabled
         */
        void setEnabled(bool enabled);

        /**
         * Get linked testcase
         * @return Testcase instance or NULL if invalid
         */
        Testcase* getTestcase() const;

        /**
         * Save element
         * @param doc XML document
         * @return XML element
         */
        QDomElement save(QDomDocument& doc);

    private:
        /**
         * Parent element
         */
        Feature* parent;

        /**
         * Id of feature
         */
        QString featureId;

        /**
         * Id of testcase
         */
        QString testcaseId;

        /**
         * True if testcase is enabled in report
         */
        bool enabled;
};


#endif //TESTLI_LINKEDTESTCASE_H
