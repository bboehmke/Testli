/**
 * @file TestSpec.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TESTSPEC_H
#define TESTLI_TESTSPEC_H

#include <QList>
#include <QDomElement>
#include <QAbstractItemModel>

#include "TestSpecItem.h"
#include "FeatureGroup.h"

/**
 * Test specification
 */
class TestSpec : public TestSpecItem {
    public:
        /**
         * Create test specification
         * @param specPath Path to test specification
         * @param disabledTests List of disabled test cases
         */
        TestSpec(const QString& specPath,
                 const QStringList& disabledTests);
        ~TestSpec();

        /**
         * Get path to test specification
         * @return Test spec path
         */
        QString getSpecPath() const;
        /**
         * Get path to test report
         * @return test report path
         */
        QString getReportPath() const;

        /**
         * Check if report exist
         * @return True if report exist
         */
        bool hasReport() const;

        /**
         * Load test report
         * @param path Path to report
         */
        void loadReport(const QString& path);
        /**
         * Remove report components
         */
        void removeReport();

        /**
         * Get feature group element
         * @param index Index of element
         * @return Features element
         */
        FeatureGroup* getFeatureGroup(int index) const;
        /**
         * Get list of existing feature groups
         * @return List of feature groups
         */
        QList<FeatureGroup*> getFeatureGroupList() const;
        /**
         * Add new feature group element
         * @param pos Position to insert
         * @param name Name of element
         * @return Created instance
         */
        FeatureGroup* addFeatureGroup(int pos, const QString &name);
        /**
         * Remove element
         * @param featureGroup Element to remove
         * @return True on success
         */
        bool removeFeatureGroup(FeatureGroup* featureGroup);
        /**
         * Move element to new position
         * @param featureGroup Element to move
         * @param pos Destination position
         * @return True on success
         */
        bool moveFeatureGroup(FeatureGroup* featureGroup, int pos);

        /**
         * Check if spec components changed
         * @return True if changed
         */
        bool isSpecChanged() const;
        /**
         * Check if report components changed
         * @return True if changed
         */
        bool isReportChanged() const;

        /**
         * Remove all report results
         */
        void clearReport();

        /**
         * Save test spec
         */
        void saveSpec();
        /**
         * Save test spec
         */
        void saveReport();


    private:
        /**
         * Path to test specification
         */
        QString specPath;
        /**
         * Path to test report
         */
        QString reportPath;

        /**
         * True if spec is integrated in bigger file
         */
        bool integratedSpec;

        /**
         * True if report is integrated in bigger file
         */
        bool integratedReport;

        /**
         * True if spec components changed
         */
        bool specChanged;

        /**
         * True if report components changed
         */
        bool reportChanged;

        /**
         * List of existing feature groups
         */
        QList<FeatureGroup*> featureGroupList;

        /**
         * Load test spec
         * @param root Test spec root
         * @param disabledTests List of disabled test cases
         */
        void loadSpec(QDomElement root,
                      const QStringList& disabledTests);
        /**
         * Load test report
         * @param root Test spec root
         */
        void loadReport(QDomElement root);

        /**
         * Dump test spec
         * @param doc XML document
         * @param element Destination element
         */
        void dumpSpec(QDomDocument& doc, QDomElement* element);
        /**
         * Dump test report
         * @param doc XML document
         * @param element Destination element
         */
        void dumpReport(QDomDocument& doc, QDomElement* element);
};


#endif //TESTLI_TESTSPEC_H
