/**
 * @file LinkedTestcase.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "LinkedTestcase.h"

#include "TestSpec.h"

LinkedTestcase::LinkedTestcase(Feature* parent, const QDomElement& element) :
        parent(parent), enabled(true) {

    featureId = element.attribute("FeatureId");
    testcaseId = element.attribute("TestcaseId");
}

LinkedTestcase::LinkedTestcase(Feature* parent, const QString& featureId,
                               const QString& testcaseId) :
        parent(parent), featureId(featureId), testcaseId(testcaseId),
        enabled(true) {

}

QString LinkedTestcase::getUniqueName() const {
    return parent->getUniqueName() + "." + featureId + "." + testcaseId;
}

Feature* LinkedTestcase::getParent() const {
    return parent;
}
int LinkedTestcase::getIndexNumber() const {
    return parent->getTestcaseList().indexOf(
            dynamic_cast<TestSpecItem*>(
                    const_cast<LinkedTestcase*>(this)));
}

QString LinkedTestcase::getFeatureId() const {
    return featureId;
}
void LinkedTestcase::setFeatureId(const QString& id) {
    featureId = id;
}
QString LinkedTestcase::getTestcaseId() const {
    return testcaseId;
}
void LinkedTestcase::setTestcaseId(const QString& id) {
    testcaseId = id;
}

bool LinkedTestcase::isEnabled() const {
    return enabled;
}
void LinkedTestcase::setEnabled(bool enabled) {
    this->enabled = enabled;
}

Testcase* LinkedTestcase::getTestcase() const {
    TestSpec* spec = parent->getParent()->getParent();
    for (FeatureGroup* featureGroup : spec->getFeatureGroupList()) {
        for (Feature* feature : featureGroup->getFeatureList()) {
            if (feature->getId() != featureId) {
                continue;
            }
            for (TestSpecItem* item : feature->getTestcaseList()) {
                Testcase* testcase = dynamic_cast<Testcase*>(item);
                if (testcase && testcase->getId() == testcaseId) {
                    return testcase;
                }
            }
            
        }
    }
    return NULL;
}

QDomElement LinkedTestcase::save(QDomDocument& doc) {
    QDomElement element = doc.createElement("LinkedTestcase");
    element.setAttribute("FeatureId", featureId);
    element.setAttribute("TestcaseId", testcaseId);
    return element;
}