/**
 * @file Feature.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Feature.h"

#include <QDir>

#include "../misc/XmlHelper.h"

#include "FeatureGroup.h"
#include "../Settings.h"
#include "../Exception.h"

Feature::Feature(FeatureGroup* parent, const QDomElement& element,
                 const QStringList& disabledTests) :
        parent(parent), specChanged(false), reportChanged(false) {
    name = element.attribute("Name");
    id = element.attribute("Id");
    href = element.attribute("__href");

    checkHref();

    QString basePath = XmlHelper::getRootAttribute(element, "__basePath");

    for (QDomNode n = element.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == "Testcase") {
                Testcase* tc = new Testcase(this, e);
                tc->setEnabled(!disabledTests.contains(tc->getUniqueName()));
                testcaseList.append(tc);

            } else if (e.tagName() == "LinkedTestcase") {
                LinkedTestcase* tc = new LinkedTestcase(this, e);
                tc->setEnabled(!disabledTests.contains(tc->getUniqueName()));
                testcaseList.append(tc);

            } else if(e.tagName().endsWith(":include")) {
                // load include file
                QDomElement testcase = XmlHelper::loadXml(
                        basePath + "/" + e.attribute("href")).documentElement();
                testcase.setAttribute("__href", e.attribute("href"));
                testcase.setAttribute("__basePath",
                                      QFileInfo(basePath + "/" + e.attribute("href")).absolutePath());

                Testcase* tc = new Testcase(this, testcase);
                tc->setEnabled(!disabledTests.contains(tc->getUniqueName()));
                testcaseList.append(tc);
            }
        }
    }
}
Feature::Feature(FeatureGroup* parent, const QString& name, const QString& id) :
        parent(parent), specChanged(true), reportChanged(false),
        name(name), id(id) {

    checkHref();
}

Feature::~Feature() {
    for (TestSpecItem* item : testcaseList) {
        delete(item);
    }
}
QString Feature::getUniqueName() const {
    return parent->getUniqueName() + "." + id + "." + name;
}

bool Feature::loadReport(const QDomElement& element) {
    if ((!id.isEmpty() && element.attribute("Id") != id) ||
        (id.isEmpty() && element.attribute("Name") != name)) {
        return false;
    }
    QString basePath = XmlHelper::getRootAttribute(element, "__basePath");

    for (QDomNode n = element.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == "Testcase") {

                bool changed = true;
                for (TestSpecItem* item : testcaseList) {
                    Testcase* testcase = dynamic_cast<Testcase*>(item);
                    if (testcase && testcase->getId() == e.attribute("Id")) {
                        testcase->loadReport(e);
                        changed = !testcase->isEnabled();
                        break;
                    }
                }
                reportChanged = reportChanged || changed;

            } else if(e.tagName().endsWith(":include")) {
                // load include file
                QDomElement testcase = XmlHelper::loadXml(
                        basePath + "/" + e.attribute("href")).documentElement();
                testcase.setAttribute("__href", e.attribute("href"));

                bool changed = true;
                for (TestSpecItem* item : testcaseList) {
                    Testcase* tc = dynamic_cast<Testcase*>(item);
                    if (tc && tc->getId() == testcase.attribute("Id")) {
                        tc->loadReport(testcase);
                        changed = !tc->isEnabled();
                        break;
                    }
                }
                reportChanged = reportChanged || changed;
            }
        }
    }

    return true;
}
void Feature::removeReport() {
    for (TestSpecItem* item : testcaseList) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        if (testcase) {
            testcase->removeReport();
        }
    }
    reportChanged = true;
}

FeatureGroup* Feature::getParent() const {
    return parent;
}
int Feature::getIndexNumber() const {
    return parent->getFeatureList().indexOf(const_cast<Feature*>(this));
}

QString Feature::getName() const {
    return name;
}
void Feature::setName(const QString& name) {
    specChanged = true;
    this->name = name;
}
QString Feature::getId() const {
    return id;
}
void Feature::setId(const QString& id) {
    specChanged = true;
    this->id = id;
}

TestSpecItem* Feature::getTestcase(int index) const {
    if (index < 0 || index >= testcaseList.size()) {
        return NULL;
    }
    return testcaseList.at(index);
}
QList<TestSpecItem*> Feature::getTestcaseList() const {
    return testcaseList;
}
Testcase* Feature::addTestcase(int pos, const QString& id) {
    Testcase* testcase = new Testcase(this, id);

    // add default steps
    if (Settings::getValue("Test/EnableDefaultPrecond").toBool()) {
        testcase->addStep(0, Step::Pre);
    }
    if (Settings::getValue("Test/EnableDefaultStep").toBool()) {
        testcase->addStep(1, Step::Main);
    }
    if (Settings::getValue("Test/EnableDefaultPostcond").toBool()) {
        testcase->addStep(2, Step::Post);
    }

    if (pos < 0) {
        testcaseList.append(testcase);
    } else {
        testcaseList.insert(pos, testcase);
    }
    specChanged = true;
    return testcase;
}
LinkedTestcase* Feature::addLinkedTestcase(int pos, const QString& featureId,
                                           const QString& testcaseId) {
    LinkedTestcase* testcase = new LinkedTestcase(this, featureId, testcaseId);
    if (pos < 0) {
        testcaseList.append(testcase);
    } else {
        testcaseList.insert(pos, testcase);
    }
    specChanged = true;
    return testcase;
}
bool Feature::removeTestcase(TestSpecItem* testcase) {
    int index = testcaseList.indexOf(testcase);
    if (index < 0) {
        return false;
    }
    specChanged = true;
    testcaseList.removeAt(index);
    delete(testcase);
    return true;
}
bool Feature::moveTestcase(TestSpecItem* testcase, int pos) {
    int index = testcaseList.indexOf(testcase);
    if (index < 0) {
        return false;
    }
    specChanged = true;
    testcaseList.removeAt(index);
    testcaseList.insert(pos, testcase);
    return true;
}
bool Feature::isSpecChanged() const {
    if (specChanged) {
        return true;
    }
    for (TestSpecItem* item : testcaseList) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        if (testcase && testcase->isSpecChanged()) {
            return true;
        }
    }
    return false;
}
bool Feature::isReportChanged() const {
    if (reportChanged || specChanged) {
        return true;
    }
    for (TestSpecItem* item : testcaseList) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        if (testcase && testcase->isReportChanged()) {
            return true;
        }
    }
    return false;
}
bool Feature::isEnabled() const {
    for (TestSpecItem* item : testcaseList) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
        if (testcase && testcase->isEnabled()) {
            return true;
        } else if (linkedTestcase && linkedTestcase->isEnabled()) {
            return true;
        }
    }
    return false;
}
void Feature::setEnabled(bool enabled) {
    for (TestSpecItem* item : testcaseList) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
        if (testcase) {
            testcase->setEnabled(enabled);
        } else if (linkedTestcase) {
            linkedTestcase->setEnabled(enabled);
        }
    }
}

QDomElement Feature::save(QDomDocument& doc, const QString& basePath,
                          bool report) {

    if (Settings::getValue("Test/EnableFeatureSeparation").toBool()) {
        // create include element
        QDomElement include = doc.createElement("xi:include");
        include.setAttribute("href", href);
        include.setAttribute("parse", "xml");

        QDir dir(basePath);
        QString testFile = dir.filePath(href);

        QDir incDir = QFileInfo(testFile).dir();
        if (!incDir.exists()) {
            incDir.mkpath(".");
        }

        if ((!report && isSpecChanged()) ||  // changed spec
            (report && isReportChanged()) || // changed report
            !QFile::exists(testFile)) {      // file not exist

            // open output file
            QFile file(testFile);
            if (Settings::getApplicationValue("Gen/OverwriteReadOnly").toBool()) {
                file.setPermissions(file.permissions() | QFile::WriteUser | QFile::WriteOwner | QFile::WriteOther);
            }
            if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
                throw Exception(Exception::FileOpenFailed,
                                "Failed to save feature file " + testFile + ": " +
                                file.errorString());
            }

            // create test document
            QDomDocument testDoc;
            testDoc.appendChild(
                    testDoc.createProcessingInstruction(
                            "xml", "version=\"1.0\" encoding=\"utf-8\""));
            testDoc.appendChild(dump(testDoc, incDir.absolutePath(), report));
            testDoc.documentElement().setAttribute("xmlns", "http://www.younix.biz/docgen/include.xsd");

            // create destination path
            QFileInfo info(testFile);
            info.dir().mkpath(".");

            // write file
            file.write(testDoc.toByteArray(4));
            file.close();
        }

        if (report) {
            reportChanged = false;
        } else {
            specChanged = false;
        }
        return include;

    } else {
        if (report) {
            reportChanged = false;
        } else {
            specChanged = false;
        }
        return dump(doc, basePath, report);
    }
}

QDomElement Feature::dump(QDomDocument& doc, const QString& basePath,
                          bool report) {
    QDomElement element = doc.createElement("Feature");
    element.setAttribute("Name", name);
    if (!id.isEmpty()) {
        element.setAttribute("Id", id);
    }

    for (TestSpecItem* item : testcaseList) {
        Testcase* testcase = dynamic_cast<Testcase*>(item);
        LinkedTestcase* linkedTestcase = dynamic_cast<LinkedTestcase*>(item);
        if (testcase) {
            if (!report || testcase->isEnabled()) {
                element.appendChild(testcase->save(doc, basePath, report));
            }
        } else if(linkedTestcase) {
            if (!report || linkedTestcase->isEnabled()) {
                element.appendChild(linkedTestcase->save(doc));
            }
        }
    }
    return element;
}

void Feature::checkHref() {
    // create href if not exist
    if (href.isEmpty() && Settings::getValue("Test/EnableFeatureSeparation").toBool()) {
        href = Settings::getValue("Test/Subdirectory").toString();
        if (!href.endsWith("/")) {
            href += "/";
        }
        href += getParent()->getName() + "_" + name + ".xml";
        specChanged = true;
    }
}