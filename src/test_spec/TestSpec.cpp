/**
 * @file TestSpec.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestSpec.h"

#include <QFile>
#include <QFileInfo>

#include "../misc/XmlHelper.h"
#include "../Exception.h"
#include "../Settings.h"

TestSpec::TestSpec(const QString& specPath,
                   const QStringList& disabledTests) :
        specPath(specPath), integratedSpec(false), integratedReport(false),
        specChanged(false) {

    // stop if not exist -> new test spec
    if (!QFile::exists(specPath)) {
        return;
    }

    // get root element
    QDomElement root = XmlHelper::loadXml(specPath).documentElement();
    root.setAttribute("__basePath", QFileInfo(specPath).absolutePath());

    if (root.tagName() == "TestSpec") {
        loadSpec(root, disabledTests);
        integratedSpec = false;
    } else {
        QDomElement element = XmlHelper::findElement(root, "TestSpec");
        if (element.isNull()) {
            throw Exception(Exception::XmlError, "Invalid TestSpec");
        }
        loadSpec(element, disabledTests);
        integratedSpec = true;
    }
}

QString TestSpec::getSpecPath() const {
    return specPath;
}
QString TestSpec::getReportPath() const {
    return reportPath;
}
bool TestSpec::hasReport() const {
    return !reportPath.isEmpty();
}
void TestSpec::loadReport(const QString& path) {
    // stop if not exist -> new test report
    if (!QFile::exists(path)) {
        if (!reportPath.isEmpty()) {
            reportChanged = true;
        }
        reportPath = path;
        integratedReport = false;
        return;
    }

    // get root element
    QDomElement root = XmlHelper::loadXml(path).documentElement();
    root.setAttribute("__basePath", QFileInfo(path).absolutePath());

    if (root.tagName() == "TestReport") {
        loadReport(root);
        integratedReport = false;
    } else {
        QDomElement element = XmlHelper::findElement(root, "TestReport");
        if (element.isNull()) {
            throw Exception(Exception::XmlError, "Invalid TestReport");
        }
        loadReport(element);
        integratedReport = true;
    }
    reportPath = path;
}
void TestSpec::removeReport() {
    for (FeatureGroup* featureGroups : featureGroupList) {
        featureGroups->removeReport();
    }
    reportPath = "";
    reportChanged = false;
}

TestSpec::~TestSpec() {
    for (FeatureGroup* featureGroups : featureGroupList) {
        delete(featureGroups);
    }
}

FeatureGroup* TestSpec::getFeatureGroup(int index) const {
    if (index < 0 || index >= featureGroupList.size()) {
        return NULL;
    }
    return featureGroupList.at(index);
}
QList<FeatureGroup*> TestSpec::getFeatureGroupList() const {
    return featureGroupList;
}
FeatureGroup* TestSpec::addFeatureGroup(int pos, const QString &name) {
    FeatureGroup* featureGroup = new FeatureGroup(this, name);
    if (pos < 0) {
        featureGroupList.append(featureGroup);
    } else {
        featureGroupList.insert(pos, featureGroup);
    }
    specChanged = true;
    return featureGroup;
}
bool TestSpec::removeFeatureGroup(FeatureGroup* featureGroup) {
    int index = featureGroupList.indexOf(featureGroup);
    if (index < 0) {
        return false;
    }
    specChanged = true;
    featureGroupList.removeAt(index);
    delete(featureGroup);
    return true;
}
bool TestSpec::moveFeatureGroup(FeatureGroup* featureGroup, int pos) {
    int index = featureGroupList.indexOf(featureGroup);
    if (index < 0) {
        return false;
    }
    specChanged = true;
    featureGroupList.removeAt(index);
    featureGroupList.insert(pos, featureGroup);
    return true;
}
bool TestSpec::isSpecChanged() const {
    if (specChanged) {
        return true;
    }
    for (FeatureGroup* featureGroup : featureGroupList) {
        if (featureGroup->isSpecChanged()) {
            return true;
        }
    }
    return false;
}
bool TestSpec::isReportChanged() const {
    if (!hasReport()) {
        return false;
    }
    if (reportChanged || specChanged) {
        return true;
    }
    for (FeatureGroup* featureGroup : featureGroupList) {
        if (featureGroup->isReportChanged()) {
            return true;
        }
    }
    return false;
}
void TestSpec::clearReport() {
    for (FeatureGroup* featureGroup : featureGroupList) {
        featureGroup->removeReport();
    }
    reportChanged = true;
}

void TestSpec::saveSpec() {
    // no save if no path
    if (specPath.isEmpty()) {
        return;
    }
    QDomDocument doc;
    if (integratedSpec) {
        doc = XmlHelper::loadXml(specPath);
        QDomElement root = doc.documentElement();

        QDomElement element = XmlHelper::findElement(root, "TestSpec");
        if (element.isNull()) {
            throw Exception(Exception::XmlError, "Invalid TestSpec");
        }

        QDomElement specElement = doc.createElement("TestSpec");
        dumpSpec(doc, &specElement);

        element.parentNode().replaceChild(specElement, element);

    } else {
        doc.appendChild(
                doc.createProcessingInstruction(
                        "xml", "version=\"1.0\" encoding=\"utf-8\""));
        QDomElement specElement = doc.createElement("TestSpec");
        specElement.setAttribute("xmlns:xi", "http://www.w3.org/2003/XInclude");
        specElement.setAttribute("xmlns", "http://www.younix.biz/docgen/include.xsd");
        doc.appendChild(specElement);

        dumpSpec(doc, &specElement);
    }

    // save to file
    QFile file(specPath);
    if (Settings::getApplicationValue("Gen/OverwriteReadOnly").toBool()) {
        file.setPermissions(
                file.permissions() | QFile::WriteUser |
                QFile::WriteOwner | QFile::WriteOther);
    }
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        throw Exception(Exception::FileOpenFailed,
                        "Failed to save file " + specPath + ": " + file.errorString());
    }
    file.write(doc.toByteArray(4));
    file.close();
    specChanged = false;
}

void TestSpec::saveReport() {
    // no save if no path
    if (reportPath.isEmpty()) {
        return;
    }
    QDomDocument doc;
    if (integratedReport) {
        doc = XmlHelper::loadXml(reportPath);
        QDomElement root = doc.documentElement();

        QDomElement element = XmlHelper::findElement(root, "TestReport");
        if (element.isNull()) {
            throw Exception(Exception::XmlError, "Invalid TestReport");
        }

        QDomElement specElement = doc.createElement("TestReport");
        dumpReport(doc, &specElement);

        element.parentNode().replaceChild(specElement, element);

    } else {
        doc.appendChild(
                doc.createProcessingInstruction(
                        "xml", "version=\"1.0\" encoding=\"utf-8\""));
        QDomElement specElement = doc.createElement("TestReport");
        specElement.setAttribute("xmlns:xi", "http://www.w3.org/2003/XInclude");
        specElement.setAttribute("xmlns", "http://www.younix.biz/docgen/include.xsd");
        doc.appendChild(specElement);

        dumpReport(doc, &specElement);
    }

    // save to file
    QFile file(reportPath);
    if (Settings::getApplicationValue("Gen/OverwriteReadOnly").toBool()) {
        file.setPermissions(
                file.permissions() | QFile::WriteUser |
                QFile::WriteOwner | QFile::WriteOther);
    }
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        throw Exception(Exception::FileOpenFailed,
                        "Failed to save file " + reportPath + ": " + file.errorString());
    }
    file.write(doc.toByteArray(4));
    file.close();
    reportChanged = false;
}

void TestSpec::loadSpec(QDomElement root,
                        const QStringList& disabledTests) {
    for (QDomElement e = root.firstChildElement("Features");
         !e.isNull();
         e = e.nextSiblingElement("Features")) {

        featureGroupList.append(new FeatureGroup(this, e, disabledTests));
    }
}
void TestSpec::loadReport(QDomElement root) {
    for (QDomElement e = root.firstChildElement("Features");
         !e.isNull();
         e = e.nextSiblingElement("Features")) {

        bool loaded = false;
        for (FeatureGroup* featureGroup : featureGroupList) {
            if (featureGroup->loadReport(e)) {
                loaded = true;
                break;
            }
        }
        reportChanged = !loaded;
    }
}

void TestSpec::dumpSpec(QDomDocument& doc, QDomElement* element) {
    for (FeatureGroup* featureGroup : featureGroupList) {
        element->appendChild(
                featureGroup->save(doc,
                                   QFileInfo(specPath).absolutePath(),
                                   false));
    }
}
void TestSpec::dumpReport(QDomDocument& doc, QDomElement* element) {
    for (FeatureGroup* featureGroup : featureGroupList) {
        QDomElement e = featureGroup->save(doc,
                                           QFileInfo(reportPath).absolutePath(),
                                           true);
        if (e.hasChildNodes()) {
            element->appendChild(e);
        }
    }
}