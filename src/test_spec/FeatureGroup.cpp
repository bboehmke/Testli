/**
 * @file FeatureGroup.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "FeatureGroup.h"

#include <QFileInfo>

#include "TestSpec.h"
#include "../misc/XmlHelper.h"

FeatureGroup::FeatureGroup(TestSpec* parent, const QDomElement& element,
                           const QStringList& disabledTests) :
        parent(parent), specChanged(false), reportChanged(false) {
    name = element.attribute("Name");

    QString basePath = XmlHelper::getRootAttribute(element, "__basePath");

    for (QDomNode n = element.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull()) {
            if (e.tagName() == "Feature") {
                featureList.append(new Feature(this, e, disabledTests));

            } else if(e.tagName().endsWith(":include")) {
                // load include file
                QDomElement feature = XmlHelper::loadXml(
                        basePath + "/" + e.attribute("href")).documentElement();
                feature.setAttribute("__href", e.attribute("href"));
                feature.setAttribute("__basePath",
                                     QFileInfo(basePath + "/" + e.attribute("href")).absolutePath());

                featureList.append(new Feature(this, feature, disabledTests));
            }

        }
    }
}
FeatureGroup::FeatureGroup(TestSpec* parent, const QString& name) :
        parent(parent), specChanged(true), reportChanged(false), name(name) {

}
FeatureGroup::~FeatureGroup() {
    for (Feature* feature : featureList) {
        delete(feature);
    }
}
QString FeatureGroup::getUniqueName() const {
    return name;
}

bool FeatureGroup::loadReport(const QDomElement& element) {
    if (element.attribute("Name") != name) {
        return false;
    }
    for (QDomElement e = element.firstChildElement("Feature");
         !e.isNull();
         e = e.nextSiblingElement("Feature")) {

        bool loaded = false;
        for (Feature* feature : featureList) {
            if (feature->loadReport(e)) {
                loaded = true;
                break;
            }
        }
        reportChanged = !loaded;
    }
    return true;
}
void FeatureGroup::removeReport() {
    for (Feature* feature : featureList) {
        feature->removeReport();
    }
    reportChanged = true;
}

TestSpec* FeatureGroup::getParent() const {
    return parent;
}
int FeatureGroup::getIndexNumber() const {
    return parent->getFeatureGroupList().indexOf(const_cast<FeatureGroup*>(this));
}

QString FeatureGroup::getName() const {
    return name;
}
void FeatureGroup::setName(const QString& name) {
    specChanged = true;
    this->name = name;
}

Feature* FeatureGroup::getFeature(int index) const {
    if (index < 0 || index >= featureList.size()) {
        return NULL;
    }
    return featureList.at(index);
}
QList<Feature*> FeatureGroup::getFeatureList() const {
    return featureList;
}
Feature* FeatureGroup::addFeature(int pos, const QString& name,
                                  const QString& id) {
    Feature* feature = new Feature(this, name, id);
    if (pos < 0) {
        featureList.append(feature);
    } else {
        featureList.insert(pos, feature);
    }
    specChanged = true;
    return feature;
}
bool FeatureGroup::removeFeature(Feature* feature) {
    int index = featureList.indexOf(feature);
    if (index < 0) {
        return false;
    }
    specChanged = true;
    featureList.removeAt(index);
    delete(feature);
    return true;
}
bool FeatureGroup::moveFeature(Feature* feature, int pos) {
    int index = featureList.indexOf(feature);
    if (index < 0) {
        return false;
    }
    specChanged = true;
    featureList.removeAt(index);
    featureList.insert(pos, feature);
    return true;
}
bool FeatureGroup::isSpecChanged() const {
    if (specChanged) {
        return true;
    }
    for (Feature* feature : featureList) {
        if (feature->isSpecChanged()) {
            return true;
        }
    }
    return false;
}
bool FeatureGroup::isReportChanged() const {
    if (reportChanged || specChanged) {
        return true;
    }
    for (Feature* feature : featureList) {
        if (feature->isReportChanged()) {
            return true;
        }
    }
    return false;
}
bool FeatureGroup::isEnabled() const {
    for (Feature* feature : featureList) {
        if (feature->isEnabled()) {
            return true;
        }
    }
    return false;
}
void FeatureGroup::setEnabled(bool enabled) {
    for (Feature* feature : featureList) {
        if (feature) {
            feature->setEnabled(enabled);
        }
    }
}

QDomElement FeatureGroup::save(QDomDocument &doc, const QString &basePath,
                               bool report) {
    QDomElement element = doc.createElement("Features");
    element.setAttribute("Name", name);

    for (Feature* feature : featureList) {
        QDomElement e = feature->save(doc, basePath, report);
        if (e.hasChildNodes() || e.tagName() == "xi:include") {
            element.appendChild(e);
        }
    }
    if (report) {
        reportChanged = false;
    } else {
        specChanged = false;
    }
    return element;
}