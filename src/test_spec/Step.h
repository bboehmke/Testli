/**
 * @file Step.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_STEP_H
#define TESTLI_STEP_H

#include <QString>
#include <QDomElement>
#include <QDate>
#include <QTime>

#include "Instruction.h"

class Testcase;

/**
 * Class to handle step element
 */
class Step {
    public:
        /**
         * Possible step types
         */
        enum Type {
            Pre,  //!< Precondition
            Main, //!< Main test step
            Post  //!< Postcondition
        };

        /**
         * Create step element from XML element
         * @param parent Parent element
         * @param element XML element
         * @param type Type of step
         */
        Step(Testcase* parent, const QDomElement& element, Type type);
        /**
         * Create empty step element
         * @param parent Parent element
         * @param type Type of step
         */
        Step(Testcase* parent, Type type);
        ~Step();

        /**
         * Load report components
         * @param element XML element
         */
        void loadReport(const QDomElement& element);
        /**
         * Remove report components
         */
        void removeReport();

        /**
         * Parent item
         * @return Parent instance
         */
        Testcase* getParent() const;

        /**
         * Get type of step
         * @return Step type
         */
        Type getType() const;

        /**
         * Get title of step
         * @return Step title
         */
        QString getTitle() const;
        /**
         * Set title of step
         * @param title Step title
         */
        void setTitle(const QString& title);

        /**
         * Get description of step
         * @return Step description
         */
        QString getDescription() const;
        /**
         * Set description of step
         * @param text Step description
         */
        void setDescription(const QString& text);
        /**
         * Get expected result of step
         * @return Expected result
         */
        QString getExpectedResult() const;
        /**
         * Set expected result of step
         * @param text Expected result
         */
        void setExpectedResult(const QString& text);

        /**
         * Get result state of step
         * @return Result state
         */
        QString getResultState() const;
        /**
         * Set result state of step
         * @param state Result state
         */
        void setResultState(const QString& state);
        /**
         * Get result note of step
         * @return Result note
         */
        QString getResultNote() const;
        /**
         * Set result note of step
         * @param note Result note
         */
        void setResultNote(const QString& note);

        /**
         * Get list of logs
         * @return Log list
         */
        QMap<QString, QString> getLogs() const;
        /**
         * Add new log
         * @param name Name of log
         * @param data Content of log
         */
        void addLog(const QString& name, const QString& data);
        /**
         * Remove log
         * @param name Name of log
         */
        void removeLog(const QString& name);

        /**
         * Get instruction
         * @param index Index of element
         * @return Instruction
         */
        Instruction* getInstruction(int index) const;
        /**
         * Get list of existing instructions
         * @return List of instructions
         */
        QList<Instruction*> getInstructionList() const;
        /**
         * Add new instruction
         * @param pos Position to insert
         * @param name Name of instruction
         * @return Created instance
         */
        Instruction* addInstruction(int pos, const QString& name);
        /**
         * Remove instruction
         * @param instruction Instruction to remove
         * @return True on success
         */
        bool removeInstruction(Instruction* instruction);
        /**
         * Move step to new position
         * @param instruction Instruction to move
         * @param pos Destination position
         * @return True on success
         */
        bool moveInstruction(Instruction* instruction, int pos);

        /**
         * Check if spec components changed
         * @return True if changed
         */
        bool isSpecChanged() const;
        /**
         * Check if report components changed
         * @return True if changed
         */
        bool isReportChanged() const;

        /**
         * Save element
         * @param doc XML document
         * @param report True if report save
         * @return XML element
         */
        QDomElement save(QDomDocument& doc, bool report);

    private:
        /**
         * Parent element
         */
        Testcase* parent;

        /**
         * Type of step
         */
        Type type;

        /**
         * True if spec components changed
         */
        bool specChanged;

        /**
         * True if report components changed
         */
        bool reportChanged;

        /**
         * Title of step
         */
        QString title;
        /**
         * Description of step
         */
        QString description;
        /**
         * Expected result
         */
        QString expectedResult;
        /**
         * Annotation id
         */
        QString annotationId;

        /**
         * Date of result
         */
        QDate resultDate;
        /**
         * Time of result
         */
        QTime resultTime;
        /**
         * Result state
         */
        QString resultState;
        /**
         * Result note
         */
        QString resultNote;

        /**
         * List of instructions
         */
        QList<Instruction*> instructionList;

        /**
         * List of files in logger
         */
        QMap<QString, QString> logger;
};


#endif //TESTLI_STEP_H
