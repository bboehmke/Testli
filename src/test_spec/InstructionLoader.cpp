/**
 * @file InstructionLoader.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "InstructionLoader.h"

#include "instructions/GenericInstruction.h"
#include "instructions/ProcessInstruction.h"
#include "instructions/CopyFilesInstruction.h"
#include "instructions/RemoveFilesInstruction.h"
#include "instructions/XsltInstruction.h"
#include "instructions/TcpTermInstruction.h"

InstructionLoader* InstructionLoader::instance = NULL;

InstructionLoader::~InstructionLoader() {
    for (InstructionCreator* creator : creators) {
        delete(creator);
    }
}

Instruction* InstructionLoader::create(const QString& name) {
    if (creators.contains(name)) {
        return creators[name]->create();
    } else {
        return new GenericInstruction(name);
    }
}
Instruction* InstructionLoader::create(const QDomElement& element) {
    if (creators.contains(element.attribute("Function"))) {
        return creators[element.attribute("Function")]->create(element);
    } else {
        return new GenericInstruction(element);
    }
}


QStringList InstructionLoader::getInstructionTypes() const {
    return creators.keys();
}

InstructionLoader* InstructionLoader::getInstance() {
    if (!instance) {
        instance = new InstructionLoader();
        instance->registerInstruction<ProcessInstruction>();
        instance->registerInstruction<CopyFilesInstruction>();
        instance->registerInstruction<RemoveFilesInstruction>();
        instance->registerInstruction<XsltInstruction>();
        instance->registerInstruction<TcpTermInstruction>();
    }
    return instance;
}
void InstructionLoader::cleanup() {
    if (instance) {
        delete(instance);
    }
}