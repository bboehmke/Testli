/**
 * @file Feature.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_FEATURE_H
#define TESTLI_FEATURE_H

#include <QList>
#include <QDomElement>

#include "TestSpecItem.h"
#include "Testcase.h"
#include "LinkedTestcase.h"

class FeatureGroup;

/**
 * Class to handle feature element
 */
class Feature : public TestSpecItem {
    public:
        /**
         * Create feature element from XML element
         * @param parent Features of this element
         * @param element XML element
         * @param disabledTests List of disabled test cases
         */
        Feature(FeatureGroup* parent, const QDomElement& element,
                const QStringList& disabledTests);
        /**
         * Create empty feature element
         * @param parent Features of this element
         * @param name Name of feature element
         * @param id Id of feature element
         */
        Feature(FeatureGroup* parent, const QString& name, const QString& id);
        ~Feature();

        /**
         * Get unique name
         * @return Unique name of item
         */
        QString getUniqueName() const;

        /**
         * Load report components
         * @param element XML element
         * @return True if element was load
         */
        bool loadReport(const QDomElement& element);
        /**
         * Remove report components
         */
        void removeReport();

        /**
         * Parent item
         * @return Parent instance
         */
        FeatureGroup* getParent() const;
        /**
         * Index of this element
         * @return Index
         */
        int getIndexNumber() const;

        /**
         * Get name of feature element
         * @return Name of element
         */
        QString getName() const;
        /**
         * Set name of feature element
         * @param name Name of element
         */
        void setName(const QString& name);
        /**
         * Get id of feature element
         * @return Id of element
         */
        QString getId() const;
        /**
         * Set id of feature element
         * @param id Id of element
         */
        void setId(const QString& id);

        /**
         * Get testcase element
         * @param index Index of element
         * @return Testcase element
         */
        TestSpecItem* getTestcase(int index) const;
        /**
         * Get list of existing testcases
         * @return List of testcases
         */
        QList<TestSpecItem*> getTestcaseList() const;
        /**
         * Add new testcase element
         * @param pos Position to insert
         * @param id Id of element
         * @return Created instance
         */
        Testcase* addTestcase(int pos, const QString& id);
        /**
         * Add new linked testcase element
         * @param pos Position to insert
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         * @return Created instance
         */
        LinkedTestcase* addLinkedTestcase(int pos, const QString& featureId,
                                          const QString& testcaseId);
        /**
         * Remove element
         * @param testcase Element to remove
         * @return True on success
         */
        bool removeTestcase(TestSpecItem* testcase);
        /**
         * Move element to new position
         * @param testcase Element to move
         * @param pos Destination position
         * @return True on success
         */
        bool moveTestcase(TestSpecItem* testcase, int pos);

        /**
         * Check if spec components changed
         * @return True if changed
         */
        bool isSpecChanged() const;
        /**
         * Check if report components changed
         * @return True if changed
         */
        bool isReportChanged() const;
        /**
         * Check if element is enabled
         * @return True if enabled
         */
        bool isEnabled() const;
        /**
         * Enable element
         * @param enabled True if enabled
         */
        void setEnabled(bool enabled);

        /**
         * Save element
         * @param doc XML document
         * @param basePath Base path of test spec
         * @param report True if report save
         * @return XML element
         */
        QDomElement save(QDomDocument& doc, const QString& basePath,
                         bool report);

    private:
        /**
         * Parent element
         */
        FeatureGroup* parent;

        /**
         * True if spec components changed
         */
        bool specChanged;

        /**
         * True if report components changed
         */
        bool reportChanged;

        /**
         * Name of feature element
         */
        QString name;
        /**
         * Id of feature element
         */
        QString id;
        /**
         * Storage path of feature
         */
        QString href;

        /**
         * List of existing testcases
         */
        QList<TestSpecItem*> testcaseList;

        /**
         * Dump feature
         * @param doc Document to dump
         * @param basePath Base path of test spec
         * @param report True if report dump
         * @return Feature element
         */
        QDomElement dump(QDomDocument& doc, const QString& basePath,
                         bool report);

        /**
         * Check if href is valid
         */
        void checkHref();
};


#endif //TESTLI_FEATURE_H
