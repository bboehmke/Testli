/**
 * @file Instruction.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Instruction.h"

#include <QDir>
#include <QXmlQuery>

#include "../Settings.h"
#include "../misc/MessageHandler.h"
#include "../misc/XmlHelper.h"
#include "../Exception.h"
#include "../misc/XmlReceiver.h"


Instruction::Instruction(QString name) : changed(false), name(name) {

}

QString Instruction::getName() const {
    return name;
}
bool Instruction::canRun() const {
    return true;
}
bool Instruction::isChanged() const {
    return changed;
}

QDomElement Instruction::createDump(QDomDocument& doc,
                                    const QMap<QString, QString>& attributes,
                                    const QString& data) {
    QDomElement element = doc.createElement("Instruction");
    QMapIterator<QString, QString> it(attributes);
    while (it.hasNext()) {
        it.next();
        element.setAttribute(it.key(), it.value());
    }
    element.appendChild(doc.createTextNode(data));
    return element;
}
void Instruction::stop() {
    emit(finished(-1));
}

QString Instruction::getWorkingDir() const {
    QDir dir(Settings::getSpecBasePath());
    QString workDir = Settings::getValue("Execution/WorkingDir").toString();
    dir.mkpath(workDir);
    dir.cd(workDir);
    return dir.absolutePath();
}
QString Instruction::getToolPath(const QString& tool) const {
    QDir dir(Settings::getSpecBasePath());
    dir.cd(Settings::getValue("Execution/ToolDir").toString());

    if (QFile::exists(dir.filePath(tool))) {
        return dir.filePath(tool);
    } else if (QFile::exists(dir.filePath(tool + ".exe"))) {
        return dir.filePath(tool);
    } else {
        return tool;
    }
}
QString Instruction::handleVariables(QString data) {
    QRegExp rx("(\\$\\{\\s*)(\\S*)(\\s)([^\\}]*)(\\})");
    while (rx.indexIn(data) != -1) {
        data.replace(rx.cap(0), handleVariable(rx.cap(2).trimmed(), rx.cap(4).trimmed()));
    }
    return data;
}
QString Instruction::handleVariable(const QString& key, const QString& value) {
    if (key.toLower() == "xpath") {
        int index = value.indexOf(" ");
        if (index < 0) {
            emit(logConsoleError("Invalid xpath arguments"));
            return "";
        }

        QString file = getWorkingDir() + "/" + value.left(index);
        QString xpath = value.mid(index+1).trimmed();

        try {
            // get name spaces
            QDomNamedNodeMap attributes =
                    XmlHelper::loadXml(file).documentElement().attributes();

            QString xquery = "";
            for (int i = 0; i < attributes.size(); ++i) {
                QDomAttr attr = attributes.item(i).toAttr();
                if (!attr.isNull()) {
                    if (attr.name().startsWith("xmlns")) {
                        QString ns = "ns";
                        if (attr.name().contains(":")) {
                            ns = attr.name().split(":").at(1);
                        }
                        xquery += "declare namespace " + ns + " = \"" +
                                attr.value() + "\";\n";
                    }
                }

            }
            xquery += xpath;

            // run xpath
            MessageHandler messageHandler;

            QXmlQuery query;
            query.setMessageHandler(&messageHandler);
            query.setFocus(QUrl::fromLocalFile(file));
            query.setQuery(xquery);

            XmlReceiver receiver(&query);
            if (!query.evaluateTo(&receiver)) {
                emit(logConsoleError(QString("XPath failed %1\n")
                                             .arg(messageHandler.getDescription())));
                return "";
            }

            return receiver.getResultValue();

        } catch (Exception& e) {
            emit(logConsoleError(e.getMessage()));
        }
        return "";
    } else {
        return "";
    }
}