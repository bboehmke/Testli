/**
 * @file XsltInstruction.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "XsltInstruction.h"

#include <QGroupBox>
#include <QToolButton>
#include <QFileDialog>
#include <QXmlQuery>

#include "../../misc/MessageHandler.h"

XsltInstruction::XsltInstruction() :
        Instruction("XSLT"), xslEdit(NULL), fileEdit(NULL) {
    changed = true;
}
XsltInstruction::XsltInstruction(const QDomElement& element) :
        Instruction("XSLT"), xslEdit(NULL), fileEdit(NULL) {
    QStringList split = element.text().trimmed().split(";");
    xsl = split.value(0, "");
    file = split.value(1, "");
}

QLayout* XsltInstruction::getSettingLayout(QWidget* parent) {
    QVBoxLayout* layout = new QVBoxLayout(parent);

    QGroupBox* xslGroup = new QGroupBox(tr("XSL script"), parent);
    QHBoxLayout* xslLayout = new QHBoxLayout(xslGroup);
    xslEdit = new QLineEdit(xslGroup);
    xslEdit->setText(xsl);
    xslLayout->addWidget(xslEdit);

    QToolButton* xslSelect = new QToolButton(xslGroup);
    xslSelect->setIcon(QIcon(":/icons/document-open.svg"));
    xslLayout->addWidget(xslSelect);
    layout->addWidget(xslGroup);

    QGroupBox* fileGroup = new QGroupBox(tr("XSL script"), parent);
    QHBoxLayout* fileLayout = new QHBoxLayout(fileGroup);
    fileEdit = new QLineEdit(fileGroup);
    fileEdit->setText(file);
    fileLayout->addWidget(fileEdit);

    QToolButton* fileSelect = new QToolButton(fileGroup);
    fileSelect->setIcon(QIcon(":/icons/document-open.svg"));
    fileLayout->addWidget(fileSelect);
    layout->addWidget(fileGroup);

    connect(xslEdit, SIGNAL(textChanged(const QString&)),
            this, SLOT(on_xslChanged(const QString&)));
    connect(fileEdit, SIGNAL(textChanged(const QString&)),
            this, SLOT(on_fileChanged(const QString&)));
    connect(xslSelect, SIGNAL(pressed()),
            this, SLOT(on_xslSelect()));
    connect(fileSelect, SIGNAL(pressed()),
            this, SLOT(on_fileSelect()));

    return layout;
}

void XsltInstruction::run() {
    QDir workingDir(getWorkingDir());

    // get file paths
    QString xslFile = workingDir.filePath(xsl);
    QString sourceFile = workingDir.filePath(file);
    if (!QFile::exists(sourceFile)) {
        // try glob file
        sourceFile = workingDir.filePath(workingDir.entryList(QStringList() << file, QDir::Files).value(0));
    }

    // check if files exist
    if (!QFile::exists(xslFile)) {
        emit(logConsoleError(QString("Failed to found XSL file \"%1\"\n").arg(xsl)));
        emit(finished(-2));
        return;
    }
    if (!QFile::exists(sourceFile)) {
        emit(logConsoleError(QString("Failed to found source file \"%1\"\n").arg(file)));
        emit(finished(-2));
        return;
    }

    MessageHandler messageHandler;

    QXmlQuery query(QXmlQuery::XSLT20);
    query.setMessageHandler(&messageHandler);
    query.setFocus(QUrl::fromLocalFile(sourceFile));
    query.setQuery(QUrl::fromLocalFile(xslFile));

    QString out;
    if (!query.evaluateTo(&out)) {
        emit(logConsoleError(QString("Transformation failed in line %1 column %2\n")
                                     .arg(messageHandler.getSourceLocation().line())
                                     .arg(messageHandler.getSourceLocation().column())));
        emit(logConsoleError(QString("%1\n").arg(messageHandler.getDescription())));
        emit(finished(-3));
        return;
    }

    // save to file
    QFile file(sourceFile);
    file.setPermissions(file.permissions() | QFile::WriteUser |
                                QFile::WriteOwner | QFile::WriteOther);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        emit(logConsoleError("Failed to save file " + sourceFile +
                                     ": " + file.errorString()));
        emit(finished(-4));
    }
    file.write(out.toUtf8());
    file.close();

    emit(finished(0));
}

QDomElement XsltInstruction::dump(QDomDocument& doc, bool report) {
    if (!report) {
        changed = false;
    }
    QMap<QString, QString> attr;
    attr.insert("Function", getName());
    return createDump(doc, attr, QString("%1;%2").arg(xsl).arg(file));
}

void XsltInstruction::on_xslChanged(const QString& text) {
    changed = true;
    xsl = text;
}
void XsltInstruction::on_fileChanged(const QString& text) {
    changed = true;
    file = text;
}

void XsltInstruction::on_xslSelect() {
    QString workingDir = getWorkingDir();
    QString dir = xsl;
    if (dir.isEmpty()) {
        dir = workingDir;
    }
    QString fileName = QFileDialog::getOpenFileName(xslEdit, 
                                                    tr("Add File"),
                                                    dir,
                                                    "XSLT file (*.xsl *.xslt)");
    if (!fileName.isEmpty()) {
        QDir d(workingDir);
        xslEdit->setText(d.relativeFilePath(fileName));
    }
}
void XsltInstruction::on_fileSelect() {
    QString workingDir = getWorkingDir();
    QString dir = file;
    if (dir.isEmpty()) {
        dir = workingDir;
    }
    QString fileName = QFileDialog::getSaveFileName(fileEdit,
                                                    tr("Add File"),
                                                    dir,
                                                    "XML file (*.xml)",
                                                    NULL,
                                                    QFileDialog::DontConfirmOverwrite | QFileDialog::DontUseNativeDialog);
    if (!fileName.isEmpty()) {
        QDir d(workingDir);
        fileEdit->setText(d.relativeFilePath(fileName));
    }
}