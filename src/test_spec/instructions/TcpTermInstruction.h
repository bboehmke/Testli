/**
 * @file TcpTermInstruction.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TCPTERMINSTRUCTION_H
#define TESTLI_TCPTERMINSTRUCTION_H

#include "../Instruction.h"

#include <QPlainTextEdit>

/**
 * Instruction to send commands to a TCP raw socket
 */
class TcpTermInstruction : public Instruction {
    Q_OBJECT
    public:
        /**
         * Create empty instruction
         */
        TcpTermInstruction();
        /**
         * Create instruction from XML element
         * @param element XML element
         */
        TcpTermInstruction(const QDomElement& element);

        /**
         * Create GUI for instruction settings
         * @param parent Parent widget
         * @return Layout instance
         */
        QLayout* getSettingLayout(QWidget* parent);


        /**
         * Start execution of instruction
         */
        void run();

        /**
         * Dump instruction
         * @param doc XML destination document
         * @param report True if report save
         * @return XML element
         */
        QDomElement dump(QDomDocument& doc, bool report);

    public slots:
        /**
         * Stop execution
         */
        void stop();

    private slots:
        /**
         * Handle command changes
         */
        void on_commandChanged();

    private:
        /**
         * Edit widget for command
         */
        QPlainTextEdit* commandEdit;
        /**
         * List of commands to run
         * First line contains connection information
         */
        QString commands;

        /**
         * True if instruction is stopped
         */
        bool stopped;

        /**
         * Wait and handle events
         * @param duration Time to wait
         */
        void eventLoopWait(int duration);
};


#endif //TESTLI_TCPTERMINSTRUCTION_H
