/**
 * @file TcpTermInstruction.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TcpTermInstruction.h"

#include <QTcpSocket>
#include <QDateTime>
#include <QApplication>

TcpTermInstruction::TcpTermInstruction() :
        Instruction("TcpTerm"), commandEdit(NULL), stopped(false) {
    changed = true;
}
TcpTermInstruction::TcpTermInstruction(const QDomElement& element) :
        Instruction("TcpTerm"), commandEdit(NULL), stopped(false) {
    commands = element.text().trimmed();
}

QLayout* TcpTermInstruction::getSettingLayout(QWidget* parent) {
    QVBoxLayout* layout = new QVBoxLayout(parent);

    QFont f("unexistent");
    f.setStyleHint(QFont::Monospace);

    commandEdit = new QPlainTextEdit(parent);
    commandEdit->setPlainText(commands);
    commandEdit->setFont(f);
    layout->addWidget(commandEdit);

    connect(commandEdit, SIGNAL(textChanged()),
            this, SLOT(on_commandChanged()));

    return layout;
}

void TcpTermInstruction::run() {
    stopped = false;
    QStringList commandList = handleVariables(commands).split("\n");
    if (commandList.size() < 2) {
        emit(logConsoleError("Invalid instruction - Command missing"));
        emit(finished(-3));
        return;
    }
    // get connection data
    QStringList connectionList = commandList.takeFirst().split(" ");
    QString address = connectionList.at(0);
    quint16 port = 54321;
    uint waitTime = 3;
    if (connectionList.size() > 1) {
        port = connectionList.at(1).toUShort();
        if (connectionList.size() > 2) {
            waitTime = connectionList.at(2).toUInt();
        }
    }

    emit(logConsole(QString("TcpTerm -> %1:%2").arg(address).arg(port)));

    // create TCP connection
    QTcpSocket socket;
    socket.connectToHost(address, port);
    eventLoopWait(0);
    if (stopped) {
        socket.close();
        emit(logConsoleError("Instruction stopped"));
        emit(finished(1));
        return;
    }
    if (!socket.waitForConnected(1)) {
        emit(logConsoleError(QString("Failed to connect - %1").arg(socket.errorString())));
        emit(finished(-4));
        return;
    }

    for (QString command : commandList) {
        command = command.trimmed();

        if (stopped) {
            socket.close();
            emit(logConsoleError("Instruction stopped"));
            emit(finished(1));
            return;
        }
        if (!socket.isOpen()) {
            emit(logConsoleError(QString("Connection closed - %1").arg(socket.errorString())));
            emit(finished(-5));
            return;
        }

        socket.write(QString("%1\r\n").arg(command).toLatin1());
        eventLoopWait(waitTime);

        if (socket.bytesAvailable() > 0) {
            emit(logConsole(QString::fromLatin1(socket.readAll())));
        }
    }
    socket.close();
    emit(finished(0));
}

QDomElement TcpTermInstruction::dump(QDomDocument& doc, bool report) {
    if (!report) {
        changed = false;
    }
    QMap<QString, QString> attr;
    attr.insert("Function", getName());
    return createDump(doc, attr, commands);
}

void TcpTermInstruction::stop() {
    stopped = true;
}

void TcpTermInstruction::on_commandChanged() {
    changed = true;
    commands = commandEdit->toPlainText();
}

void TcpTermInstruction::eventLoopWait(int duration) {
    QDateTime start = QDateTime::currentDateTime();

    QApplication::processEvents();
    while (!stopped && start.secsTo(QDateTime::currentDateTime()) <= duration) {
        QApplication::processEvents();
    }
}