/**
 * @file XsltInstruction.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_XSLTINSTRUCTION_H
#define TESTLI_XSLTINSTRUCTION_H

#include "../Instruction.h"

#include <QLineEdit>

/**
 * Instruction to run XSLT
 */
class XsltInstruction  : public Instruction {
    Q_OBJECT
    public:
        /**
         * Create empty instruction
         */
        XsltInstruction();
        /**
         * Create instruction from XML element
         * @param element XML element
         */
        XsltInstruction(const QDomElement& element);

        /**
         * Create GUI for instruction settings
         * @param parent Parent widget
         * @return Layout instance
         */
        QLayout* getSettingLayout(QWidget* parent);


        /**
         * Start execution of instruction
         */
        void run();

        /**
         * Dump instruction
         * @param doc XML destination document
         * @param report True if report save
         * @return XML element
         */
        QDomElement dump(QDomDocument& doc, bool report);

    private slots:
        /**
         * Handle XSL script change
         * @param text Path to XSL script
         */
        void on_xslChanged(const QString& text);
        /**
         * Handle file to handle change
         * @param text Changed file to handle
         */
        void on_fileChanged(const QString& text);

        /**
         * Handle XSL select button
         */
        void on_xslSelect();
        /**
         * Handle file select button
         */
        void on_fileSelect();

    private:
        /**
         * XSL script
         */
        QString xsl;
        /**
         * File to handle
         */
        QString file;

        /**
         * XSL edit instance
         */
        QLineEdit* xslEdit;
        /**
         * File edit instance
         */
        QLineEdit* fileEdit;

};


#endif //TESTLI_XSLTINSTRUCTION_H
