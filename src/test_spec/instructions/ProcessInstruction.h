/**
 * @file ProcessInstruction.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_PROCESSINSTRUCTION_H
#define TESTLI_PROCESSINSTRUCTION_H

#include "../Instruction.h"

#include <QProcess>

/**
 * Instruction to run a process
 */
class ProcessInstruction : public Instruction {
    Q_OBJECT
    public:
        /**
         * Create empty instruction
         */
        ProcessInstruction();
        /**
         * Create instruction from XML element
         * @param element XML element
         */
        ProcessInstruction(const QDomElement& element);

        /**
         * Create GUI for instruction settings
         * @param parent Parent widget
         * @return Layout instance
         */
        QLayout* getSettingLayout(QWidget* parent);


        /**
         * Start execution of instruction
         */
        void run();

        /**
         * Dump instruction
         * @param doc XML destination document
         * @param report True if report save
         * @return XML element
         */
        QDomElement dump(QDomDocument& doc, bool report);

    public slots:
        /**
         * Stop execution
         */
        void stop();

    private slots:
        /**
         * Handle command changes
         * @param text New command
         */
        void on_commandChanged(const QString& text);

        /**
         * Handle process output
         */
        void on_readyRead();
        /**
         * Handle process finished
         * @param exitCode
         */
        void on_finished(int exitCode);

    private:
        /**
         * Command to run
         */
        QString command;

        /**
         * Process instance
         */
        QProcess* process;
};


#endif //TESTLI_PROCESSINSTRUCTION_H
