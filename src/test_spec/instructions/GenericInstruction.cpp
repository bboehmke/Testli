/**
 * @file GenericInstruction.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GenericInstruction.h"

#include <QFormLayout>
#include <QLineEdit>

GenericInstruction::GenericInstruction(const QString& name) : Instruction("Generic") {
    attributes.insert("Function", name);
}
GenericInstruction::GenericInstruction(const QDomElement& element) :
        Instruction("Generic") {

    QDomNamedNodeMap nodeMap = element.attributes();
    for (int i = 0; i < nodeMap.length(); ++i) {
        QDomAttr attr = nodeMap.item(i).toAttr();
        if (!attr.isNull()) {
            attributes.insert(attr.name(), attr.value());
        }
    }
    data = element.text();
}

QLayout* GenericInstruction::getSettingLayout(QWidget* parent) {
    QFormLayout* layout = new QFormLayout(parent);

    QMapIterator<QString, QString> it(attributes);
    while (it.hasNext()) {
        it.next();
        QLineEdit* edit = new QLineEdit(parent);
        layout->addRow(it.key(), edit);
        edit->setText(it.value());
        edit->setReadOnly(true);
    }

    return layout;
}
bool GenericInstruction::canRun() const {
    return false;
}
void GenericInstruction::run() {
    emit(finished(-1));
}
QDomElement GenericInstruction::dump(QDomDocument& doc, bool /*report*/) {
    return createDump(doc, attributes, data);
}