/**
 * @file RemoveFilesInstruction.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "RemoveFilesInstruction.h"

#include <QFileDialog>

#include "../../Settings.h"


RemoveFilesInstruction::RemoveFilesInstruction() :
        Instruction("RemoveFiles"), listView(NULL), removeButton(NULL) {
    changed = true;
    model = new QStringListModel();
}
RemoveFilesInstruction::RemoveFilesInstruction(const QDomElement& element) :
        Instruction("RemoveFiles"), listView(NULL), removeButton(NULL) {
    model = new QStringListModel(element.text().trimmed().split(";"));
}
RemoveFilesInstruction::~RemoveFilesInstruction() {
    delete(model);
}


QLayout* RemoveFilesInstruction::getSettingLayout(QWidget* parent) {
    QVBoxLayout* layout = new QVBoxLayout(parent);

    QHBoxLayout* toolBar = new QHBoxLayout();

    QToolButton* addButton = new QToolButton(parent);
    addButton->setIcon(QIcon(":/icons/list-add.svg"));
    toolBar->addWidget(addButton);

    removeButton = new QToolButton(parent);
    removeButton->setIcon(QIcon(":/icons/list-remove.svg"));
    toolBar->addWidget(removeButton);
    toolBar->addStretch();
    layout->addLayout(toolBar);

    listView = new QListView(parent);
    listView->setModel(model);
    layout->addWidget(listView);

    connect(addButton, SIGNAL(pressed()), this, SLOT(on_addFile()));
    connect(removeButton, SIGNAL(pressed()), this, SLOT(on_removeFile()));
    connect(listView->selectionModel(),
            SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
            this,
            SLOT(currentChanged(const QModelIndex&)));
    connect(listView, SIGNAL(destroyed()), this, SLOT(on_destroyed()));

    return layout;
}

void RemoveFilesInstruction::run() {
    QString workingDir = getWorkingDir() + "/";
    bool failed = false;
    QDir workDir(workingDir);
    for (QString file : model->stringList()) {
        if (QFile::exists(workDir.filePath(file))) {
            QFile f(workDir.filePath(file));
            f.setPermissions(f.permissions() | QFile::WriteUser |
                                     QFile::WriteOwner | QFile::WriteOther);
            f.remove();
            emit(logConsole(QString("Removed file \"%1\"\n").arg(file)));
        } else {
            emit(logConsoleError(QString("Failed to remove file \"%1\"\n").arg(file)));
            failed = true;
        }
    }
    if (failed) {
        emit(finished(1));
    } else {
        emit(finished(0));
    }
}

QDomElement RemoveFilesInstruction::dump(QDomDocument& doc, bool report) {
    if (!report) {
        changed = false;
    }
    QMap<QString, QString> attr;
    attr.insert("Function", getName());
    return createDump(doc, attr, model->stringList().join(";"));
}


void RemoveFilesInstruction::on_addFile() {
    QString fileName = QFileDialog::getSaveFileName(
            listView, tr("Add File"), QString(), QString(),
            NULL, QFileDialog::DontConfirmOverwrite | QFileDialog::DontUseNativeDialog);
    if (!fileName.isEmpty()) {
        QModelIndex index = listView->selectionModel()->currentIndex();
        int pos;
        if (index.isValid()) {
            pos = index.row()+1;
        } else {
            pos = model->stringList().size();
        }
        model->insertRow(pos);

        QDir dir(Settings::getSpecBasePath() + "/" +
                 Settings::getValue("Execution/WorkingDir").toString());
        model->setData(model->index(pos, 0), dir.relativeFilePath(fileName));
        changed = true;
    }
}
void RemoveFilesInstruction::on_removeFile() {
    QModelIndex index = listView->selectionModel()->currentIndex();
    model->removeRow(index.row());
    changed = true;
}
void RemoveFilesInstruction::on_destroyed() {
    listView = NULL;
    removeButton = NULL;
}
void RemoveFilesInstruction::currentChanged(const QModelIndex& current) {
    if (removeButton) {
        removeButton->setEnabled(current.isValid());
    }
}
