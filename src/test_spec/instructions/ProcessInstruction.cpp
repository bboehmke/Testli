/**
 * @file ProcessInstruction.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ProcessInstruction.h"

#include <QFormLayout>
#include <QLineEdit>

ProcessInstruction::ProcessInstruction() :
        Instruction("Process"), process(NULL) {
    changed = true;
}
ProcessInstruction::ProcessInstruction(const QDomElement& element) :
        Instruction("Process"), process(NULL) {
    command = element.text().trimmed();
}

QLayout* ProcessInstruction::getSettingLayout(QWidget* parent) {
    QFormLayout* layout = new QFormLayout(parent);

    QLineEdit* commandEdit = new QLineEdit(parent);
    layout->addRow(tr("Command"), commandEdit);
    commandEdit->setText(command);

    connect(commandEdit, SIGNAL(textChanged(const QString&)),
            this, SLOT(on_commandChanged(const QString&)));

    return layout;
}

void ProcessInstruction::run() {
    // create process
    process = new QProcess(this);
    connect(process, SIGNAL(readyRead()),
            this, SLOT(on_readyRead()));
    connect(process, SIGNAL(finished(int, QProcess::ExitStatus)),
            this, SLOT(on_finished(int)));

    process->setWorkingDirectory(getWorkingDir());

    QStringList commandSplit = handleVariables(command).split(" ");
    commandSplit.insert(0, getToolPath(commandSplit.takeAt(0)));
    QString cmd = commandSplit.join(" ");
    process->start(cmd);

    emit(logConsole(QString("# %1\n").arg(cmd)));

    if (!process->waitForStarted(10000)) {
        emit(logConsoleError(QString("Failed to execute: %1\n").arg(cmd)));
        emit(finished(-3));
    }
}

QDomElement ProcessInstruction::dump(QDomDocument& doc, bool report) {
    if (!report) {
        changed = false;
    }
    QMap<QString, QString> attr;
    attr.insert("Function", getName());
    return createDump(doc, attr, command);
}

void ProcessInstruction::stop() {
    if (process) {
        process->kill();
    } else {
        emit(finished(-2));
    }
}

void ProcessInstruction::on_commandChanged(const QString& text) {
    changed = true;
    command = text;
}

void ProcessInstruction::on_readyRead() {
    if (process) {
        QByteArray output = process->readAllStandardOutput();
        if (!output.isEmpty()) {
            emit(logConsole(QString::fromLatin1(output)));
        }
        QByteArray error = process->readAllStandardError();
        if (!error.isEmpty()) {
            emit(logConsoleError(QString::fromLatin1(error)));
        }
    }
}
void ProcessInstruction::on_finished(int exitCode) {
    on_readyRead();
    delete(process);
    process = NULL;
    emit(finished(exitCode));
}