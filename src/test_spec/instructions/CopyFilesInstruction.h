/**
 * @file CopyFilesInstruction.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_COPYFILESINSTRUCTION_H
#define TESTLI_COPYFILESINSTRUCTION_H

#include "../Instruction.h"

#include <QStringListModel>
#include <QListView>
#include <QToolButton>

/**
 * Instruction to copy files
 */
class CopyFilesInstruction : public Instruction {
    Q_OBJECT
    public:
        /**
         * Create empty instruction
         */
        CopyFilesInstruction();

        /**
         * Create instruction from XML element
         * @param element XML element
         */
        CopyFilesInstruction(const QDomElement& element);
        ~CopyFilesInstruction();

        /**
         * Create GUI for instruction settings
         * @param parent Parent widget
         * @return Layout instance
         */
        QLayout* getSettingLayout(QWidget* parent);

        /**
         * Start execution of instruction
         */
        void run();

        /**
         * Dump instruction
         * @param doc XML destination document
         * @param report True if report save
         * @return XML element
         */
        QDomElement dump(QDomDocument& doc, bool report);

    private slots:
        /**
         * Handle add button
         */
        void on_addFile();
        /**
         * Handle remove button
         */
        void on_removeFile();

        /**
         * Handle widget destroy
         */
        void on_destroyed();

        /**
         * Handle file selection change
         * @param current Current selected instruction
         */
        void currentChanged(const QModelIndex& current);

    private:
        /**
         * Model for file list
         */
        QStringListModel* model;

        /**
         * List view of files
         */
        QListView* listView;

        /**
         * Remove file button
         */
        QToolButton* removeButton;
};


#endif //TESTLI_COPYFILESINSTRUCTION_H
