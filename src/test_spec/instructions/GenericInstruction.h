/**
 * @file GenericInstruction.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_GENERICINSTRUCTION_H
#define TESTLI_GENERICINSTRUCTION_H

#include "../Instruction.h"

/**
 * Instruction for unknown instruction elements
 */
class GenericInstruction : public Instruction {
    Q_OBJECT
    public:
        /**
         * Create empty instruction
         * @param name Name of function
         */
        GenericInstruction(const QString& name);
        /**
         * Create instruction from XML element
         * @param element XML element
         */
        GenericInstruction(const QDomElement& element);

        /**
         * Create GUI for instruction settings
         * @param parent Parent widget
         * @return Layout instance
         */
        QLayout* getSettingLayout(QWidget* parent);

        /**
         * Check if action can run
         * @return True if run is supported
         */
        bool canRun() const;

        /**
         * Start execution of instruction
         */
        void run();

        /**
         * Dump instruction
         * @param doc XML destination document
         * @param report True if report save
         * @return XML element
         */
        QDomElement dump(QDomDocument& doc, bool report);

    private:
        /**
         * Attributes of instruction
         */
        QMap<QString, QString> attributes;
        /**
         * Content of instruction
         */
        QString data;
};


#endif //TESTLI_GENERICINSTRUCTION_H
