/**
 * @file Step.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Step.h"

#include "../misc/XmlHelper.h"
#include "InstructionLoader.h"

#include "Testcase.h"
#include "../Settings.h"

Step::Step(Testcase* parent, const QDomElement& element, Type type) :
        parent(parent), type(type), specChanged(false), reportChanged(false) {
    title = element.attribute("Title");
    annotationId = element.attribute("AnnotationId");

    description = XmlHelper::getXmlContent(element.firstChildElement("Description"));
    expectedResult = XmlHelper::getXmlContent(element.firstChildElement("ExpectedResult"));

    QDomElement instructions = element.firstChildElement("Instructions");
    for (QDomNode n = instructions.firstChild();
         !n.isNull();
         n = n.nextSibling()) {

        QDomElement e = n.toElement();
        if (!e.isNull() && e.tagName() == "Instruction") {
            instructionList.append(InstructionLoader::getInstance()->create(e));
        }
    }
}
Step::Step(Testcase* parent, Type type) :
        parent(parent), type(type), specChanged(true), reportChanged(false) {

}
Step::~Step() {
    for (Instruction* instruction : instructionList) {
        delete(instruction);
    }
}

void Step::loadReport(const QDomElement& element) {
    logger.clear();
    QDomElement resultElement = element.firstChildElement("Result");
    if (resultElement.hasAttribute("State")) {
        resultDate =
                QDate::fromString(resultElement.attribute("Date"), "yyyy-MM-dd");
        resultTime =
                QTime::fromString(resultElement.attribute("Time"), "hh:mm:ss");
        resultState = resultElement.attribute("State");
        resultNote = XmlHelper::getXmlContent(resultElement);
    }

    QDomElement loggerElement = element.firstChildElement("Logger");
    for (QDomElement e = loggerElement.firstChildElement("log");
         !e.isNull();
         e = e.nextSiblingElement("log")) {
        logger.insert(e.attribute("Name"), e.text());
    }
}
void Step::removeReport() {
    logger.clear();
    resultDate = QDate();
    resultTime = QTime();
    resultState = QString();
    resultNote = QString();
    reportChanged = true;
}

Testcase* Step::getParent() const {
    return parent;
}

Step::Type Step::getType() const {
    return type;
}

QString Step::getTitle() const {
    return title;
}
void Step::setTitle(const QString& title) {
    specChanged = true;
    this->title = title;
    parent->resetTestState();
}

QString Step::getDescription() const {
    return description;
}
void Step::setDescription(const QString& text) {
    specChanged = true;
    description = text;
    parent->resetTestState();
}

QString Step::getExpectedResult() const {
    return expectedResult;
}
void Step::setExpectedResult(const QString& text) {
    specChanged = true;
    expectedResult = text;
    parent->resetTestState();
}

QString Step::getResultState() const {
    return resultState;
}
void Step::setResultState(const QString& state) {
    reportChanged = true;
    resultState = state;
    resultDate = QDate::currentDate();
    resultTime = QTime::currentTime();
    parent->updateExecutionState();
}
QString Step::getResultNote() const {
    return resultNote;
}
void Step::setResultNote(const QString& note) {
    reportChanged = true;
    resultNote = note;
    resultDate = QDate::currentDate();
    resultTime = QTime::currentTime();
    parent->updateExecutionState();
}

QMap<QString, QString> Step::getLogs() const {
    return logger;
}
void Step::addLog(const QString& name, const QString& data) {
    reportChanged = true;
    logger.insert(name, data);
}
void Step::removeLog(const QString& name) {
    if (logger.contains(name)) {
        reportChanged = true;
        logger.remove(name);
    }
}

Instruction* Step::getInstruction(int index) const {
    if (index < 0 || index >= instructionList.size()) {
        return NULL;
    }
    return instructionList.at(index);
}
QList<Instruction*> Step::getInstructionList() const {
    return instructionList;
}
Instruction* Step::addInstruction(int pos, const QString& name) {
    InstructionLoader* loader = InstructionLoader::getInstance();
    if (!loader->getInstructionTypes().contains(name)) {
        return NULL;
    }

    specChanged = true;
    Instruction* instruction = loader->create(name);
    instructionList.insert(pos, instruction);
    return instruction;
}
bool Step::removeInstruction(Instruction* instruction) {
    if (!instructionList.removeOne(instruction)) {
        return false;
    }
    specChanged = true;
    delete(instruction);
    return true;
}
bool Step::moveInstruction(Instruction* instruction, int pos) {
    if (!instructionList.removeOne(instruction)) {
        return false;
    }
    specChanged = true;
    instructionList.insert(pos, instruction);
    return true;
}

bool Step::isSpecChanged() const {
    if (specChanged) {
        return true;
    }
    for (Instruction* instruction : instructionList) {
        if (instruction->isChanged()) {
            return true;
        }
    }
    return false;
}
bool Step::isReportChanged() const {
    return reportChanged || isSpecChanged();
}

QDomElement Step::save(QDomDocument& doc, bool report) {
    QDomElement element;
    switch (type) {
        case Pre:
            element = doc.createElement("Precondition");
            break;
        case Post:
            element = doc.createElement("Postcondition");
            break;
        default:
            element = doc.createElement("Step");
    }
    if (!title.isEmpty()) {
        element.setAttribute("Title", title);
    }
    if (!annotationId.isEmpty()) {
        element.setAttribute("AnnotationId", annotationId);
    }
    QDomElement descriptionElement = doc.createElement("Description");
    XmlHelper::addXmlContent(&descriptionElement, description);
    element.appendChild(descriptionElement);

    QDomElement expectedResultElement = doc.createElement("ExpectedResult");
    XmlHelper::addXmlContent(&expectedResultElement, expectedResult);
    element.appendChild(expectedResultElement);

    if (!instructionList.isEmpty()) {
        QDomElement instructionsElement = doc.createElement("Instructions");
        for (Instruction* instruction : instructionList) {
            instructionsElement.appendChild(instruction->dump(doc, report));
        }
        element.appendChild(instructionsElement);
    }

    QDomElement resultElement = doc.createElement("Result");
    if (report || Settings::getValue("Test/EnableSpecResultElement").toBool()) {
        element.appendChild(resultElement);
    }

    if (report) {
        if (resultDate.isValid()) {
            resultElement.setAttribute("Date", resultDate.toString("yyyy-MM-dd"));
        }
        if (resultTime.isValid()) {
            resultElement.setAttribute("Time", resultTime.toString("hh:mm:ss"));
        }
        if (!resultState.isEmpty()) {
            resultElement.setAttribute("State", resultState);
        } else {
            resultElement.setAttribute("State", "notexecuted");
        }
        XmlHelper::addXmlContent(&resultElement, resultNote);

        if (!logger.isEmpty()) {
            QDomElement loggerElement = doc.createElement("Logger");

            QMapIterator<QString, QString> it(logger);
            while (it.hasNext()) {
                it.next();

                QDomElement e = doc.createElement("log");
                e.setAttribute("Name", it.key());
                if (it.key().toLower().endsWith(".xml")) {
                    e.setAttribute("Language", "xml");
                }
                e.appendChild(doc.createTextNode(it.value()));
                loggerElement.appendChild(e);
            }

            element.appendChild(loggerElement);
        }

        reportChanged = false;
    } else {
        specChanged = false;
    }
    return element;
}
