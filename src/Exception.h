/**
 * @file Exception.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_EXCEPTION_H
#define TESTLI_EXCEPTION_H

#include <QString>

/**
 * Test spec exception
 */
class Exception {
    public:
        /**
         * Possible exception types
         */
        enum Type {
            FileOpenFailed,  //!< Failed too open file
            XmlError,        //!< Invalid XML
        };

        /**
         * Create exception
         * @param type Type of exception
         * @param msg Message string
         */
        Exception(Type type, QString msg);

        /**
         * Get type of exception
         * @return Exception type
         */
        Type getType() const;
        /**
         * Get type of exception as string
         * @return Exception type string
         */
        QString getTypeStr() const;
        /**
         * Get message string
         * @return Message string
         */
        QString getMessage() const;

    private:
        /**
         * Type of exception
         */
        Type type;
        /**
         * Message string
         */
        QString msg;
};


#endif //TESTLI_EXCEPTION_H
