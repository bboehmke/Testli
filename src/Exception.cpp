/**
 * @file Exception.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Exception.h"

Exception::Exception(Type type, QString msg) : type(type), msg(msg) {

}

Exception::Type Exception::getType() const {
    return type;
}
QString Exception::getTypeStr() const {
    switch (type) {
        case FileOpenFailed:
            return "Failed to open file";
        case XmlError:
            return "Invalid XML";
        default:
            return "Unknown";
    }
}
QString Exception::getMessage() const {
    return msg;
}