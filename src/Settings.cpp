/**
 * @file Settings.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Settings.h"

#include <QStandardPaths>
#include <QFileInfo>
#include <QDir>

QSettings* Settings::applicationSettings = NULL;
QMap<QString, QVariant> Settings::defaultValues = QMap<QString, QVariant>();
QMap<QString, QVariant> Settings::projectSettings = QMap<QString, QVariant>();
QString Settings::specBasePath = "";

void Settings::init() {
    defaultValues.clear();
    // application
    setDefaultValue("Gen/Autosave", false);
    setDefaultValue("Gen/OverwriteReadOnly", false);
    setDefaultValue("Execution/WorkingDir", "work/");
    setDefaultValue("Execution/ToolDir", "");

    // project
    setDefaultValue("Test/EnableFeatureSeparation", false);
    setDefaultValue("Test/EnableTestSeparation", true);
    setDefaultValue("Test/Subdirectory", "testcases/");
    setDefaultValue("Test/DefaultState", "prepared");
    setDefaultValue("Test/EnableChangeToDefaultState", true);
    setDefaultValue("Test/EnableSpecResultElement", false);
    setDefaultValue("Test/DefaultText", "<p>\n</p>");
    setDefaultValue("Test/EnableDefaultPrecond", false);
    setDefaultValue("Test/EnableDefaultStep", true);
    setDefaultValue("Test/EnableDefaultPostcond", false);

    QString location = QStandardPaths::writableLocation(
            QStandardPaths::AppLocalDataLocation);
    applicationSettings = new QSettings(location + "/settings.ini",
                                        QSettings::IniFormat);
}
void Settings::loadProject(const QDomElement& element,
                           const QString& specPath) {
    projectSettings.clear();
    if (element.tagName() == "ProjectSettings") {
        for (QDomNode n = element.firstChild();
             !n.isNull();
             n = n.nextSibling()) {

            QDomElement baseElement = n.toElement();

            if (!baseElement.isNull()) {
                for (QDomNode c = baseElement.firstChild();
                     !c.isNull();
                     c= c.nextSibling()) {

                    QDomElement childElement = c.toElement();
                    if (!childElement.isNull()) {
                        projectSettings.insert(
                                QString("%1/%2").arg(baseElement.tagName()).arg(childElement.tagName()),
                                childElement.text());
                    }
                }
            }
        }
    }
    QFileInfo info(specPath);
    specBasePath = info.dir().absolutePath();
}
void Settings::saveProject(QDomElement* element) {
    if (!element) {
        return;
    }
    QDomDocument doc = element->ownerDocument();

    QMap<QString, QDomElement> baseElements;
    QMapIterator<QString, QVariant> it(projectSettings);
    while (it.hasNext()) {
        it.next();

        // split key
        int pos = it.key().indexOf("/");
        if (pos < 0) {
            continue;
        }
        QString base = it.key().left(pos);
        QString child = it.key().mid(pos+1);

        // create base element if not exist
        if (!baseElements.contains(base)) {
            QDomElement e = doc.createElement(base);
            element->appendChild(e);
            baseElements.insert(base, e);
        }

        // add settings element
        QDomElement e = doc.createElement(child);
        e.appendChild(doc.createTextNode(it.value().toString()));
        baseElements[base].appendChild(e);
    }
}
void Settings::clear() {
    if (applicationSettings) {
        delete(applicationSettings);
    }
}

QVariant Settings::getValue(const QString& key) {
    if (projectSettings.contains(key)) {
        return projectSettings[key];
    } else {
        return getApplicationValue(key);
    }
}
QVariant Settings::getProjectValue(const QString& key) {
    if (projectSettings.contains(key)) {
        return projectSettings[key];
    } else {
        return getDefaultValue(key);
    }
}
void Settings::setProjectValue(const QString& key, const QVariant& value) {
    if (value == getDefaultValue(key)) {
        if (projectSettings.contains(key)) {
            projectSettings.remove(key);
        }
    } else {
        projectSettings.insert(key, value);
    }
}
QVariant Settings::getApplicationValue(const QString& key) {
    if (applicationSettings) {
        return applicationSettings->value(key, getDefaultValue(key));
    }
    return getDefaultValue(key);
}
void Settings::setApplicationValue(const QString& key, const QVariant& value) {
    if (applicationSettings) {
        if (value == getDefaultValue(key)) {
            if (applicationSettings->contains(key)) {
                applicationSettings->remove(key);
            }
        } else {
            applicationSettings->setValue(key, value);
        }
    }
}
QString Settings::getSpecBasePath() {
    return specBasePath;
}

Settings::Settings() {

}

void Settings::setDefaultValue(const QString& key, const QVariant& value) {
    defaultValues.insert(key, value);
}
QVariant Settings::getDefaultValue(const QString& key) {
    if (defaultValues.contains(key)) {
        return defaultValues[key];
    } else {
        return QVariant();
    }
}