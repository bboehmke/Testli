/**
 * @file TestSpecModel.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_TESTSPECMODEL_H
#define TESTLI_TESTSPECMODEL_H

#include <QAbstractItemModel>

#include "test_spec/TestSpec.h"

/**
 * Model to interface test spec
 */
class TestSpecModel : public QAbstractItemModel {
    Q_OBJECT
    public:
        /**
         * Create model
         * @param testSpec Test spec instance
         */
        TestSpecModel(TestSpec* testSpec);

        /**
         * Insert new feature group in current position
         * @param current Current selected item
         * @param name Name of feature group
         * @return Index of created item
         */
        QModelIndex insertFeatureGroup(const QModelIndex &current,
                                       const QString &name);
        /**
         * Insert new feature in current position
         * @param current Current selected item
         * @param name Name of feature
         * @param id Id of feature
         * @return Index of created item
         */
        QModelIndex insertFeature(const QModelIndex& current,
                                  const QString& name,
                                  const QString& id);
        /**
         * Insert new testcase in current position
         * @param current Current selected item
         * @param id Id of testcase
         * @return Index of created item
         */
        QModelIndex insertTestcase(const QModelIndex& current,
                                   const QString& id);
        /**
         * Insert new linked testcase in current position
         * @param current Current selected item
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         * @return Index of created item
         */
        QModelIndex insertLinkedTestcase(const QModelIndex& current,
                                         const QString& featureId,
                                         const QString& testcaseId);

        /**
         * Check if feature can be inserted
         * @param current Current selected item
         * @return True if feature can be inserted
         */
        bool canInsertFeature(const QModelIndex& current) const;
        /**
         * Check if testcase can be inserted
         * @param current Current selected item
         * @return True if testcase can be inserted
         */
        bool canInsertTestcase(const QModelIndex& current) const;

        /**
         * Remove selected item
         * @param current Current selected item
         * @return True if item was removed
         */
        bool remove(const QModelIndex& current);
        /**
         * Check if item can be removed
         * @param current Current selected item
         * @return True if item can be removed
         */
        bool canRemove(const QModelIndex& current) const;

        /**
         * Move item up
         * @param current Current selected item
         * @return True if item was moved
         */
        bool moveUp(const QModelIndex& current);
        /**
         * Move item down
         * @param current Current selected item
         * @return True if item was moved
         */
        bool moveDown(const QModelIndex& current);
        /**
         * Check if item can be moved up
         * @param current Current selected item
         * @return True if item can be moved
         */
        bool canMoveUp(const QModelIndex& current) const;
        /**
         * Check if item can be moved down
         * @param current Current selected item
         * @return True if item can be moved
         */
        bool canMoveDown(const QModelIndex& current) const;

        /**
         * Get actual selected feature group
         * @param current Current selected item
         * @return Feature group instance or NULL if no feature group selected
         */
        FeatureGroup* getFeatureGroup(const QModelIndex& current) const;
        /**
         * Get actual selected feature
         * @param current Current selected item
         * @return Feature instance or NULL if no feature selected
         */
        Feature* getFeature(const QModelIndex& current) const;
        /**
         * Get actual selected testcase
         * @param current Current selected item
         * @return Testcase instance or NULL if no testcase selected
         */
        Testcase* getTestcase(const QModelIndex& current) const;
        /**
         * Get actual selected linked testcase
         * @param current Current selected item
         * @return Linked testcase instance or NULL if no linked testcase selected
         */
        LinkedTestcase* getLinkedTestcase(const QModelIndex& current) const;

        /**
         * Search for testcase in model
         * @param testcase Testcase instance
         * @return Position of testcase or invalid
         */
        QModelIndex findTestcase(Testcase* testcase) const;

        /**
         * Get amount of columns off this model
         * @param parent Parent item
         * @return Amount of columns
         */
        int	columnCount(const QModelIndex& parent = QModelIndex()) const;
        /**
         * Get data of item
         * @param index Index of item
         * @param role Requested data type
         * @return Data of item
         */
        QVariant data(const QModelIndex& index,
                      int role = Qt::DisplayRole) const;
        /**
         * Get header data of section
         * @param section Column index
         * @param orientation Orientation type
         * @param role Requested data type
         * @return Data of header
         */
        QVariant headerData(int section, Qt::Orientation orientation,
                            int role = Qt::DisplayRole) const;
        /**
         * Get index object of item
         * @param row Row of item
         * @param column Column of item
         * @param parent Parent index object
         * @return Index object
         */
        QModelIndex index(int row, int column,
                          const QModelIndex& parent = QModelIndex()) const;
        /**
         * Get parent of item
         * @param index Index object
         * @return Parent index object
         */
        QModelIndex parent(const QModelIndex& index) const;
        /**
         * Get amount of child rows of item
         * @param parent Parent index
         * @return Amount of child items
         */
        int rowCount(const QModelIndex& parent = QModelIndex()) const;

    private:
        /**
         * Instance of test spec
         */
        TestSpec* testSpec;

};


#endif //TESTLI_TESTSPECMODEL_H
