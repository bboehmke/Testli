/**
 * @file TestStepsModel.cpp
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TestStepsModel.h"

#include <QColor>
#include <QBrush>
#include <QFont>

#include "test_spec/TestSpec.h"

TestStepsModel::TestStepsModel(Testcase* testcase) : testcase(testcase) {

}

QModelIndex TestStepsModel::insertStep(const QModelIndex &current, Step::Type type) {
    int pos = testcase->getStepList().size();
    if (current.isValid()) {
        pos = current.row() + 1;
    }

    beginInsertRows(QModelIndex(), pos, pos);
    Step* step = testcase->addStep(pos, type);
    endInsertRows();
    return createIndex(pos, 0, step);
}

bool TestStepsModel::remove(const QModelIndex& current) {
    if (!canRemove(current)) {
        return false;
    }

    beginRemoveRows(QModelIndex(), current.row(), current.row());
    testcase->removeStep(testcase->getStep(current.row()));
    endRemoveRows();
    return true;
}
bool TestStepsModel::canRemove(const QModelIndex& current) const {
    return current.isValid();
}

bool TestStepsModel::moveUp(const QModelIndex& current) {
    if (!canMoveUp(current)) {
        return false;
    }

    beginMoveRows(QModelIndex(), current.row(), current.row(),
                  QModelIndex(), current.row()-1);
    testcase->moveStep(testcase->getStep(current.row()), current.row()-1);
    endMoveRows();
    return true;
}
bool TestStepsModel::moveDown(const QModelIndex& current) {
    if (!canMoveDown(current)) {
        return false;
    }

    beginMoveRows(QModelIndex(), current.row(), current.row(),
                  QModelIndex(), current.row()+2);
    testcase->moveStep(testcase->getStep(current.row()), current.row()+1);
    endMoveRows();
    return true;
}
bool TestStepsModel::canMoveUp(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }
    Step* step = testcase->getStep(current.row());
    
    if (current.row() <= 0) {
        return false;
    }
    Step* preStep = testcase->getStep(current.row() - 1);

    return step->getType() == preStep->getType();
}
bool TestStepsModel::canMoveDown(const QModelIndex& current) const {
    if (!current.isValid()) {
        return false;
    }

    Step* step = testcase->getStep(current.row());

    if (current.row() >= testcase->getStepList().size()-1) {
        return false;
    }
    Step* preStep = testcase->getStep(current.row() + 1);

    return step->getType() == preStep->getType();
}

Step* TestStepsModel::getStep(const QModelIndex& current) const {
    if (!current.isValid()) {
        return NULL;
    }

    return testcase->getStep(current.row());
}


QVariant TestStepsModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    Step* step = testcase->getStep(index.row());
    if (!step) {
        return QVariant();
    }
    TestSpec* testSpec = step->getParent()->getParent()->getParent()->getParent();
    
    if (role == Qt::DisplayRole) {
        // get step string
        QString type;
        switch (step->getType()) {
            case Step::Pre:
                type = tr("Precondition");
                break;
            case Step::Main:
                type = tr("Step");
                break;
            case Step::Post:
                type = tr("Postcondition");
                break;
        }

        type += QString(" %1").arg(testcase->getStepIndex(step)+1);

        // get step name
        if (step->getTitle().isEmpty()) {
            return type;
        } else {
            return step->getTitle() + " (" + type + ")";
        }
    } else if (role == Qt::BackgroundRole) {
        if (step->getResultState() == "failed") {
            return QBrush(QColor(Qt::red));
        } else if (step->getResultState() == "passed") {
            return QBrush(QColor(Qt::green));
        }

    } else if (role == Qt::FontRole) {
        QFont font;
        font.setItalic(step->isSpecChanged() ||
                       (step->isReportChanged() && testSpec->hasReport()));
        return font;

    } else if (role == Qt::ToolTipRole) {
        if (step->isSpecChanged() && step->isReportChanged() && testSpec->hasReport()) {
            return "Spec & Report changed";
        } else if (step->isSpecChanged()) {
            return "Spec changed";
        } else if (step->isReportChanged() && testSpec->hasReport()) {
            return "Report changed";
        } else {
            return "";
        }
    }
    return QVariant();
}
int TestStepsModel::rowCount(const QModelIndex& /*parent*/) const {
    return testcase->getStepList().size();
}