/**
 * @file MainWindow.h
 *
 * Copyright (c) 2017 Benjamin Böhmke <benjamin@boehmke.net>
 *
 * This file is part of Testli.
 *
 * Testli is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Testli is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Testli. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTLI_MAINWINDOW_H
#define TESTLI_MAINWINDOW_H

#include <QObject>
#include <QMainWindow>

#include "ui_MainWindow.h"

#include "dialogs/CreateProjectDialog.h"
#include "dialogs/CreateFeatureGroupDialog.h"
#include "dialogs/CreateFeatureDialog.h"
#include "dialogs/CreateTestcaseDialog.h"
#include "dialogs/TextEditDialog.h"
#include "dialogs/EditFeatureGroupDialog.h"
#include "dialogs/EditFeatureDialog.h"
#include "dialogs/EditTestcaseDialog.h"
#include "dialogs/EditStepDialog.h"
#include "dialogs/ProjectSettingsDialog.h"
#include "dialogs/ApplicationSettingsDialog.h"
#include "dialogs/TestExecutionDialog.h"
#include "dialogs/StepLoggerDialog.h"
#include "dialogs/CreateRequirementDialog.h"
#include "dialogs/CreateLinkedTestcaseDialog.h"
#include "dialogs/EditLinkedTestcaseDialog.h"
#include "dialogs/AboutDialog.h"

#include "test_spec/TestSpec.h"

#include "TestSpecModel.h"
#include "TestStepsModel.h"

/**
 * Main window of application
 */
class MainWindow : public QMainWindow {
    Q_OBJECT
    public:
        /**
         * Create main window
         * @param projectFile Path to project file
         */
        MainWindow(const QString& projectFile);
        ~MainWindow();

    private slots:
        // main menu
        /**
         * Handle new project action
         */
        void on_actionNew_triggered();
        /**
         * Handle open project action
         */
        void on_actionOpen_triggered();
        /**
         * Handle save project action
         */
        void on_actionSave_triggered();
        /**
         * Handle settings action
         */
        void on_actionSettings_triggered();

        /**
         * Handle report select action
         */
        void on_actionSelectReport_triggered();
        /**
         * Handle report remove action
         */
        void on_actionRemoveReport_triggered();
        /**
         * Handle clear report action
         */
        void on_actionClearReport_triggered();
        /**
         * Handle open requirements action
         */
        void on_actionOpenRequirements_triggered();
        /**
         * Handle enable spec mode action
         */
        void on_actionModeSpec_triggered();
        /**
         * Handle enable report mode action
         */
        void on_actionModeReport_triggered();
        /**
         * Handle project settings action
         */
        void on_actionProjectSettings_triggered();

        /**
         * Handle about action
         */
        void on_actionAbout_triggered();
        /**
         * Handle about qt action
         */
        void on_actionAboutQt_triggered();

        // test spec
        /**
         * Handle add feature group
         */
        void on_specAddFeatures();
        /**
         * Handle add feature
         */
        void on_specAddFeature();
        /**
         * Handle add testcase
         */
        void on_specAddTestcase();
        /**
         * Handle add linked testcase
         */
        void on_specAddLinkedTestcase();
        /**
         * Handle remove spec element
         */
        void on_specRemoveButton_clicked();
        /**
         * Handle move spec element up
         */
        void on_specUpButton_clicked();
        /**
         * Handle move spec element down
         */
        void on_specDownButton_clicked();
        /**
         * Handle edit spec element
         */
        void on_specEditButton_clicked();
        /**
         * Handle double click on spec element
         * @param current Current item in spec list
         */
        void on_testList_doubleClicked(const QModelIndex& current);
        /**
         * Handle context menu request of spec
         * @param pos Position of request
         */
        void on_testList_customContextMenuRequested(const QPoint &pos);
        /**
         * Handle enable spec element
         */
        void on_testEnable();
        /**
         * Handle disable spec element
         */
        void on_testDisable();
        /**
         * Handle enable all spec element
         */
        void on_testEnableAll();
        /**
         * Handle disable all spec element
         */
        void on_testDisableAll();

        /**
         * Handle selection changes in test spec
         * @param current Selected item
         */
        void testList_currentChanged(const QModelIndex &current);

        // test steps
        /**
         * Handle add precondition
         */
        void on_stepAddPrecondition();
        /**
         * Handle add step
         */
        void on_stepAddStep();
        /**
         * Handle add postcondition
         */
        void on_stepAddPostcondition();
        /**
         * Handle remove step
         */
        void on_stepRemoveButton_clicked();
        /**
         * Handle move step up
         */
        void on_stepUpButton_clicked();
        /**
         * Handle move step down
         */
        void on_stepDownButton_clicked();
        /**
         * Handle edit step
         */
        void on_stepEditButton_clicked();
        /**
         * Handle step execution button
         */
        void on_stepExecutionButton_clicked();
        /**
         * Handle logger button
         */
        void on_loggerButton_clicked();
        /**
         * Handle test result changes
         * @param text Selected result
         */
        void on_testResultState_currentTextChanged(const QString& text);

        /**
         * Handle context menu request of step
         * @param pos Position of request
         */
        void on_stepList_customContextMenuRequested(const QPoint &pos);
        /**
         * Handle step passed action
         */
        void on_stepSetPassed();
        /**
         * Handle step remove result action
         */
        void on_stepRemoveResult();

        /**
         * Handle stpe selection
         * @param current Selected step
         */
        void stepList_currentChanged(const QModelIndex &current);

        /**
         * Handle add requirement
         */
        void on_requirementAddButton_clicked();
        /**
         * Handle remove requirement
         */
        void on_requirementRemoveButton_clicked();
        /**
         * Handle requirement selection
         */
        void requirementList_currentChanged();

        /**
         * Handle project creation
         * @param project Path to project file
         * @param spec Path to spec file
         */
        void createProject(QString project, QString spec);
        /**
         * Create feature group
         * @param name Name of feature group
         */
        void createFeatures(QString name);
        /**
         * Create feature
         * @param name Name of feature
         * @param id Id of feature
         */
        void createFeature(QString name, QString id);
        /**
         * Create testcase
         * @param id Id of testcase
         */
        void createTestcase(QString id);
        /**
         * Handle linked testcase creation
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         */
        void createLinkedTestcase(QString featureId, QString testcaseId);
        /**
         * Handle edited text
         * @param text Changed text
         * @param ref Reference object
         */
        void textEdited(QString text, QObject* ref);

        /**
         * Handle feature group edit
         * @param name Name of feature group
         */
        void editFeatureGroup(QString name);
        /**
         * Handle feature edit
         * @param name Name of feature
         * @param id Id of feature
         */
        void editFeature(QString name, QString id);
        /**
         * Handle testcase edit
         * @param id Id of testcase
         * @param state State of testcase
         */
        void editTestcase(QString id, QString state);
        /**
         * Handle linked testcase edit
         * @param featureId Id of feature
         * @param testcaseId Id of testcase
         */
        void editLinkedTestcase(QString featureId, QString testcaseId);
        /**
         * Handle step edit
         * @param title Title of step
         */
        void editStep(QString title);

        /**
         * Handle project settings changes
         */
        void projectSettingsChanged();
        /**
         * Handle requirement creation
         * @param id Id of requirement
         */
        void createRequirement(QString id);

        /**
         * Update state of GUI elements
         */
        void updateGuiState();

    protected:
        /**
         * Called if window closed
         * @param event Event object
         */
        void closeEvent(QCloseEvent* event);

        /**
         * Handle events of installed filter
         * @param watched Instance of watched object
         * @param event Occured event
         * @return True if handled
         */
        bool eventFilter(QObject* watched, QEvent* event);

    private:
        /**
         * GUI elements
         */
        Ui::MainWindow ui;

        /**
         * Action to add feature element
         */
        QAction* addFeatureAction;
        /**
         * Action to add testcase element
         */
        QAction* addTestcaseAction;
        /**
         * Action to add linked testcase element
         */
        QAction* addLinkedTestcaseAction;

        /**
         * Test list menu
         */
        QMenu* testListMenu;
        /**
         * Test step list menu
         */
        QMenu* testStepListMenu;

        /**
         * Dialog for project creation
         */
        CreateProjectDialog* createProjectDialog;
        /**
         * Dialog for feature group creation
         */
        CreateFeatureGroupDialog* createFeatureGroupDialog;
        /**
         * Dialog for feature creation
         */
        CreateFeatureDialog* createFeatureDialog;
        /**
         * Dialog for testcase creation
         */
        CreateTestcaseDialog* createTestcaseDialog;
        /**
         * Dialog for text edit
         */
        TextEditDialog* textEditDialog;
        /**
         * Dialog for feature group edit
         */
        EditFeatureGroupDialog* editFeatureGroupDialog;
        /**
         * Dialog for feature edit
         */
        EditFeatureDialog* editFeatureDialog;
        /**
         * Dialog for testcase edit
         */
        EditTestcaseDialog* editTestcaseDialog;
        /**
         * Dialog for step edit
         */
        EditStepDialog* editStepDialog;
        /**
         * Dialog for project settings edit
         */
        ProjectSettingsDialog* projectSettingsDialog;
        /**
         * Dialog for application settings edit
         */
        ApplicationSettingsDialog* applicationSettingsDialog;
        /**
         * Dialog for test execution
         */
        TestExecutionDialog* testExecutionDialog;
        /**
         * Dialog for step logger
         */
        StepLoggerDialog* stepLoggerDialog;
        /**
         * Dialog for requirement creation
         */
        CreateRequirementDialog* createRequirementDialog;
        /**
         * Dialog for linked testcase creation
         */
        CreateLinkedTestcaseDialog* createLinkedTestcaseDialog;
        /**
         * Dialog for linked testcase edit
         */
        EditLinkedTestcaseDialog* editLinkedTestcaseDialog;
        /**
         * About dialog
         */
        AboutDialog* aboutDialog;

        /**
         * Path to project file
         */
        QString projectFile;
        /**
         * Path to requirements file
         */
        QString requirementsFile;

        /**
         * Instance of test specification or NULL if not loaded
         */
        TestSpec* testSpec;
        /**
         * Model of test list
         */
        TestSpecModel* testSpecModel;
        /**
         * Model of test requirements
         */
        QStringListModel* requirementsModel;
        /**
         * Model of test list
         */
        TestStepsModel* testStepsModel;

        /**
         * Current selected features
         */
        FeatureGroup* currentFeatures;
        /**
         * Current selected feature
         */
        Feature* currentFeature;
        /**
         * Current selected testcase
         */
        Testcase* currentTestcase;

        /**
         * True if project changed
         */
        bool projectChanged;
        /**
         * True if report changed
         */
        bool reportChanged;

        /**
         * True if report mode is enabled
         */
        bool projectReportMode;

        /**
         * Close current open project
         * @return True on success
         */
        bool closeProject();

        /**
         * Load test spec
         * @param specPath Path to test spec
         * @param disabledTests List of disabled test cases
         */
        void loadTestSpec(const QString& specPath,
                          const QStringList& disabledTests);

        /**
         * Load requirement list
         * @param path Path to requirement list
         */
        void loadRequirements(const QString& path);
        /**
         * Load requirement group
         * @param group Group element
         * @return List of requirements
         */
        QStringList loadRequirementGroup(const QDomElement& group) const;

        /**
         * Load given project file
         * @param projectFile Project file to open
         * @return True on success
         */
        bool loadProject(const QString &projectFile);
        /**
         * Save project
         */
        void saveProject();

        /**
         * Disable all GUI elements
         */
        void gui_disableSpec();
        /**
         * Disable all testcase GUI elements
         */
        void gui_disableTestcase();
        /**
         * Disable all test step GUI elements
         */
        void gui_disableStep();
};


#endif //TESTLI_MAINWINDOW_H
