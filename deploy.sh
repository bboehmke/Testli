#!/bin/bash

# copy Qt5Svg manually
cp ${MXE_BASE_PATH}/usr/x86_64-w64-mingw32.shared/qt5/bin/Qt5Svg.dll bin/

# copy all dependencies
${MXE_BASE_PATH}/tools/copydlldeps.sh \
    --indir bin/ \
    --destdir ../Testli/   \
    --recursivesrcdir ${MXE_BASE_PATH}/usr/${MXE_TARGET}/ \
    --copy \
    --enforcedir ${MXE_BASE_PATH}/usr/${MXE_TARGET}/qt5/plugins/platforms/ \
    --enforcedir ${MXE_BASE_PATH}/usr/${MXE_TARGET}/qt5/plugins/iconengines/ \
    --objdump ${MXE_BASE_PATH}/usr/bin/${MXE_TARGET}-objdump

# copy build results
cp bin/* ../Testli/